<?php
/**
 * Manages sidebars.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

use WpBuilder\Utilities;

/**
 * The SidebarManager class.
 */
class SidebarManager {
	/**
	 * Sidebars.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $sidebars
	 */
	protected $sidebars;

	/**
	 * Constructucts a new Sidebars object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->sidebars = $this->set_sidebars();
	}

	/**
	 * Register sidebars.
	 *
	 * @since 0.0.1
	 */
	public function register_sidebars() {
		$sidebars = $this->sidebars;
		if ( ! empty( $sidebars ) ) {
			foreach ( $sidebars as $sidebar ) {
				$widget_classes = [
					'widget',
					( isset( $sidebar['widget_class'] ) && ! empty( $sidebar['widget_class'] ) ? strip_tags( $sidebar['widget_class'] ) : '' ),
				];

				$widget_title_classes = [
					'widget-title',
					$sidebar['machine_name'],
					( isset( $sidebar['widget_title_class'] ) && ! empty( $sidebar['widget_title_class'] ) ? strip_tags( $sidebar['widget_title_class'] ) : '' ),
				];

				register_sidebar(
					[
						'name' => $sidebar['title'],
						'id' => $sidebar['machine_name'],
						'description' => $sidebar['description'],
						'before_widget' => '<' . $sidebar['widget_wrapper'] . ' id="%1$s" class="' . implode( ' ', $widget_classes ) . ' %2$s">',
						'after_widget' => '</' . $sidebar['widget_wrapper'] . '>',
						'before_title' => '<' . $sidebar['widget_title_wrapper'] . ' class="' . implode( ' ', $widget_title_classes ) . '">',
						'after_title' => '</' . $sidebar['widget_title_wrapper'] . '>',
					]
				);
			}
		}
	}

	/**
	 * Delete single sidebar.
	 *
	 * @since 2.0.0
	 * @param string $item The machine_name of the sidebar to delete.
	 *
	 * @return bool
	 */
	public function delete_single( $item ) {
		$sidebars = $this->sidebars;
		$ordered = [];
		$i = 0;
		foreach ( $sidebars as $sidebar => $properties ) {
			if ( $properties['machine_name'] == $item ) {
				unset( $sidebars[ $sidebar ] );
			} else {
				$ordered[ $i ] = $sidebars[ $sidebar ];
				++$i;
			}
		}
		return $this->save( $ordered );
	}

	/**
	 * Update sidebars.
	 *
	 * @since 2.0.0
	 * @param array  $new_sidebars The new sidebars.
	 * @param string $action       The action when updating.
	 *
	 * @return bool $success
	 */
	public function update( $new_sidebars, $action = 'add' ) {
		$stored_sidebars = $this->sidebars;
		$sidebars = [];
		$success = false;
		foreach ( $new_sidebars as $sidebar => $properties ) {
			if ( $this->validate( $properties ) ) {
				$sidebars[] = Utilities::sanitizeArray( $properties );
			}
		}

		if ( empty( $sidebars ) ) {
			return;
		}
		switch ( $action ) {
			case 'delete':
				$success = $this->save( $sidebars );
				break;
			case 'add':
				if ( empty( $stored_sidebars ) ) {
					$success = $this->save( $sidebars );
				} else {
					foreach ( $sidebars as $sb_key => $sb_val ) {
						$matched = false;
						foreach ( $stored_sidebars as $ssb_key => $ssb_val ) {
							if ( $sb_val == $ssb_val ) {
								return true;
							}
							if ( $sb_val['machine_name'] == $ssb_val['machine_name'] ) {
								$matched = true;
							}
						}
						if ( ! $matched ) {
							$stored_sidebars[] = $sb_val;
						}
					}
					$success = $this->save( $stored_sidebars );
				}
				break;
			case 'overwrite':
				if ( empty( $stored_sidebars ) ) {
					$success = $this->save( $sidebars );
				} else {
					foreach ( $sidebars as $sb_key => $sb_val ) {
						$matched = false;
						foreach ( $stored_sidebars as $ssb_key => $ssb_val ) {
							if ( $sb_val == $ssb_val ) {
								return true;
							}
							if ( $sb_val['machine_name'] == $ssb_val['machine_name'] ) {
								$matched = true;
								$stored_sidebars[ $ssb_key ] = $sb_val;
							}
						}
						if ( ! $matched ) {
							$stored_sidebars[] = $sb_val;
						}
					}
					$success = $this->save( $stored_sidebars );
				}
				break;
		}
		return $success;
	}

	/**
	 * Validate sidebar.
	 *
	 * @since 2.0.0
	 * @param array $sidebar The sidebar to validate.
	 */
	public function validate( $sidebar ) {
		if ( ! isset( $sidebar['machine_name'] ) || strlen( $sidebar['machine_name'] ) < 1 ) {
			return false;
		}
		return true;
	}

	/**
	 * Save sidebars.
	 *
	 * @since 2.0.0
	 * @param array $sidebars The sidebars to save.
	 *
	 * @return bool
	 */
	public function save( $sidebars ) {
		$save_settings['sidebars'] = serialize( $sidebars );
		return update_option( 'wp_builder_sidebars', $save_settings );
	}

	/**
	 * Sets sidebars.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return array $sidebars
	 */
	private function set_sidebars() {
		$sidebars = [];
		$sb = get_option( 'wp_builder_sidebars' );
		if ( ! empty( $sb ) && isset( $sb['sidebars'] ) ) {
			$options = unserialize( $sb['sidebars'] );
			if ( is_array( $options ) ) {
				$sidebars = $options;
			}
		}
		return $sidebars;
	}

	/**
	 * Gets sidebars.
	 *
	 * @since 2.0.0
	 *
	 * @return array $sidebars
	 */
	public function get_sidebars() {
		return $this->sidebars;
	}
}
