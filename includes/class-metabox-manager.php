<?php
/**
 * Manages Metaboxes.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

use WpBuilder\NoticeManager;
use WpBuilder\Utilities;

/**
 * The MetaboxManager class.
 */
class MetaboxManager {

	/**
	 * Metaboxes.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $metaboxes
	 */
	protected $metaboxes;

	/**
	 * Constructs a new Metabox Manager object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->init_cmb();
		$this->metaboxes = $this->set_metaboxes();
	}

	/**
	 * Register metaboxes.
	 *
	 * @since 0.0.1
	 */
	public function register_metaboxes() {
		$metaboxes = $this->metaboxes;
		if ( ! empty( $metaboxes ) ) {
			foreach ( $metaboxes as $metabox ) {
				$mb_args = [
					'id' => $metabox['id'] . '_' . $metabox['unique_id'],
					'title' => $metabox['title'],
					'object_types' => isset( $metabox['pages'] ) ? $metabox['pages'] : [],
					'priority' => isset( $metabox['priority'] ) ? $metabox['priority'] : 0,
					'show_names' => $metabox['show_names'],
					'context' => isset( $metabox['context'] ) ? $metabox['context'] : null,
				];
				if ( isset( $metabox['tpls'] ) ) {
					$mb_args['show_on'] = [
						'key' => 'page-template',
						'value' => $metabox['tpls'],
					];
				}
				$mb = new_cmb2_box( $mb_args );
				if ( array_key_exists( 'fields', $metabox ) ) {
					foreach ( $metabox['fields'] as $field ) {
						$mapped_field = $this->map_field_type( $field );
						if ( $mapped_field ) {
							$mb->add_field( $mapped_field );
						}
					}
				}
			}
		}
	}

	/**
	 * Delete single metabox, field, or option.
	 *
	 * @since 2.0.0
	 * @param string $var The Metabox to delete.
	 *
	 * @return bool
	 */
	public function delete_single( $var ) {
		$metaboxes = $this->metaboxes;
		$ordered = [];
		$i = 0;
		foreach ( $metaboxes as $metabox => $properties ) {
			if ( $properties['unique_id'] == $var ) {
				unset( $metaboxes[ $metabox ] );
				$deleted = true;
			} else {
				$ordered[ $i ] = $metaboxes[ $metabox ];
				++$i;
			}
		}
		return $this->save( $ordered );
	}

	/**
	 * Update metaboxes.
	 *
	 * @since 2.0.0
	 * @param array  $new_metaboxes The array of new metaboxes.
	 * @param string $action        The action to use.
	 *
	 * @return bool $success
	 */
	public function update( $new_metaboxes, $action = 'add' ) {
		$stored_metaboxes = $this->metaboxes;
		$metaboxes = [];
		$success = false;
		foreach ( $new_metaboxes as $metabox => $properties ) {
			if ( $this->validate( $properties ) ) {
				$properties = Utilities::sanitizeArray( $properties );
				$metaboxes[] = $this->map_properties( $properties );
			}
		}
		switch ( $action ) {
			case 'delete':
				$success = $this->save( $new_metaboxes );
				break;
			case 'add':
				if ( empty( $stored_metaboxes ) ) {
					$success = $this->save( $new_metaboxes );
				} else {
					foreach ( $metaboxes as $metabox_key => $metabox ) {
						$matched = false;
						foreach ( $stored_metaboxes as $smb_key => $smb_values ) {
							// Short cut results if no values have changed because update_option.
							if ( $smb_values == $metabox ) {
								return true;
							}
							if ( $smb_values['unique_id'] == $metabox['unique_id'] ) {
								$matched = true;
							}
						}
						if ( ! $matched ) {
							$stored_metaboxes[] = $metabox;
						}
					}
					$success = $this->save( $stored_metaboxes );
				}
				break;
			case 'overwrite':
				if ( empty( $stored_metaboxes ) ) {
					$success = $this->save( $metaboxes );
				} else {
					foreach ( $metaboxes as $metabox_key => $metabox ) {
						$matched = false;
						foreach ( $stored_metaboxes as $smb_key => $smb_values ) {
							if ( $smb_values == $metabox ) {
								return true;
							}
							if ( $smb_values['unique_id'] == $metabox['unique_id'] ) {
								$matched = true;
								$stored_metaboxes[ $smb_key ] = $metabox;
							}
						}
						if ( ! $matched ) {
							$stored_metaboxes[] = $metabox;
						}
					}
					$success = $this->save( $stored_metaboxes );
				}
				break;
		}
		return $success;
	}

	/**
	 * Map properties.
	 *
	 * @since 2.0.0
	 * @param array $properties The metabox properties to map.
	 *
	 * @return array $properties
	 */
	public function map_properties( $properties ) {
		$prefix = '_wp_builder_' . $properties['unique_id'];
		$excluded_types = [
			'wysiwyg',
			'colorpicker',
			'file',
			'file_list',
		];
		if ( isset( $properties['show_names'] ) && ( $properties['show_names'] == 1 || $properties['show_names'] == true ) ) {
			$properties['show_names'] = true;
		} else {
			$properties['show_names'] = false;
		}
		if ( isset( $properties['fields'] ) ) {
			foreach ( $properties['fields'] as $field => $field_values ) {
				if ( ! $this->validate_field( $field_values ) ) {
					unset( $properties['fields'][ $field ] );
					continue;
				}
				if ( strpos( $field_values['id'], $prefix ) === false ) {
					$properties['fields'][ $field ]['id'] = $prefix . '_' . $field_values['id'];
				}
				if ( ! in_array( $field_values['type'], $excluded_types ) ) {
					if ( isset( $field_values['options'] ) ) {
						foreach ( $field_values['options'] as $option => $option_value ) {
							if ( trim( $option_value['name'] ) == '' || trim( $option_value['value'] ) == '' ) {
								unset( $properties['fields'][ $field ]['options'][ $option ] );
							}
						}
					}
				}
				// Add child fields if type == group.
				if ( 'group' == $field_values['type'] ) {
					if ( isset( $field_values['fields'] ) && ! empty( $field_values['fields'] ) ) {
						foreach ( $field_values['fields'] as $group_field => $group_field_values ) {
							if ( ! $this->validate_field( $group_field_values ) ) {
								unset( $properties['fields'][ $field ]['fields'][ $group_field ] );
								continue;
							}
							if ( ! in_array( $group_field_values['type'], $excluded_types ) ) {
								if ( isset( $group_field_values['options'] ) ) {
									foreach ( $group_field_values['options'] as $option => $option_value ) {
										if ( trim( $option_value['name'] ) == '' || trim( $option_value['value'] ) == '' ) {
											unset( $properties['fields'][ $field ]['fields'][ $group_field ]['options'][ $option ] );
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $properties;
	}

	/**
	 * Validate field.
	 *
	 * @since 2.0.0
	 * @param array $field The field to validate.
	 *
	 * @return bool
	 */
	public function validate_field( $field ) {
		if ( ! isset( $field['name'] ) || empty( $field['name'] ) ) {
			return false;
		}
		if ( ! isset( $field['id'] ) || empty( $field['id'] ) ) {
			return false;
		}
		return true;
	}

	/**
	 * Validate metabox.
	 *
	 * @since 2.0.0
	 * @param array $metabox The metabox to validate.
	 *
	 * @return bool
	 */
	public function validate( $metabox ) {
		if ( ! isset( $metabox['id'] ) || empty( $metabox['id'] ) ) {
			return false;
		}
		if ( ! isset( $metabox['unique_id'] ) || empty( $metabox['unique_id'] ) ) {
			return false;
		}
		return true;
	}
	/**
	 * Save metaboxes.
	 *
	 * @since 2.0.0
	 * @param array $metaboxes The metaboxes to save.
	 *
	 * @return bool
	 */
	public function save( $metaboxes ) {
		$save_settings['metaboxes'] = serialize( $metaboxes );
		return update_option( 'wp_builder_metaboxes', $save_settings );
	}

	/**
	 * Init CMB2.
	 *
	 * @since 2.0.0
	 */
	public function init_cmb() {
		if ( ! class_exists( 'cmb_Meta_Box' ) && file_exists( WP_BUILDER_VENDOR_PATH . 'cmb2/init.php' ) ) {
			require_once WP_BUILDER_VENDOR_PATH . 'cmb2/init.php';
		}
	}

	/**
	 * Map Field Type Settings
	 *
	 * @since 0.0.1
	 * @param array $field An array of field values.
	 */
	private static function map_field_type( $field ) {
		if ( is_array( $field ) ) {
			switch ( $field['type'] ) {
				case 'colorpicker':
					if ( array_key_exists( 'options', $field ) ) {
						$palettes = [];
						foreach ( $field['options'] as $option ) {
							$palettes[] = $option['value'];
						}
						$field['attributes']['data-colorpicker'] = json_encode( array( 'palettes' => $palettes ) );
					}
					break;
				case 'select':
					if ( array_key_exists( 'options', $field ) ) {
						$options = [];
						foreach ( $field['options'] as $option ) {
							$options[ $option['value'] ] = $option['name'];
						}
						$field['options'] = $options;
					}
					break;
				case 'radio':
					if ( array_key_exists( 'options', $field ) ) {
						$options = [];
						foreach ( $field['options'] as $option ) {
							$options[ $option['value'] ] = $option['name'];
						}
						$field['options'] = $options;
					}
					break;
				case 'radio_inline':
					if ( array_key_exists( 'options', $field ) ) {
						$options = [];
						foreach ( $field['options'] as $option ) {
							$options[ $option['value'] ] = $option['name'];
						}
						$field['options'] = $options;
					}
					break;
				case 'checkbox':
					if ( array_key_exists( 'options', $field ) ) {
						$options = [];
						foreach ( $field['options'] as $option ) {
							$options[ $option['value'] ] = $option['name'];
						}
						$field['options'] = $options;
					}
					break;
				case 'multicheck':
					if ( array_key_exists( 'options', $field ) ) {
						$options = [];
						foreach ( $field['options'] as $option ) {
							$options[ $option['value'] ] = $option['name'];
						}
						$field['options'] = $options;
					}
					break;
				case 'group':
					$field['options'] = [
						'group_title' => $field['name'],
						'add_button' => __( 'Add', 'wp-builder' ) . ' ' . $field['name'],
						'remove_button' => __( 'Remove', 'wp-builder' ) . ' ' . $field['name'],
						'sortable' => true,
						'closed' => true,
					];
					if ( array_key_exists( 'fields', $field ) ) {
						foreach ( $field['fields'] as $sub_field => $sub_field_props ) {
							$mapped_field = self::map_field_type( $sub_field_props );
							if ( $mapped_field ) {
								$field['fields'][ $sub_field ] = $mapped_field;
							}
						}
					}
					break;
				case 'file':
					break;
				case 'file_list':
					break;
				case 'faiconselect':
					$field['options_cb'] = 'returnRayFapsa';
					$field['attributes'] = [ 'faver' => 5 ];
					break;
				case 'post_search_ajax':
					if ( isset( $field['post_type'] ) ) {
						$field['query_args'] = [
							'post_type' => $field['post_type'],
						];
					}
					break;
				case 'wysiwyg':
					break;
				default:
					break;
			}
			return $field;
		}
		return false;
	}

	/**
	 * Set metaboxes.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return array $metaboxes
	 */
	private function set_metaboxes() {
		$metaboxes = [];
		$mb = get_option( 'wp_builder_metaboxes' );
		if ( ! empty( $mb ) && is_array( $mb ) && array_key_exists( 'metaboxes', $mb ) ) {
			$options = unserialize( $mb['metaboxes'] );
			if ( is_array( $options ) ) {
				$metaboxes = $options;
			}
		}
		return $metaboxes;
	}

	/**
	 * Get metaboxes.
	 *
	 * @since 2.0.0
	 *
	 * @return array $metaboxes
	 */
	public function get_metaboxes() {
		return $this->metaboxes;
	}

	/**
	 * Supported post types.
	 *
	 * @since 2.0.3
	 *
	 * @return array $supported_post_types
	 */
	public function supported_post_types() {
		$supported_post_types = [];
		$unsupported = [
			'attachment',
			'revision',
			'nav_menu_item',
			'custom_css',
			'customize_changeset',
			'oembed_cache',
			'user_request',
			'wp_block',
		];
		$current_post_types = get_post_types( '', 'objects' );
		foreach ( $current_post_types as $cpt ) {
			if ( ! in_array( $cpt->name, $unsupported ) ) {
				$supported_post_types[] = $cpt;
			}
		}
		return $supported_post_types;
	}

	/**
	 * Array of field types.
	 *
	 * @since 2.0.0
	 *
	 * @return array $types
	 */
	public function field_types() {
		// default types.
		$types = [
			'text' => 'text (optionally repeatable)',
			'text_small' => 'text small (optionally repeatable)',
			'text_medium' => 'text medium (optionally repeatable)',
			'text_url' => 'text url (optionally repeatable)',
			'text_email' => 'text email (optionally repeatable)',
			'text_money' => 'text money (optionally repeatable)',
			'text_date' => 'date picker',
			'text_date_timestamp' => 'date picker (unix timestamp)',
			'text_datetime_timestamp' => 'time picker combo (unix timestamp)',
			'text_datetime_timestamp_timezone' => 'date time picker with time zone combo (serialized DateTime object)',
			'select_timezone' => 'time zone dropdown',
			'text_time' => 'time picker',
			'colorpicker' => 'color picker',
			'textarea' => 'textarea',
			'textarea_small' => 'textarea small',
			'textarea_code' => 'textarea code',
			'select' => 'select',
			'radio' => 'radio',
			'radio_inline' => 'radio inline',
			'taxonomy_radio' => 'taxonomy radio',
			'taxonomy_select' => 'taxonomy select',
			'taxonomy_multicheck' => 'taxonomy multicheck',
			'checkbox' => 'checkbox',
			'multicheck' => 'multicheck',
			'wysiwyg' => 'WYSIWYG/TinyMCE',
			'file' => 'Image/file upload',
			'file_list' => 'File List',
			'oembed' => 'oEmbed',
			'group' => 'Group',
		];
		// contrib types.
		$contrib_types = $this->contrib_field_types();
		// check if plugin is active.
		foreach ( $contrib_types as $plugin ) {
			$plugin_file = $plugin['plugin'];
			if ( is_plugin_active( $plugin_file ) ) {
				$types = array_merge( $types, $plugin['props'] );
			}
		}
		return $types;
	}

	/**
	 * Contrib field types.
	 *
	 * @since 2.0.0
	 *
	 * @return array $contrib_types
	 */
	public function contrib_field_types() {
		return [
			'cmb2-field-link' => [
				'plugin' => 'cmb2-field-link/cmb2-field-link.php',
				'props' => [ 'link' => 'Link field: href, text, class, id' ],
			],
			'cmb2-field-widget-selector' => [
				'plugin' => 'cmb2-field-widget-selector/cmb2-field-widget-selector.php',
				'props' => [ 'widget_selector' => 'Widget Selector: Select widgets from a sidebar.' ],
			],
			'cmb2-field-address' => [
				'plugin' => 'cmb2-field-address/cmb2-field-address.php',
				'props' => [ 'address' => 'Address field: Street 1, Street 2, City, State, Zip' ],
			],
			'cmb2-field-faiconselect' => [
				'plugin' => 'cmb2-field-faiconselect/iconselect.php',
				'props' => [ 'faiconselect' => 'Fontawesome Icon Field: Insert a Fontawesome Icon' ],
			],
			'cmb2-field-type-font-awesome' => [
				'plugin' => 'cmb2-field-type-font-awesome/iconselect.php',
				'props' => [ 'faiconselect' => 'Fontawesome Icon: Insert a Fontawesome icon' ],
			],
			'cmb2-field-post-search-ajax' => [
				'plugin' => 'cmb2-field-post-search-ajax/cmb-field-post-search-ajax.php',
				'props' => [ 'post_search_ajax' => 'Post search: Select posts and more.' ],
			],
		];
	}

}
