<?php
/**
 * Notice Manager.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

/**
 * Contains NoticeManager class.
 */
class NoticeManager {

	/**
	 * $notice = [
	 *   'type' => string ( 'info', 'warning', 'error', 'success' ),
	 *   'message' => string $message,
	 *   'dismissable' => bool,
	 *   'scope' => string ( 'global' ),
	 *   'code' => int ( 100 ),
	 * ]
	 */

	/**
	 * Show Notices.
	 *
	 * @since 2.0.0
	 */
	public function show_notices() {
		global $pagenow;
		$notices = get_option( 'wp_builder_notices' );
		if ( is_array( $notices ) ) {
			foreach ( $notices as $id => $notice ) {
				$dismiss_url = add_query_arg( [ 'wp_builder_admin_notice_dismiss' => $id ], admin_url() );
				ob_start();
				?>
				<div class="notice wp-builder-notice notice-<?= $notice['type']; ?> <?php if ( $notice['dismissible'] ) { print 'is-dismissible'; } ?> "
					<?php if ( $notice['dismissible'] ) : ?>
						data-dismiss-url="<?= esc_url( $dismiss_url ); ?>"
					<?php endif; ?>
				>
					<p><strong>WP Builder:</strong> <?php print $notice['message']; ?></p>
				</div>
				<?php
				$output = ob_get_clean();
				if ( isset( $notice['scope'] ) ) {
					if ( 'global' == $notice['scope'] ) {
						print $output;
					} elseif ( $pagenow == $notice['scope'] ) {
						print $output;
					} elseif ( isset( $_GET['tab'] ) && strpos( $notice['scope'], $_GET['tab'] ) != false ) {
						print $output;
					}
				}
				if ( ! $notice['perm'] ) {
					unset( $notices[ $id ] );
				}
			}
		}
		update_option( 'wp_builder_notices', $notices );
	}

	/**
	 * Dismiss Notice.
	 *
	 * @since 2.0.0
	 */
	public function dismiss_notice() {
		$dismiss_notice = filter_input( INPUT_GET, 'wp_builder_admin_notice_dismiss', FILTER_SANITIZE_STRING );
		$notices = get_option( 'wp_builder_notices' );
		if ( is_array( $notices ) ) {
			if ( isset( $notices[ $dismiss_notice ] ) ) {
				unset( $notices[ $dismiss_notice ] );
			}
		}
		update_option( 'wp_builder_notices', $notices );
	}

	/**
	 * Add Notice.
	 *
	 * @since 2.0.0
	 * @param string $type        ( 'info', 'warning', 'error', 'success' ).
	 * @param string $message     The body of the message.
	 * @param bool   $dismissible Is notification dismissable.
	 * @param string $scope       Scope, global or per tab.
	 * @param int    $code        An error code.
	 * @param bool   $perm        Display once or until dismissed.
	 */
	public static function add_notice( $type, $message, $dismissible = true, $scope = 'global', $code = 100, $perm = false ) {
		$notices = get_option( 'wp_builder_notices' );
		if ( ! $notices || empty( $notices ) ) {
			$notices = [];
		}
		if ( ! array_search( $code, array_column( $notices, 'code' ), true ) ) {
			$id = uniqid();
			$notices[ $id ] = [
				'type' => $type,
				'message' => $message,
				'dismissible' => $dismissible,
				'scope' => $scope,
				'code' => $code,
				'perm' => $perm,
			];
		}
		update_option( 'wp_builder_notices', $notices );
	}

	/**
	 * Delete all notices.
	 *
	 * @since 1.0.0
	 */
	public static function delete_all_notices() {
		update_option( 'wp_builder_notices', '' );
	}
}
