<?php
/**
 * Contains utility functions.
 *
 * @since 2.0.0
 * @package WpBuilder.
 */
namespace WpBuilder;

class Utilities {

	/*
	 * Validate json strings
	 *
	 * @since 0.0.1
	 * @param string $string
	 *
	 * @return bool
	 */
	function isJson( $string ) {
		json_decode( $string );
		return ( json_last_error() == JSON_ERROR_NONE );
	}

	/**
	 * Sanitize array.
	 *
	 * @since 2.0.2
	 * @param array $array
	 *   The array to sanitize.
	 *
	 * @return array $array
	 *   The sanitized array.
	 */
	public static function sanitizeArray( $array ) {
		foreach ( $array as $key => &$value ) {
			if ( is_array( $value ) ) {
				$value = self::sanitizeArray( $value );
			}
			else {
				switch ( true ) {
					case ( filter_var( $value, FILTER_VALIDATE_URL ) ) :
						$value = esc_url( $value );
						break;
					case ( filter_var( $value, FILTER_VALIDATE_EMAIL ) ) :
						$value = sanitize_email( $value );
						break;
					case ( filter_var( $value, FILTER_VALIDATE_BOOLEAN ) ) :
						break;
					case ( filter_var( $value, FILTER_VALIDATE_INT ) ) :
						break;
					default :
						if ( $key == 'unique_id') {
							continue 2;
						}
						$value = sanitize_text_field( $value );
				}
			}
		}
		return $array;
	}
}