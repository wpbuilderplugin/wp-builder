<?php
/**
 * Shortcodes.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

/**
 * Contains Shortcodes class.
 */
class Shortcodes {

	/**
	 * Array of shortcodes.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $shortcodes
	 */
	protected $shortcodes;

	/**
	 * Constructs a \WpBuilder\Shortcodes object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->shortcodes = $this->define_shortcodes();
	}


	/**
	 * Define shortcodes.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return array $defined_shortcodes
	 */
	private function define_shortcodes() {
		$defined_shortcodes[] = [
			'name' => 'wp_builder',
			'callback' => 'metabox_shortcode',
		];
		return $defined_shortcodes;
	}

	/**
	 * Get shortcodes.
	 *
	 * @since 2.0.0
	 *
	 * @return array $shortcodes
	 */
	public function get_shortcodes() {
		return $this->shortcodes;
	}

	/**
	 * Metabox shortcode.
	 *
	 * @since 2.0.0
	 * @param array $atts The attributes passed to the shortcode.
	 *
	 * @return string $output
	 */
	public static function metabox_shortcode( $atts ) {
		extract( shortcode_atts( ['field' => '', 'post_id' => '' ], $atts ) );
		if ( ! isset( $atts['field'] ) ) {
			return 'field not defined';
		}
		$field = $atts['field'];
		if ( isset( $atts['post_id'] ) ) {
			$post_id = $atts['post_id'];
		} else {
			global $post;
			$post_id = $post->ID;
		}
		$content = get_post_meta( $post_id, $field, true );
		return $content;
	}
}
