<?php
/**
 * Manages taxonomies.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

use WpBuilder\Utilities;
use WpBuilder\NotificationManager;

/**
 * Contains TaxonomyManager class.
 */
class TaxonomyManager {

	/**
	 * Taxonomies.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $taxonomies
	 */
	protected $taxonomies;

	/**
	 * Constructs a new TaxonomiesManager.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->taxonomies = $this->set_taxonomies();
	}

	/**
	 * Register taxonomy.
	 *
	 * @since 0.0.1
	 */
	public function register_taxonomies() {
		$taxonomies = $this->taxonomies;
		if ( ! empty( $taxonomies ) ) {
			foreach ( $taxonomies as $tax_key => $tax_val ) {
				register_taxonomy(
					$tax_val['machine_name'],
					$tax_val['post_types'],
					$tax_val['args']
				);
				if ( is_array( $tax_val['post_types'] ) ) {
					register_taxonomy_for_object_type( $tax_val['machine_name'], $tax_val['post_types'] );
				} else {
					register_taxonomy_for_object_type( $tax_val['machine_name'], $tax_val['post_types'] );
				}
			}
		}
	}

	/**
	 * Delete single taxonomy.
	 *
	 * @since 2.0.0
	 * @param string $item The unique_id of the taxonomy to delete.
	 *
	 * @return bool
	 */
	public function delete_single( $item ) {
		$taxonomies = $this->taxonomies;
		$ordered = [];
		$i = 0;
		foreach ( $taxonomies as $taxonomy => $properties ) {
			if ( $properties['unique_id'] == $item ) {
				unset( $taxonomies[ $taxonomy ] );
			} else {
				$ordered[ $i ] = $taxonomies[ $taxonomy ];
				++$i;
			}
		}
		return $this->save( $ordered );
	}

	/**
	 * Update taxonomies.
	 *
	 * @since 2.0.0
	 * @param array  $new_taxonomies Array of taxonomies to add.
	 * @param string $action         The action to use.
	 *
	 * @return bool $success
	 */
	public function update( $new_taxonomies, $action = 'add' ) {
		$stored_taxonomies = $this->taxonomies;
		$success = false;
		$taxonomies = [];
		foreach ( $new_taxonomies as $taxonomy => $properties ) {
			if ( $this->validate( $properties ) ) {
				$properties = Utilities::sanitizeArray( $properties );
				$taxonomies[] = $this->map_properties( $properties );
			}
		}
		switch ( $action ) {
			case 'delete':
				$success = $this->save( $taxonomies );
				break;
			case 'add':
				if ( empty( $stored_taxonomies ) ) {
					$success = $this->save( $taxonomies );
				} else {
					foreach ( $taxonomies as $t_key => $t_val ) {
						$matched = false;
						foreach ( $stored_taxonomies as $st_key => $st_val ) {
							if ( $t_val == $st_val ) {
								return true;
							}
							if ( $t_val['unique_id'] == $st_val['unique_id'] ) {
								$matched = true;
							}
						}
						if ( ! $matched ) {
							$stored_taxonomies[] = $t_val;
						}
					}
					$success = $this->save( $stored_taxonomies );
				}
				break;
			case 'overwrite':
				if ( empty( $stored_taxonomies ) ) {
					$success = $this->save( $taxonomies );
				} else {
					foreach ( $taxonomies as $t_key => $t_val ) {
						$matched = false;
						foreach ( $stored_taxonomies as $st_key => $st_val ) {
							if ( $t_val == $st_val ) {
								return true;
							}
							if ( $t_val['unique_id'] == $st_val['unique_id'] ) {
								$matched = true;
								$stored_taxonomies[ $st_key ] = $t_val;
							}
						}
						if ( ! $matched ) {
							$stored_taxonomies[] = $t_val;
						}
					}
					$success = $this->save( $stored_taxonomies );
				}
				break;
		}
		return $success;
	}

	/**
	 * Map taxonomy properties.
	 *
	 * @since 2.0.0
	 * @param array $properties The properties to map.
	 *
	 * @return array $properties
	 */
	private function map_properties( $properties ) {
		if ( isset( $properties['post_types'] ) ) {
			foreach ( $properties['post_types'] as $tax_pt ) {
				if ( ! in_array( $tax_pt, get_post_types( '', 'names' ) ) ) {
					unset( $properties['post_types'][$tax_pt] );
				}
			}
		}
		if ( isset( $properties['args']['show_ui'] ) && ( $properties['args']['show_ui'] == '1' || $properties['args']['show_ui'] == true ) ) {
			$properties['args']['show_ui'] = true;
		} else {
			$properties['args']['show_ui'] = false;
		}
		if ( isset( $properties['args']['hiearchical'] ) && ( $properties['args']['hiearchical'] == '1' || $properties['args']['hiearchical'] == true ) ) {
			$properties['args']['hiearchical'] = true;
		} else {
			$properties['args']['hiearchical'] = false;
		}
		if ( isset( $properties['args']['show_admin_column'] ) && ( $properties['args']['show_admin_column'] == '1' || $properties['args']['show_admin_column'] == true ) ) {
			$properties['args']['show_admin_column'] = true;
		} else {
			$properties['args']['show_admin_column'] = false;
		}
		if ( isset( $properties['args']['query_var'] ) && ( $properties['args']['query_var'] == '1' || $properties['args']['query_var'] == true ) ) {
			$properties['args']['query_var'] = true;
		} else {
			$properties['args']['query_var'] = false;
		}
		if ( isset( $properties['args']['public'] ) && ( $properties['args']['public'] == '1' || $properties['args']['public'] == true ) ) {
			$properties['args']['public'] = true;
		} else {
			$properties['args']['public'] = false;
		}
		if ( isset( $properties['args']['show_in_nav_menus'] ) && ( $properties['args']['show_in_nav_menus'] == '1' || $tax[ $taxonomy ]['args']['show_in_nav_menus'] == true ) ) {
			$properties['args']['show_in_nav_menus'] = true;
		} else {
			$properties['args']['show_in_nav_menus'] = false;
		}
		if ( isset( $properties['args']['show_tagcloud'] ) && ( $properties['args']['show_tagcloud'] == '1' || $tax[ $taxonomy ]['args']['show_tagcloud'] == true ) ) {
			$properties['args']['show_tagcloud'] = true;
		} else {
			$properties['args']['show_tagcloud'] = false;
		}
		return $properties;
	}

	/**
	 * Validate taxonomy.
	 *
	 * @since 2.0.0
	 * @param array $taxonomy The taxonomy to validate.
	 *
	 * @return bool
	 */
	public function validate( $taxonomy ) {
		if ( ! isset( $taxonomy['machine_name'] ) || strlen( $taxonomy['machine_name'] ) < 1 ) {
			return false;
		}
		if ( ! isset( $taxonomy['post_types'] ) || empty( $taxonomy['post_types'] ) ) {
			return false;
		}
		if ( ! isset( $taxonomy['unique_id'] ) || empty( $taxonomy['unique_id'] ) ) {
			return false;
		}
		return true;
	}

	/**
	 * Save taxonomies.
	 *
	 * @since 2.0.0
	 * @param array $taxonomies The taxonomies to save.
	 *
	 * @return bool
	 */
	public function save( $taxonomies ) {
		$save_settings['taxonomies'] = serialize( $taxonomies );
		return update_option( 'wp_builder_taxonomies', $save_settings );
	}

	/**
	 * Set Taxonomies.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return array $taxonomies
	 */
	private function set_taxonomies() {
		$taxonomies = [];
		$tx = get_option( 'wp_builder_taxonomies' );
		if ( ! empty( $tx ) && isset( $tx['taxonomies'] ) ) {
			$options = unserialize( $tx['taxonomies'] );
			if ( is_array( $options ) ) {
				$taxonomies = $options;
			}
		}
		return $taxonomies;
	}

	/**
	 * Get Taxonomies.
	 *
	 * @since 2.0.0
	 *
	 * @return array $taxonomies
	 */
	public function get_taxonomies() {
		return $this->taxonomies;
	}
}
