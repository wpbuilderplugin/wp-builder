<?php
/**
 * The main class.
 *
 * @since 2.0.0
 * @package WpBuilder
 */

namespace WpBuilder;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once WP_BUILDER_PATH . 'includes/class-wp-builder-loader.php';
require_once WP_BUILDER_PATH . 'includes/class-wp-builder-utilities.php';
require_once WP_BUILDER_PATH . 'includes/class-metabox-manager.php';
require_once WP_BUILDER_PATH . 'includes/class-post-type-manager.php';
require_once WP_BUILDER_PATH . 'includes/class-menu-location-manager.php';
require_once WP_BUILDER_PATH . 'includes/class-sidebar-manager.php';
require_once WP_BUILDER_PATH . 'includes/class-taxonomy-manager.php';
require_once WP_BUILDER_PATH . 'includes/class-customizer-manager.php';
require_once WP_BUILDER_PATH . 'includes/class-notice-manager.php';
require_once WP_BUILDER_PATH . 'includes/class-shortcodes.php';
require_once WP_BUILDER_ADMIN_PATH . 'class-wp-builder-admin.php';
require_once WP_BUILDER_ADMIN_PATH . 'class-display-manager.php';
require_once WP_BUILDER_ADMIN_PATH . 'forms/class-customizer-forms.php';
require_once WP_BUILDER_ADMIN_PATH . 'forms/class-importer-forms.php';
require_once WP_BUILDER_ADMIN_PATH . 'forms/class-menu-location-forms.php';
require_once WP_BUILDER_ADMIN_PATH . 'forms/class-metabox-forms.php';
require_once WP_BUILDER_ADMIN_PATH . 'forms/class-post-type-forms.php';
require_once WP_BUILDER_ADMIN_PATH . 'forms/class-sidebar-forms.php';
require_once WP_BUILDER_ADMIN_PATH . 'forms/class-taxonomy-forms.php';
require_once WP_BUILDER_ADMIN_PATH . 'views/class-customizer-views.php';
require_once WP_BUILDER_ADMIN_PATH . 'views/class-menu-location-views.php';
require_once WP_BUILDER_ADMIN_PATH . 'views/class-metabox-views.php';
require_once WP_BUILDER_ADMIN_PATH . 'views/class-post-type-views.php';
require_once WP_BUILDER_ADMIN_PATH . 'views/class-sidebar-views.php';
require_once WP_BUILDER_ADMIN_PATH . 'views/class-taxonomy-views.php';

use WpBuilder\Admin;
use WpBuilder\CustomizerManager;
use WpBuilder\DisplayManager;
use WpBuilder\Loader;
use WpBuilder\MetaboxManager;
use WpBuilder\NoticeManager;
use WpBuilder\PostTypeManager;
use WpBuilder\Shortcodes;
use WpBuilder\SidebarManager;
use WpBuilder\TaxonomyManager;

/**
 * Contains Wp_Builder class.
 */
class Wp_Builder {

	/**
	 * Plugin name.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $plugin_name
	 */
	protected $plugin_name;

	/**
	 * Plugin version.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $plugin_version
	 */
	protected $plugin_version;

	/**
	 * WpBuilder\Admin definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Admin
	 */
	protected $admin;

	/**
	 * WpBuilder\Loader definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Loader $loader
	 */
	protected $loader;

	/**
	 * WpBuilder\CustomizerManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\CustomizerManager $customizer_manager
	 */
	protected $customizer_manager;

	/**
	 * WpBuilder\MetaboxManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MetaboxManager $metabox_manager
	 */
	protected $metabox_manager;

	/**
	 * WpBuilder\PostTypeManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\PostTypeManager $post_type_manager
	 */
	protected $post_type_manager;

	/**
	 * WpBuilder\TaxonomyManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\TaxonomyManager $taxonomy_manager
	 */
	protected $taxonomy_manager;

	/**
	 * WpBuilder\MenuLocationManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MenuLocationManager $menu_location_manager
	 */
	protected $menu_location_manager;

	/**
	 * WpBuilder\SidebarManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\SidebarManager $sidebar_manager
	 */
	protected $sidebar_manager;

	/**
	 * WpBuilder\DisplayManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\DisplayManager $display_manager
	 */
	protected $display_manager;

	/**
	 * WpBuilder\Forms\MetaboxForms definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Forms\MetaboxForms $metabox_forms
	 */
	protected $metabox_forms;

	/**
	 * WpBuilder\NoticeManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\NoticeManager $notice_manager
	 */
	protected $notice_manager;

	/**
	 * WpBuilder\Shortcodes definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Shortcodes $shortcodes
	 */
	protected $shortcodes;

	/**
	 * Constructs a Wp_Builder.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->plugin_version = WP_BUILDER_VERSION;
		$this->plugin_name = 'wp-builder';
		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_plugin_hooks();
		$this->define_shortcodes();

	}

	/**
	 * Load dependencies.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function load_dependencies() {
		$this->notice_manager = new \WpBuilder\NoticeManager();
		$this->metabox_manager = new \WpBuilder\MetaboxManager();
		$this->post_type_manager = new \WpBuilder\PostTypeManager();
		$this->taxonomy_manager = new \WpBuilder\TaxonomyManager();
		$this->sidebar_manager = new \WpBuilder\SidebarManager();
		$this->menu_location_manager = new \WpBuilder\MenuLocationManager();
		$this->loader = new \WpBuilder\Loader();
		$this->admin = new \WpBuilder\Admin();
		$this->shortcodes = new \WpBuilder\Shortcodes();
		$this->display_manager = new \WpBuilder\DisplayManager();
		$this->customizer_manager = new \WpBuilder\CustomizerManager();
	}

	/**
	 * Define admin hooks.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function define_admin_hooks() {

		$admin = $this->admin;
		$this->loader->add_action( 'admin_menu', $admin, 'add_admin_menu' );
		$this->loader->add_action( 'admin_enqueue_scripts', $admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_enqueue_scripts', $admin, 'enqueue_styles' );
		$plugin_basename = plugin_basename( plugin_dir_path( __DIR__ ) . $this->plugin_name . '.php' );
		$this->loader->add_action( 'plugin_action_links_' . $plugin_basename, $admin, 'action_links' );
		$display_manager = $this->display_manager;
		$this->loader->add_action( 'wp_ajax_wp_builder_get_field_options', $display_manager, 'ajax_get_field_options' );
		$this->loader->add_action( 'wp_ajax_wp_builder_get_field_form_structure', $display_manager, 'ajax_get_field_form_structure' );
		$this->loader->add_action( 'wp_ajax_wp_builder_get_field_structure', $display_manager, 'ajax_get_field_structure' );
		$this->loader->add_action( 'wp_ajax_wp_builder_get_customizer_settings', $display_manager, 'ajax_get_customizer_settings' );
		$this->loader->add_action( 'wp_ajax_wp_builder_get_post_type_capabilities', $display_manager, 'ajax_get_post_type_capabilities' );
	}

	/**
	 * Define plugin hooks.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function define_plugin_hooks() {
		$metaboxes = $this->metabox_manager;
		$this->loader->add_action( 'cmb2_admin_init', $metaboxes, 'register_metaboxes' );
		$post_types = $this->post_type_manager;
		$this->loader->add_action( 'init', $post_types, 'register_post_types' );
		$this->loader->add_action( 'init', $post_types, 'rewrites' );
		$taxonomies = $this->taxonomy_manager;
		$this->loader->add_action( 'init', $taxonomies, 'register_taxonomies' );
		$sidebars = $this->sidebar_manager;
		$this->loader->add_action( 'init', $sidebars, 'register_sidebars' );
		$menu_locations = $this->menu_location_manager;
		$this->loader->add_action( 'init', $menu_locations, 'register_menu_locations' );
		$this->loader->add_action( 'admin_init', $this->notice_manager, 'dismiss_notice' );
		$this->loader->add_action( 'admin_notices', $this->notice_manager, 'show_notices' );
		$this->loader->add_action( 'customize_register', $this->customizer_manager, 'register_customizer' );
	}

	/**
	 * Define shortcodes.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function define_shortcodes() {

		$shortcodes = $this->shortcodes->get_shortcodes();

		if ( ! empty( $shortcodes ) ) {
			foreach ( $shortcodes as $shortcode ) {
				$this->loader->add_shortcode( $shortcode['name'], new \WpBuilder\Shortcodes(), $shortcode['callback'] );
			}
		}
	}

	/**
	 * Run the loader to execute hooks.
	 *
	 * @since 2.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * Get the plugin name.
	 *
	 * @since 2.0.0
	 *
	 * @return string $plugin_name
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * Get the loader.
	 *
	 * @since 2.0.0
	 *
	 * @return \WpBuilder\Loader
	 */
	public function get_loader() {
		return $this->loader;
	}

}
