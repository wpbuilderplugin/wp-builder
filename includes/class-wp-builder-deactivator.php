<?php
/**
 * Plugin Deactivation.
 *
 * @since 2.0.0
 * @package wp-builder
 */

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Contains Wp_Builder_Deactivator class.
 */
class Wp_Builder_Deactivator {

	/**
	 * Deactivates plugin.
	 *
	 * @since 2.0.0
	 */
	public static function deactivate() {
		self::delete_settings();
	}

	/**
	 *  Deletes settings.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private static function delete_settings() {
		delete_option( 'wp_builder_sidebars' );
		delete_option( 'wp_builder_post_types' );
		delete_option( 'wp_builder_menu_locations' );
		delete_option( 'wp_builder_taxonomies' );
		delete_option( 'wp_builder_metaboxes' );
		delete_option( 'wp_builder_customizer' );
		delete_option( 'wp_builder_customizer_panels' );
		delete_option( 'wp_builder_customizer_sections' );
		delete_option( 'wp_builder_customizer_settings' );
		delete_option( 'wp_builder_notices' );
	}
}
