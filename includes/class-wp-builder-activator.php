<?php
/**
 * Plugin activation.
 *
 * @since 2.0.0
 * @package wp-builder
 */

// IF file is called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Container Wp_Builder_Activator class.
 */
class Wp_Builder_Activator {

	/**
	 * Min PHP Version.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $min_php_version
	 */
	protected static $min_php_version;

	/**
	 * Min WordPress version.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $min_wordpress_version
	 */
	protected static $min_wordpress_version;

	/**
	 * Activates WP Builder.
	 *
	 * @since 2.0.0
	 */
	public static function activate() {

		self::$min_php_version = WP_BUILDER_MIN_PHP_VERSION;
		self::$min_wordpress_version = WP_BUILDER_MIN_WP_VERSION;

		$requirements = self::check_requirements();
		if ( ! $requirements ) {
			die( printf( __( 'WP Builder not activated!  Your site does not meet the minimum requirements.  Minimum requirements: PHP version %1s1, WordPress version %2s2', 'wp-builder' ), WP_BUILDER_MIN_WP_VERSION, WP_BUILDER_MIN_WP_VERSION ) );
		}
		$set_up = self::set_up();
	}

	/**
	 * Setup Plugin.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private static function set_up() {
		$sidebars = get_option( 'wp_builder_sidebars' );
		if ( empty( $sidebars ) ) {
			add_option( 'wp_builder_sidebars', [ 'sidebars' ], '', 'yes' );
		}
		$post_types = get_option( 'wp_builder_post_types' );
		if ( empty( $post_types ) ) {
			add_option( 'wp_builder_post_types', [ 'post_types' ], '', 'yes' );
		}
		$menus = get_option( 'wp_builder_menu_locations' );
		if ( empty( $menus ) ) {
			add_option( 'wp_builder_menu_locations', [ 'menu_locations' ], '', 'yes' );
		}
		$taxonomies = get_option( 'wp_builder_taxonomies' );
		if ( empty( $taxonomies ) ) {
			add_option( 'wp_builder_taxonomies', [ 'taxonomies' ], '', 'yes' );
		}
		$metaboxes = get_option( 'wp_builder_metaboxes' );
		if ( empty( $metaboxes ) ) {
			add_option( 'wp_builder_metaboxes', [ 'metaboxes' ], '', 'yes' );
		}
		$customizer = get_option( 'wp_builder_customizer' );
		if ( empty( $customizer ) ) {
			add_option( 'wp_builder_customizer', [ 'customizer' ], '', 'yes' );
		}
		$customizer_panels = get_option( 'wp_builder_customizer_panels' );
		if ( empty( $customizer_panels ) ) {
			add_option( 'wp_builder_customizer_panels', [ 'panels' ], '', 'yes' );
		}
		$customizer_sections = get_option( 'wp_builder_customizer_sections' );
		if ( empty( $customizer_sections ) ) {
			add_option( 'wp_builder_customizer_sections', [ 'sections' ], '', 'yes' );
		}
		$customizer_settings = get_option( 'wp_builder_customizer_settings' );
		if ( empty( $customizer_settings ) ) {
			add_option( 'wp_builder_customizer_settings', [ 'settings' ], '', 'yes' );
		}
		$notices = get_option( 'wp_builder_notices' );
		if ( empty( $notices ) ) {
			add_option( 'wp_builder_notices', [], '', 'yes' );
		}
	}

	/**
	 * Check requirements.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return bool
	 */
	private static function check_requirements() {
		$php = self::check_php_version();
		if ( ! $php ) {
			return false;
		}
		$wordpress = self::check_wordpress_version();
		if ( ! $wordpress ) {
			return false;
		}
		return true;
	}

	/**
	 * Check PHP Version.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return bool
	 */
	private static function check_php_version() {
		$current_version = phpversion();
		return version_compare( $current_version, self::$min_php_version, '>=' );
	}

	/**
	 * Check WordPress Version.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return bool
	 */
	private static function check_wordpress_version() {
		global $wp_version;
		if ( $wp_version < self::$min_wordpress_version ) {
			return false;
		}
		return true;
	}

}
