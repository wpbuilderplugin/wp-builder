<?php
/**
 * Customizer Manager.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}


use WpBuilder\Utilities;

/**
 * Contains CustomizerManager class.
 *
 * Component definition.
 * panel = [
 *  'id' => int,
 *  'machine_name' => string,
 *  'title' => string,
 *  'priority' => int,
 *  'description' => string
 * ]
 * section = [
 *  'id' => int,
 *  'machine_name' => string,
 *  'title' => string,
 *  'priority' => int,
 *  'panel' => string,
 '  'description' => string,
 * ]
 * setting = [
 *  'id' => int,
 '  'machine_name' => string,
 '  'title' => string,
 *  'type' => string,
 *  'default' => string,
 *  'control' => [
 *    'label' => string,
 *    'description' => string,
 *    'section' => string,
 *    'type' => string,
 *    'setting' => array,,
 *  ],
 * ]
 */
class CustomizerManager {

	/**
	 * Customizer Settings Types.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $setting_types
	 */
	protected $setting_types;

	/**
	 * Panels.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $panels
	 */
	protected $panels;

	/**
	 * Sections.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $sections
	 */
	protected $sections;

	/**
	 * Bad strings.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $bad_strings
	 */
	protected $bad_strings;

	/**
	 * Constructs a new \WpBuilder\CustomizerManager object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->setting_types = $this->set_setting_types();
	}

	/**
	 * Register Customizer.
	 *
	 * @since 2.0.0
	 * @param object $wp_customize WP_Customize object.
	 */
	public function register_customizer( $wp_customize ) {
		$panels = $this->get_stored_panels();
		$sections = $this->get_stored_sections();
		$settings = $this->get_stored_settings();

		if ( ! empty( $panels ) ) {
			foreach ( $panels as $panel_id => $panel ) {
				$wp_customize->add_panel( $panel_id, $panel );
			}
		}
		if ( ! empty( $sections ) ) {
			foreach ( $sections as $section_id => $section ) {
				if ( '_none' == $section['panel'] ) {
					unset( $section['panel'] );
				}
				$wp_customize->add_section( $section_id, $section );
			}
		}
		if ( ! empty( $settings ) ) {
			foreach ( $settings as $setting ) {
				$control = [];
				if ( isset( $setting['machine_name'] ) ) {
					$setting_id = $setting['machine_name'];
					unset( $setting['machine_name'] );
				}
				if ( isset( $setting['control'] ) ) {
					$control = $setting['control'];
					unset( $setting['control'] );
				}
				$wp_customize->add_setting( $setting_id, $setting );
				if ( ! empty( $control ) ) {
					if ( array_key_exists( 'choices', $control ) ) {
						$choices = [];
						foreach ( $control['choices'] as $key => $value ) {
							$choices[ $value['value'] ] = $value['label'];
						}
						$control['choices'] = $choices;
					}
					$wp_customize->add_control( $setting_id, $control );
				}
			}
		}
	}

	/**
	 * Array of customizer field types.
	 *
	 * @since 2.0.0
	 *
	 * @return array $types.
	 */
	public function field_types() {
		$types = [
			'text' => [
				'label' => __( 'Text', 'wp-builder' ),
				'sanitize_callback' => '',
				'settings' => [
					'input_attrs' => [
						'label' => __( 'Input Attributes', 'wp-builder' ),
					],
				],
			],
			'url' => [
				'label' => __( 'URL', 'wp-builder' ),
				'sanitize_callback' => 'esc_url',
				'settings' => [
					'input_attrs' => [
						'label' => __( 'Input Attributes', 'wp-builder' ),
					],
				],
			],
			'email' => [
				'label' => __( 'Email', 'wp-builder' ),
				'sanitize_callback' => 'sanitize_email',
				'settings' => [
					'input_attrs' => [
						'label' => __( 'Input Attributes', 'wp-builder' ),
					],
				],
			],
			'textarea' => [
				'label' => __( 'Textarea', 'wp-builder' ),
				'sanitize_callback' => 'esc_textarea',
				'settings' => [
					'input_attrs' => [
						'label' => __( 'Input Attributes', 'wp-builder' ),
					],
				],
			],
			'date' => [
				'label' => __( 'Date', 'wp-builder' ),
				'sanitize_callback' => '',
				'settings' => [
					'input_attrs' => [
						'label' => __( 'Input Attributes', 'wp-builder' ),
					],
				],
			],
			'range' => [
				'label' => __( 'Range', 'wp-builder' ),
				'sanitize_callback' => 'intval',
				'settings' => [
					'input_attrs' => [
						'label' => __( 'Input Attributes', 'wp-builder' ),
						'default_attributes' => [
							'min' => [
								'label' => __( 'Minimum', 'wp-builder' ),
								'field_type' => 'number',
							],
							'max' => [
								'label' => __( 'Maximum', 'wp-builder' ),
								'field_type' => 'number',
							],
							'step' => [
								'label' => __( 'Step', 'wp-builder' ),
								'field_type' => 'number',
							],
						],
					],
				],
			],
			'number' => [
				'label' => __( 'Number', 'wp-builder' ),
				'sanitize_callback' => '',
				'settings' => [
					'input_attrs' => [
						'label' => __( 'Input Attributes', 'wp-builder' ),
						'default_attributes' => [
							'min' => [
								'label' => __( 'Minimum', 'wp-builder' ),
								'field_type' => 'number',
							],
							'max' => [
								'label' => __( 'Maximum', 'wp-builder' ),
								'field_type' => 'number',
							],
							'step' => [
								'label' => __( 'Step', 'wp-builder' ),
								'field_type' => 'number',
							],
						],
					],
				],
			],
			'tel' => [
				'label' => __( 'Telephone', 'wp-builder' ),
				'sanitize_callback' => '',
				'settings' => [
					'input_attrs' => [
						'label' => __( 'Input Attributes', 'wp-builder' ),
					],
				],
			],
			'checkbox' => [
				'label' => __( 'Checkbox', 'wp-builder' ),
				'sanitize_callback' => '',
				'settings' => [
					'single_value' => __( 'Value', 'wp-builder' ),
				],
			],
			'radio' => [
				'label' => __( 'Radio', 'wp-builder' ),
				'sanitize_callback' => '',
				'settings' => [ 'choices' => __( 'Choices', 'wp-builder' ) ],
			],
			'select' => [
				'label' => 'Select',
				'sanitize_callback' => '',
				'settings' => [ 'choices' => __( 'Choices', 'wp-builder' ) ],
			],
		];
		return $types;
	}

	/**
	 * Core Panels.
	 *  Panels provided by WordPress core.
	 *
	 * @since 2.0.0
	 *
	 * @return array $core_panels
	 */
	public function core_panels() {
		$core_panels = [
			'nav_menus' => [
				'title' => 'Menus',
				'priority' => 100,
			],
			'widgets' => [
				'title' => 'Widgets',
				'priority' => 110,
			],
		];
		return $core_panels;
	}

	/**
	 * Core Sections.
	 *  Sections provided by WordPress core.
	 *
	 * @since 2.0.0
	 *
	 * @return array $core_sections
	 */
	public function core_sections() {
		$core_sections = [
			'title_tagline' => [
				'title' => 'Site Title & Tagline',
				'priority' => 20,
			],
			'colors' => [
				'title' => 'Colors',
				'priority' => 40,
			],
			'header_image' => [
				'title' => 'Header Image',
				'priority' => 60,
			],
			'background_image' => [
				'title' => 'Background Image',
				'priority' => 80,
			],
			'static_front_page' => [
				'title' => 'Static Front Page',
				'priority' => 120,
			],
			'additional_css' => [
				'title' => 'Additional CSS',
				'priority' => 200,
			],
		];
		return $core_sections;
	}

	/**
	 * Get all panels.
	 *
	 * @since 2.0.0
	 * @TODO: This function does not work.
	 *
	 * @return array $panels
	 */
	public function get_all_panels() {
		$stored_panels = $this->get_stored_panels();
		if ( ! empty( $stored_panels ) ) {
			$panels = array_merge( $stored_panels, $this->core_panels() );
		} else {
			$panels = $this->core_panels();
		}
		return $panels;
	}

	/**
	 * Get all sections.
	 *
	 * @since 2.0.0
	 *
	 * @return array $sections;
	 */
	public function get_all_sections() {
		$stored_sections = $this->get_stored_sections();
		if ( ! empty( $stored_sections ) ) {
			$sections = array_merge( $stored_sections, $this->core_sections() );
		} else {
			$sections = $this->core_sections();
		}
		return $sections;
	}

	/**
	 * Get Stored Panels.
	 *
	 * @since 2.0.0
	 *
	 * @return array $panels;
	 */
	public function get_stored_panels() {
		$stored_panels = get_option( 'wp_builder_customizer_panels' );
		if ( is_array( $stored_panels ) && array_key_exists( 'panels', $stored_panels ) ) {
			$panels = unserialize( $stored_panels['panels'] );
			return $panels;
		}
		return [];
	}
	/**
	 * Get Stored Sections.
	 *
	 * @since 2.0.0
	 *
	 * @return array $sections.
	 */
	public function get_stored_sections() {
		$stored_sections = get_option( 'wp_builder_customizer_sections' );
		if ( is_array( $stored_sections ) && array_key_exists( 'sections', $stored_sections ) ) {
			$sections = unserialize( $stored_sections['sections'] );
			return $sections;
		}
		return [];
	}

	/**
	 * Get Stored Settings.
	 *
	 * @since 2.0.0
	 *
	 * @return array $settings
	 */
	public function get_stored_settings() {
		$stored_settings = get_option( 'wp_builder_customizer_settings' );
		if ( is_array( $stored_settings ) && array_key_exists( 'settings', $stored_settings ) ) {
			$settings = unserialize( $stored_settings['settings'] );
			return $settings;
		}
		return [];
	}

	/**
	 * Get Panel.
	 *
	 * @since 2.0.0
	 * @param int $panel_id The ID of the panel to retrieve.
	 *
	 * @return array $panel
	 */
	public function get_panel_by_id( $panel_id ) {
		$panels = $this->get_all_panels();
		foreach ( $panels as $key => $value ) {
			if ( $key == $panel_id ) {
				return $value;
			}
		}
		return [];
	}

	/**
	 * Get Section by Id.
	 *
	 * @since 2.0.0
	 * @param int $section_id The ID of the section to retrieve.
	 *
	 * @return array $section
	 */
	public function get_section_by_id( $section_id ) {
		$sections = $this->get_all_sections();
		foreach ( $sections as $key => $value ) {
			if ( $key == $section_id ) {
				return $value;
			}
		}
		return [];
	}

	/**
	 * Get Setting by Id.
	 *
	 * @since 2.0.0
	 * @param int $setting_id The ID of the setting to retrieve.
	 *
	 * @return array $setting
	 */
	public function get_setting_by_id( $setting_id ) {
		$settings = $this->get_stored_settings();
		foreach ( $settings as $key => $value ) {
			if ( $key == $setting_id ) {
				return $value;
			}
		}
		return [];
	}

	/**
	 * Get Panel Next ID
	 *
	 * @since 2.0.0
	 * @param str $component The component type.
	 *
	 * @return int $id
	 */
	public function next_id( $component ) {
		$ids = [];
		switch ( $component ) {
			case 'panel':
				$panels = $this->get_stored_panels();
				$ids = array_keys( $panels );
				break;
			case 'section':
				$sections = $this->get_stored_sections();
				$ids = array_keys( $sections );
				break;
			case 'setting':
				$settings = $this->get_stored_settings();
				$ids = array_keys( $settings );
				break;
		}
		if ( ! empty( $ids ) ) {
			// sort by value, highest first, then get the first one.
			rsort( $ids );
			reset( $ids );
			return $ids[0] + 1;
		} else {
			return 0;
		}
	}

	/**
	 * Import panels.
	 *
	 * @since 2.0.0
	 * @param array  $new_panels The new panels.
	 * @param string $strategy   The strategy to apply.
	 *
	 * @return bool $success
	 */
	public function import_panels( $new_panels, $strategy = 'overwrite' ) {
		$stored_panels = $this->get_stored_panels();
		$success = false;
		$panels = [];
		foreach ( $new_panels as $key => $properties ) {
			$valid = $this->validate_panel( $properties );
			if ( $valid ) {
				$panels[] = Utilities::sanitizeArray( $properties );
			}
		}
		if ( ! empty( $panels ) ) {
			switch ( $strategy ) {
				case 'delete':
					$stored_panels = $panels;
					$success = $this->save_panels( $stored_panels );
					break;
				case 'overwrite':
					if ( empty( $stored_panels ) ) {
						$success = $this->save_panels( $panels );
					} else {
						foreach ( $panels as $panel_key => $panel ) {
							$matched = false;
							foreach ( $stored_panels as $sp_key => $sp_values ) {
								if ( $panel == $sp_values ) {
									return true;
								}
								if ( $sp_values['machine_name'] == $panel['machine_name'] ) {
									$matched = true;
									$stored_panels[ $sp_key ] = $panel;
								}
							}
							if ( ! $matched ) {
								$stored_panels[] = $panel;
							}
						}
						$this->save_panels( $stored_panels );
					}
					break;
				case 'add':
					if ( empty( $stored_panels ) ) {
						$success = $this->save_panels( $panel );
					} else {
						foreach ( $panels as $panel_key => $panel ) {
							$matched = false;
							foreach ( $stored_panels as $sp_key => $sp_values ) {
								if ( $panel == $sp_values ) {
									return true;
								}
								if ( $sp_values['machine_name'] == $panel['machine_name'] ) {
									$matched = true;
								}
							}
							if ( ! $matched ) {
								$stored_panels[] = $panel;
							}
						}
						$this->save_panels( $stored_panels );
					}
					break;
			}
		}
		return $success;
	}

	/**
	 * Validate panel.
	 *
	 * @since 2.0.0
	 * @param array $panel The panel to validate.
	 *
	 * @return bool
	 */
	private function validate_panel( $panel ) {
		if ( ! isset( $panel['machine_name'] ) || empty( $panel['machine_name'] ) ) {
			return false;
		}
		return true;
	}

	/**
	 * Save panel.
	 *
	 * @since 2.0.0
	 * @param array $panel The panel to save.
	 *
	 * @return bool
	 */
	public function save_panel( $panel ) {
		$stored_panels = $this->get_stored_panels();
		$panel = Utilities::sanitizeArray( $panel );
		if ( ! empty( $stored_panels ) && array_key_exists( key( $panel ), $stored_panels ) ) {
			if ( $stored_panels[ key( $panel ) ] == $panel[ key( $panel ) ] ) {
				return true;
			}
			$stored_panels[ key( $panel ) ] = $panel[ key( $panel ) ];
		} else {
			$stored_panels[ key( $panel ) ] = $panel[ key( $panel ) ];
		}
		return $this->save_panels( $stored_panels );
	}

	/**
	 * Save Panels.
	 *  Use for bulk operations.
	 *
	 * @since 2.0.0
	 * @param array $panels The array of panels to save.
	 *
	 * @return bool
	 */
	private function save_panels( $panels ) {
		$save_settings['panels'] = serialize( $panels );
		return update_option( 'wp_builder_customizer_panels', $save_settings );
	}

	/**
	 * Import sections.
	 *
	 * @since 2.0.0
	 * @param array  $new_sections Array of new sections.
	 * @param string $strategy     The strategy to apply.
	 *
	 * @return bool $success
	 */
	public function import_sections( $new_sections, $strategy = 'overwrite' ) {
		$stored_sections = $this->get_stored_sections();
		$success = false;
		$sections = [];
		foreach ( $new_sections as $key => $properties ) {
			$valid = $this->validate_section( $properties );
			if ( $valid ) {
				$sections[] = $properties;
			}
		}
		if ( ! empty( $sections ) ) {
			switch ( $strategy ) {
				case 'delete':
					$success = $this->save_sections( $sections );
					break;
				case 'overwrite':
					if ( empty( $stored_sections ) ) {
						$success = $this->save_sections( $sections );
					} else {
						foreach ( $sections as $section ) {
							$matched = false;
							foreach ( $stored_sections as $ss_key => $ss_values ) {
								if ( $section == $ss_values ) {
									return true;
								}
								if ( $ss_values['machine_name'] == $section['machine_name'] ) {
									$matched = true;
									$stored_sections[ $ss_key ] = $section;
								}
							}
							if ( ! $matched ) {
								$stored_sections[] = $section;
							}
						}
						$success = $this->save_sections( $stored_sections );
					}
					break;
				case 'add':
					if ( empty( $stored_sections ) ) {
						$success = $this->save_sections( $sections );
					} else {
						foreach ( $sections as $section ) {
							$matched = false;
							foreach ( $stored_sections as $ss_key => $ss_values ) {
								if ( $section == $ss_values ) {
									return true;
								}
								if ( $ss_values['machine_name'] == $section['machine_name'] ) {
									$matched = true;
								}
							}
							if ( ! $matched ) {
								$stored_sections[] = $section;
							}
						}
						$success = $this->save_sections( $stored_sections );
					}
					break;
			}
		}
		return $success;
	}

	/**
	 * Save section.
	 *  Use to save a single section.
	 *
	 * @since 2.0.0
	 * @param array $section The section to save.
	 *
	 * @return bool
	 */
	public function save_section( $section ) {
		$stored_sections = $this->get_stored_sections();
		$section = Utilities::sanitizeArray( $section );
		if ( ! empty( $stored_sections ) && array_key_exists( key( $section ), $stored_sections ) ) {
			if ( $stored_sections[ key( $section ) ] == $section[ key( $section ) ] ) {
				return true;
			}
			$stored_sections[ key( $section ) ] = $section[ key( $section ) ];
		} else {
			$stored_sections[ key( $section ) ] = $section[ key( $section ) ];
		}
		return $this->save_sections( $stored_sections );
	}

	/**
	 * Validate section.
	 *
	 * @since 2.0.0
	 * @access private
	 * @param array $section The section to validate.
	 *
	 * @return bool
	 */
	private function validate_section( $section ) {
		if ( ! isset( $section['machine_name'] ) || empty( $section['machine_name'] ) ) {
			return false;
		}
		return true;
	}

	/**
	 * Save sections.
	 *  Use for bulk operations.
	 *
	 * @since 2.0.0
	 * @access private
	 * @param array $sections The array of sections to save.
	 *
	 * @return bool
	 */
	private function save_sections( $sections ) {
		$save_settings['sections'] = serialize( $sections );
		return update_option( 'wp_builder_customizer_sections', $save_settings );
	}

	/**
	 * Import settings.
	 *
	 * @since 2.0.0
	 * @param array  $new      The array of settings to import.
	 * @param string $strategy The strategy to use.
	 *
	 * @return bool $success
	 */
	public function import_settings( $new, $strategy = 'overwrite' ) {
		$stored = $this->get_stored_settings();
		$success = false;
		$settings = [];
		foreach ( $new as $properties ) {
			$valid = $this->validate_setting( $properties );
			if ( $valid ) {
				$settings[] = Utilities::sanitizeArray( $properties );
			}
		}
		if ( ! empty( $settings ) ) {
			switch ( $strategy ) {
				case 'delete':
					$success = $this->save_settings( $settings );
					break;
				case 'overwrite':
					if ( empty( $stored ) ) {
						$success = $this->save_settings( $settings );
					} else {
						foreach ( $settings as $setting ) {
							$matched = false;
							foreach ( $stored as $key => $values ) {
								if ( $setting == $values ) {
									return true;
								}
								if ( $values['machine_name'] == $stting['machine_name'] ) {
									$matched = true;
									$stored[ $key ] = $setting;
								}
							}
							if ( ! $matched ) {
								$stored[] = $setting;
							}
						}
						$success = $this->save_setting( $stored );
					}
					break;
				case 'add':
					if ( empty( $stored ) ) {
						$success = $this->save_settings( $settings );
					} else {
						foreach ( $settings as $setting ) {
							$matched = false;
							foreach ( $stored as $key => $values ) {
								if ( $setting == $values ) {
									return true;
								}
								if ( $values['machine_name'] == $setting['machine_name'] ) {
									$matched = true;
								}
							}
							if ( ! $matched ) {
								$stored[] = $setting;
							}
						}
						$success = $this->save_settings( $stored );
					}
					break;
			}
		}
		return $success;
	}

	/**
	 * Validate setting.
	 *
	 * @since 2.0.0
	 * @param array $setting The setting to validate.
	 *
	 * @return bool
	 */
	public function validate_setting( $setting ) {
		if ( ! isset( $setting['machine_name'] ) || empty( $setting['machine_name'] ) ) {
			return false;
		}
		return true;
	}

	/**
	 * Save setting.
	 *
	 * @since 2.0.0
	 * @param array $setting The setting to save.
	 *
	 * @return bool
	 */
	public function save_setting( $setting ) {
		$stored_settings = $this->get_stored_settings();
		$setting = Utilities::sanitizeArray( $setting );
		if ( ! empty( $stored_settings ) && array_key_exists( key( $setting ), $stored_settings ) ) {
			if ( $stored_settings[ key( $setting ) ] == $setting[ key( $setting ) ] ) {
				return true;
			}
			$stored_settings[ key( $setting ) ] = $setting[ key( $setting ) ];
		} else {
			$stored_settings[ key( $setting ) ] = $setting[ key( $setting ) ];
		}
		return $this->save_settings( $stored_settings );
	}

	/**
	 * Save setings.
	 *  Use for bulk operations.
	 *
	 * @since 2.0.0
	 * @param array $settings The array of settings to save.
	 *
	 * @return bool
	 */
	private function save_settings( $settings ) {
		$save_settings['settings'] = serialize( $settings );
		return update_option( 'wp_builder_customizer_settings', $save_settings );
	}

	/**
	 * Delete Panel.
	 *
	 * @since 2.0.0
	 * @param array $panel The panel to delete.
	 *
	 * @return bool
	 */
	public function delete_panel( $panel ) {
		$stored_panels = $this->get_stored_panels();
		foreach ( $stored_panels as $sp_key => $properties ) {
			if ( $properties['machine_name'] == $panel['machine_name'] ) {
				unset( $stored_panels[ $sp_key ] );
			}
		}
		return $this->save_panels( $stored_panels );
	}

	/**
	 * Delete Section.
	 *
	 * @since 2.0.0
	 * @param array $section The Section to delete.
	 *
	 * @return bool $success
	 */
	public function delete_section( $section ) {
		$stored_sections = $this->get_stored_sections();
		$success = false;
		foreach ( $stored_sections as $ss_key => $properties ) {
			if ( $properties['machine_name'] == $section['machine_name'] ) {
				$success = true;
				unset( $stored_sections[ $ss_key ] );
			}
		}
		$updated = $this->save_sections( $stored_sections );
		if ( ! $updated ) {
			$success = $updated;
		}
		return $success;
	}

	/**
	 * Delete Setting.
	 *
	 * @since 2.0.0
	 * @param array $setting The setting to delete.
	 *
	 * @return bool $success
	 */
	public function delete_setting( $setting ) {
		$stored_settings = $this->get_stored_settings();
		$success = false;
		foreach ( $stored_settings as $ss_key => $properties ) {
			if ( $properties['machine_name'] == $setting['machine_name'] ) {
				$success = true;
				unset( $stored_settings[ $ss_key ] );
			}
		}
		$updated = $this->save_settings( $stored_settings );
		if ( ! $updated ) {
			$success = $updated;
		}
		return $success;
	}

	/**
	 * Set Customizer setting types.
	 *
	 * @since 2.0.0
	 *
	 * @return array $setting_types
	 */
	public function set_setting_types() {
		$setting_types = [
			'option' => __( 'Option', 'wp-builder' ),
			'theme_mod' => __( 'Theme Mod', 'wp-builder' ),
		];
		return $setting_types;
	}

	/**
	 * Get Customizer setting types.
	 *
	 * @since 2.0.0
	 *
	 * @return array $setting_types
	 */
	public function get_setting_types() {
		return $this->setting_types;
	}

	/**
	 * Input attribute types.
	 *
	 * @since 2.0.0
	 * @param string $attribute The attribute type.
	 *
	 * @return array $attribute_types;
	 */
	public function input_attribute_types( $attribute = '' ) {
		$attribute_types = [
			'class' => [
				'label' => 'Class',
				'field_type' => 'text',
				'required' => false,
			],
			'placeholder' => [
				'label' => 'Placeholder',
				'field_type' => 'text',
				'required' => false,
			],
		];
		if ( array_key_exists( $attribute, $attribute_types ) ) {
			return $attribute_types[ $attribute ];
		}
		return $attribute_types;
	}
}
