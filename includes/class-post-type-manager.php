<?php
/**
 * Manages Post Types.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

use WpBuilder\Utilities;

/**
 * The PostTypeManager class.
 */
class PostTypeManager {

	/**
	 * Post types.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $post_types
	 */
	protected $post_types;

	/**
	 * Constructs a new PostTypes object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->post_types = $this->set_post_types();
	}
	/**
	 * Register post types
	 *
	 * @since 2.0.0
	 */
	public function register_post_types() {
		$post_types = $this->post_types;
		if ( ! empty( $post_types ) ) {
			foreach ( $post_types as $post_type ) {
				$args = [
					'labels' => [
						'name' => $post_type['labels']['name'],
						'singular_name' => $post_type['labels']['singular_name'],
					],
					'public' => ( isset( $post_type['public'] ) ? true : false ),
					'has_archive' => ( isset( $post_type['has_archive'] ) ? $post_type['has_archive'] : false ),
					'show_in_rest' => ( isset( $post_type['show_in_rest'] ) ? $post_type['show_in_rest'] : false ),
				];
				if ( array_key_exists( 'rewrite', $post_type ) ) {
					$post_type['rewrite']['with_front'] = false;
					$args['rewrite'] = $post_type['rewrite'];
				}
				if ( array_key_exists( 'taxonomies', $post_type ) && is_array( $post_type['taxonomies'] ) ) {
					$args['taxonomies'] = $post_type['taxonomies'];
				}
				if ( is_array( $post_type['description'] ) ) {
					$args['description'] = $post_type['description'];
				}
				if ( is_array( $post_type['supports'] ) ) {
					$args['supports'] = $post_type['supports'];
				}
				if ( isset( $post_type['hierarchical'] ) && $post_type['hierarchical'] == true ) {
					$args['hierarchical'] = true;
				}
				if ( isset( $post_type['capability_type'] ) ) {
					$args['capability_type'] = $post_type['capability_type'];
				}
				if ( isset( $post_type['map_meta_cap'] ) ) {
					if ( ! $post_type['map_meta_cap'] ) {
						$args['map_meta_cap'] = false;
					}
					if ( array_key_exists( 'capabilities', $post_type ) && is_array( $post_type['capabilities'] ) ) {
						foreach ( $post_type['capabilities'] as $c => $cap ) {
							$args['capabilities'][ $cap['key'] ] = $cap['value'];
						}
					}
				}
				register_post_type( $post_type['machine_name'], $args );
			}
		}
	}

	/**
	 * Supported features.
	 *
	 * @since 2.0.0
	 *
	 * @return array $features
	 */
	public function get_supported_features() {
		return [
			'title' => __( 'Title', 'wp-builder' ),
			'editor' => __( 'Editor', 'wp-builder' ),
			'thumbnail' => __( 'Thumbnail', 'wp-builder' ),
			'custom-fields' => __( 'Custom Fields', 'wp-builder' ),
			'page-attributes' => __( 'Page Attributes', 'wp-builder' ),
			'excerpt' => __( 'Excerpts', 'wp-builder' ),
			'comments' => __( 'Comments', 'wp-builder' ),
		];
	}
	/**
	 * Rewrites for custom post types.
	 *
	 * @since 2.0.0
	 */
	public function rewrites() {
		$post_types = $this->post_types;
		if ( ! empty( $post_types ) ) {
			foreach ( $post_types as $post_type ) {
				if ( array_key_exists( 'has_archive', $post_type )
				  && $post_type['has_archive'] == 1
				  && array_key_exists( 'rewrite', $post_type ) ) {
					add_rewrite_rule(
						$post_type['rewrite']['slug'] . '/([0-9]{4})/([0-9]{1,2})/?$',
						'index.php?post_type=' . $post_type['rewrite']['slug'] . '&year=$matches[1]&monthnum=$matches[2]',
						'top'
					);
					add_rewrite_rule(
						$post_type['rewrite']['slug'] . '/([0-9]{4})/?$',
						'index.php?post_type=' . $post_type['rewrite']['slug'] . '&year=$matches[1]',
						'top'
					);
				}
			}
		}
	}

	/**
	 * Delete single post type.
	 *
	 * @since 2.0.0
	 * @param string $item The post_type to delete.
	 *
	 * @return bool
	 */
	public function delete_single( $item ) {
		$post_types = $this->post_types;
		$ordered = [];
		$i = 0;
		foreach ( $post_types as $post_type => $properties ) {
			if ( $properties['unique_id'] == $item ) {
				unset( $post_types[ $post_type ] );
			} else {
				$ordered[ $i ] = $post_types[ $post_type ];
				++$i;
			}
		}
		return $this->save( $ordered );
	}

	/**
	 * Update post_types
	 *
	 * @since 2.0.0
	 * @param array  $new_post_types The array of the new post type.
	 * @param string $action         The action to use.
	 *
	 * @return bool $success
	 */
	public function update( $new_post_types, $action = 'add' ) {
		$stored_post_types = $this->post_types;
		$post_types = [];
		$success = false;
		foreach ( $new_post_types as $post_type => $properties ) {
			if ( $this->validate( $properties ) ) {
				$post_types[ $post_type ] = Utilities::sanitizeArray( $properties );
			}
		}
		switch ( $action ) {
			case 'delete':
				$success = $this->save( $post_types );
				break;
			case 'add':
				if ( empty( $stored_post_types ) ) {
					$success = $this->save( $post_types );
				} else {
					foreach ( $post_types as $pt_key => $pt_val ) {
						$matched = false;
						foreach ( $stored_post_types as $spt_key => $spt_val ) {
							if ( $pt_val == $spt_val ) {
								return true;
							}
							if ( $pt_val['unique_id'] == $spt_val['unique_id'] ) {
								$matched = true;
							}
						}
						if ( ! $matched ) {
							$stored_post_types[] = $pt_val;
						}
					}
					$success = $this->save( $stored_post_types );
				}
				break;
			case 'overwrite':
				if ( empty( $stored_post_types ) ) {
					$success = $this->save( $post_types );
				} else {
					foreach ( $post_types as $pt_key => $pt_val ) {
						$matched = false;
						foreach ( $stored_post_types as $spt_key => $spt_val ) {
							if ( $pt_val == $spt_val ) {
								return true;
							}
							if ( $pt_val['unique_id'] == $spt_val['unique_id'] ) {
								$matched = true;
								$stored_post_types[ $spt_key ] = $pt_val;
							}
						}
						if ( ! $matched ) {
							$stored_post_types[] = $pt_val;
						}
					}
					$success = $this->save( $stored_post_types );
				}
				break;
		}
		return $success;
	}

	/**
	 * Validate post_type
	 *
	 * @since 2.0.0
	 * @param array $post_type The post type to validate.
	 *
	 * @return bool
	 */
	public function validate( $post_type ) {
		if ( ! isset( $post_type['machine_name'] ) || strlen( $post_type['machine_name'] ) < 1 ) {
			return false;
		}
		return true;
	}

	/**
	 * Save post types.
	 *
	 * @since 2.0.0
	 * @param array $post_types The post_types to save.
	 *
	 * @return bool $success
	 */
	public function save( $post_types ) {
		$save_settings['post_types'] = serialize( $post_types );
		return update_option( 'wp_builder_post_types', $save_settings );
	}

	/**
	 * Set post_types.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return array $post_types
	 */
	private function set_post_types() {
		$post_types = [];
		$pt = get_option( 'wp_builder_post_types' );
		if ( ! empty( $pt ) && isset( $pt['post_types'] ) && gettype( $pt['post_types'] ) == 'string' ) {
			$options = unserialize( $pt['post_types'] );
			if ( is_array( $options ) ) {
				$post_types = $options;
			}
		}
		return $post_types;
	}

	/**
	 * Get post_types.
	 *
	 * @since 2.0.0
	 *
	 * @return array $post_types
	 */
	public function get_post_types() {
		return $this->post_types;
	}

}
