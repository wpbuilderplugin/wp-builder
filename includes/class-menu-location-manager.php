<?php
/**
 * Managers Menu Locations.
 *
 * @since   2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

use WpBuilder\Utilities;

/**
 * Contains class MenuLocationManager.
 *
 * @since 2.0.0
 */
class MenuLocationManager {
	/**
	 * Menu locations.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var array $menu_locations
	 */
	protected $menu_locations;

	/**
	 * Constructs a new MenuLocationManager object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->menu_locations = $this->set_menu_locations();
	}

	/**
	 * Register menu locations.
	 *
	 * @since 0.0.1
	 */
	public function register_menu_locations() {
		$locations = $this->menu_locations;
		if ( ! empty( $locations ) ) {
			$menu_locations = [];
			foreach ( $locations as $location ) {
				if ( isset( $location['machine_name'] ) && ! empty( $location['machine_name'] ) && isset( $location['name'] ) && ! empty( $location['name'] ) ) {
					$menu_locations[ $location['machine_name'] ] = $location['name'];
				}
			}
			if ( ! empty( $menu_locations ) ) {
				register_nav_menus( $menu_locations );
			}
		}
	}

	/**
	 * Delete single menu location.
	 *
	 * @since 2.0.0
	 * @param array $item The menu location item to delete.
	 *
	 * @return bool
	 */
	public function delete_single( $item ) {
		$menu_locations = $this->menu_locations;
		$ordered = [];
		$i = 0;
		foreach ( $menu_locations as $menu_location => $properties ) {
			if ( $properties['machine_name'] == $item ) {
				unset( $menu_locations[ $menu_location ] );
			} else {
				$ordered[ $i ] = $menu_locations[ $menu_location ];
				++$i;
			}
		}
		return $this->save( $ordered );
	}

	/**
	 * Update menu_locations.
	 *
	 * @since 2.0.0
	 * @param array  $new_menu_locations The menu location item to add / update.
	 * @param string $action             The action to perform when add / updating.
	 *
	 * @return bool $success;
	 */
	public function update( $new_menu_locations, $action = 'add' ) {
		$stored_menu_locations = $this->menu_locations;
		$menu_locations = [];
		$success = false;
		foreach ( $new_menu_locations as $menu_location => $properties ) {
			if ( $this->validate( $properties ) ) {
				$menu_locations[ $menu_location ] = Utilities::sanitizeArray( $properties );
			}
		}
		switch ( $action ) {
			case 'delete':
				$success = $this->save( $menu_locations );
				break;
			case 'add':
				if ( empty( $stored_menu_locations ) ) {
					$success = $this->save( $menu_locations );
				} else {
					foreach ( $menu_locations as $ml_key => $ml_val ) {
						$matched = false;
						foreach ( $stored_menu_locations as $sml_key => $sml_val ) {
							// Short cut results if there is no change.
							if ( $ml_val == $sml_val ) {
								return true;
							}
							if ( $ml_val['machine_name'] == $sml_val['machine_name'] ) {
								$matched = true;
							}
						}
						if ( ! $matched ) {
							$stored_menu_locations[] = $ml_val;
						}
					}
					$success = $this->save( $stored_menu_locations );
				}
				break;
			case 'overwrite':
				if ( empty( $stored_menu_locations ) ) {
					$success = $this->save( $menu_locations );
				} else {
					foreach ( $menu_locations as $ml_key => $ml_val ) {
						$matched = false;
						foreach ( $stored_menu_locations as $sml_key => $sml_val ) {
							if ( $ml_val == $sml_val ) {
								return true;
							}
							if ( $ml_val['machine_name'] == $sml_val['machine_name'] ) {
								$matched = true;
								$stored_menu_locations[ $sml_key ] = $ml_val;
							}
						}
						if ( ! $matched ) {
							$stored_menu_locations[] = $ml_val;
						}
					}
					$success = $this->save( $stored_menu_locations );
				}
				break;
		}
		return $success;
	}

	/**
	 * Validate menu_location
	 *
	 * @since 2.0.0
	 * @param array $menu_location The menu location to validate.
	 *
	 * @return bool
	 */
	public function validate( $menu_location ) {
		if ( ! isset( $menu_location['machine_name'] ) || strlen( $menu_location['machine_name'] ) < 1 ) {
			return false;
		}
		return true;
	}

	/**
	 * Save menu_locations.
	 *
	 * @since 2.0.0
	 * @param array $menu_locations The menu locations to save.
	 *
	 * @return bool
	 */
	public function save( $menu_locations ) {
		$save_settings['menu_locations'] = serialize( $menu_locations );
		return update_option( 'wp_builder_menu_locations', $save_settings );
	}

	/**
	 * Set menu locations.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 * @return array $menu_locations
	 */
	private function set_menu_locations() {
		$menu_locations = [];
		$ml = get_option( 'wp_builder_menu_locations' );
		if ( ! empty( $ml ) && isset( $ml['menu_locations'] ) ) {
			$options = unserialize( $ml['menu_locations'] );
			if ( is_array( $options ) ) {
				$menu_locations = $options;
			}
		}
		return $menu_locations;
	}

	/**
	 * Get Menu Locations.
	 *
	 * @since 2.0.0
	 *
	 * @return array $menu_locations
	 */
	public function get_menu_locations() {
		return $this->menu_locations;
	}
}
