
# WP Builder
## A nice way to build WordPress sites with less coding

This plugin adds some common tools that typically requires either coding, or one or more plugins, and puts the power into a site builder or themers hands.  

Please note, to leverage the power of WP Builder, you will need to be familiar with WordPress concepts such as Metaboxes and Post Types. 

** WARNING ** This is not a toy, this is a tool meant for developers and site builders with familiarity writing some PHP code, using WordPress APIs and themes.

## Use cases

A common problem when building a WordPress site is structured content.  WordPress offers two post types ( posts and pages ).  Often these are not great options for more specialized content.  It typically makes sense to define different post types for theming and adding structured content to post types.  Rather than referring to the WordPress Codex to recall all of the available options, WP Builder offers a simple form for you to create custom post types with a few clicks.

Another problem is classifying content.  Rather than repurposing WordPress core's Categories and Tags taxonomies, you may want to create custom taxonomies. Using WP Builder, you can create custom taxonomies and connect to any number of post types, even core WordPress post types.

WordPress core offers only a few options for structuring your content, a Title, Body, and a few other fields.  But what if you wanted to add "Ingredients" to a "Recipe" post type?  Using WP Builder, you can easily create a metabox on your Recipe post type with fields for collecting that information in a consistent manner.  It's up to the site builder to architect the structure and decide how it will be used.  

WP Builder makes it easy to create custom structured content with a few clicks.

## Installation

1. Install and activate CMB2.
2. Install and activate WP Builder.

## Basic Usage

Once the plugin is installed, you will have a new menu option on the dashboard WP Builder.  This will allow you to create various types of structures in WordPress.

## Demo

There is a [demo video](https://youtu.be/OG6H_pZtN7g) and a [repo containing the theme from the video](https://github.com/scottsawyer/ice-cream-eater).  There are some WP Builder configurations that can be imported for you to explore.

### Dashboard Tab

The "Dashboard" tab gives you an overview of the different structures you have created.  

Each structure has a configuration export button which shows a JSON string for the configuration.  You can use this JSON to move configuration from a "dev" or "staging" environment to "production".  If you use this feature, be careful if you alter the JSON, there is very little error checking at this time.

For each Metabox, there is a sample of the fields you added.  Not every field type offers a preview.  There is also a sample of how to retrieve the data from the field, both as a PHP snippet and a shortcode.  The shortcode is not "multi-value" field aware at this time.  In fact, the PHP snippet may need to be altered to display multi-value fields.  I would refer to the CMB2 docs on how to access those fields.

### Sidebars Tab

WordPress uses sidebars for widget placement.  Using the sidebar tab, you can create many sidebars.  Keep in mind, they do not automatically appear in your theme until you actually add them to your theme.  Something like:

```<?php dynamic_sidebar( $sidebar_name ); ?>```

Machine names are what you will refer to in your code as `$sidebar_name`.  For Machine Name, you should use basic characters, `0-9, a-z, _`.  Don't use other punctuation or spaces.  

### Post Types Tab

Post types have a variety of settings, but maybe not exhaustive.  For better understanding of what these settings mean, refer to the WordPress Codex https://codex.wordpress.org/Post_Types.  

One feature of custom Post Types created with WP Builder is the ability to add template selection to post types with Page attributes.  This is similar to the selection metabox on the built-in Page post type.  You must select Page Attributes in the Supports option for this to work.  You add your templates to your theme in a directory structure like:

`/wp-content/themes/theme_name/templates/post_type_machine_name/`

If you want to enable the Block Editor on a custom post type, be sure to select the 'Use In REST' option.

### Taxonomies Tab

Most of the options here are very similar to what is in the [WordPress Codex](https://codex.wordpress.org/Taxonomies).

### Metaxboxes Tab

Metaboxes allow you to add new properties to a post type.  The basic flow is this:

1. Create the metabox.
2. Add fields.
3. Save.

There are a ton of field options, for that I would look at CMB2 documentation.  At this time, WP Builder only supports the default field types that come with CMB2, but there are plenty of field types to choose from.

** Update ** The following custom field types are now supported!  
- [CMB2 Field Type: Address](https://github.com/scottsawyer/cmb2-field-address)
- [CMB2 Field Type: Link](https://github.com/scottsawyer/cmb2-field-link)
- [CMB2 Field Type: Widget Selector](https://github.com/scottsawyer/cmb2-field-widget-selector)
- [CMB2 Field Type: FontAwesome](https://github.com/serkanalgur/cmb2-field-faiconselect)
- [CMB2 Field Type: Post Search Ajax](https://github.com/alexis-magina/cmb2-field-post-search-ajax)

If you have a specific field type you want supported, please create an issue.

You can change the order in which the fields appear by dragging the fields.  It uses jQuery UI, so I don't know how well it works on mobile.  

** WARNING ** At this time, nested "Group" fields are not supported, though WP Builder will allow you to nest them.  There is an open issue in [CMB2](https://github.com/CMB2/CMB2/issues/565). 

### Menu Locations Tab

Also known as Theme Locations will allow you to assign different menus to locations in your theme.  It provides a code snippet like:

``` <?php wp_nav_menu( ['theme_location' => 'header_navigation'] ); ?> ```

You can just drop this into your theme.

### Customizer Tab

The Customizer tab allows creating Customizer Panels, Sections, and Settings.  Only WordPress core controls are currently supported.  

All Settings must be added to a Section in order for WordPress to render the controls.  You can either add a new Section or use one of WordPress core's Sections.

### Import Tab

~~Right now, only the "Paste Code" Input Method works.  Choose whether you want to overwrite existing configuration, otherwise it will append.  Append if you have only new configuration, otherwise, there might be name collisions, and we don't want that.~~

**UPDATE** Properly formatted JSON files with a file name \*.json is suppoerted. Please be aware, while there is some validation, it will broadly accept invalid configuration.  This can have negative consequences.  

You can paste JSON from the export configuration seen on the Dashboard tab.  The feature was designed to easily move configuration from a Dev to Production environment. It's best to export from an identical copy of the site with the same version of WP Builder on both instances. Be really careful if you alter the JSON by hand.  There is no warranty.

Have fun, and thank you for using WP Builder.

## To do

- [x] 2016-11-20 - Add new meta capabilities to post types.  Allow users to enter new meta caps.

- [x] 2016-11-29 - Add file import for Import settings.  Currently, you need to use JSON paste method.

- [x] 2016-12-02 - There is no name collision checking, so you need to ensure you don't create two post types with the same "machine name".  Same for Taxonomy, Metabox, and Sidebars.

- [] 2016-12-02 - Need some good "how to's" and code examples.

## Credits

This was built with the AWESOME [CMB2 project](https://github.com/WebDevStudios/CMB2).

Inspiration ( and a good bit of code co-opting ) for custom post type template selection from [Ronald Huereca](https://gist.github.com/ronalfy/2d957a861237d6207b14d2f7106b1327).

~~This does not use the WordPress CMB2 plugin, so you will need to clone it into wp_builder/inc/mb.  It's set up as a git submodule, so when you install this plugin, you should be able to run `git submodule update -init`.~~

** Update ** While this plugin does ship with CMB2, it is highly recommended you install the [CMB2 plugin](https://wordpress.org/plugins/cmb2/) from WordPress.org.