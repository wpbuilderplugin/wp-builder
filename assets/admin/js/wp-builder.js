( function ( $ ) {
	'use strict';

let totalFields = $('.sortable li').length;

// Sortable things.
$( '.sortable' ).each( function () {
	$( this ).sortable( { placeholder: "sortable-placeholder" } );
} );
$( '.sortable-field-options, .sortable-field-settings' ).each( function () {
	$( this ).sortable( { placeholder: 'sortable-placeholder' } );
} );
$( document ).on( 'click', '.wp_builder-meta-add-option', function () {
	$( '.sortable-field-options' ).each( function () {
		if ( $( this ).sortable( 'instance' ) ) {
			$( this ).sortable( 'destroy' ).sortable( { placeholder: 'sortable-placeholder' } );
		}
		else {
			$( this ).sortable( { placeholder: 'sortable-placeholder' } );
		}
	} );
} );
$( document ).on( 'click', '.wp-builder-add-group-fields', function () {
	$( '.group-fields-wrapper' ).each( function () {
		if ( $( this ).sortable( 'instance' ) ) {
			$( this ).sortable( 'destroy' ).sortable( { placeholder: 'sortable-placeholder' } );
		}
		else {
			$( this ).sortable( { placeholder: 'sortable-placeholder' } );
		}
	} );
} );
$( document ).on( 'click', '.add-control-setting', function () {
	$( '.sortable-field-settings' ).each( function () {
		if ( $( this ).sortable( 'instance' ) ) {
			$( this ).sortable( 'destroy' ).sortable( { placeholder: 'sortable-placeholder' } );
		}
		else {
			$( this ).sortable( { placeholder: 'sortable-placeholder' } );
		}
	} );
} );
// Accordion for accordion things.
$( document ).on( 'click', '.collapser', function ( e ) {
	e.preventDefault();
	let fieldSettingsContainer = $( this ).parents( '.metabox-field-legend').next( '.field-settings-container' );
	let requiredFields = fieldSettingsContainer.find( ':input[required]' );
	// Force valid input prior to closing accordion.
	if ( $( this ).hasClass( 'open' ) ) {
		let formValid = true;
		requiredFields.each ( function ( i, field ) {
			let result = field.checkValidity();
			if ( result === false ) {
				formValid = false;
				field.reportValidity();
			}
		} );
		if ( formValid === true ) {
			$( this ).toggleClass( 'open' ).parents( '.metabox-field-legend' ).next( '.field-settings-container' ).slideToggle();
		}
	}
	else {
		$( this ).toggleClass( 'open' ).parents( '.metabox-field-legend' ).next( '.field-settings-container' ).slideToggle();
	}
} );

$( '.wp_builder-accordion' ).accordion( {
	heightStyle: "content",
	collapsible: true,
	active: false
} );
$( '.export-field-container' ).hide();
$( '.show-export' ).on( 'click', function ( e ) {
	e.preventDefault();
	$( this ).parents( '.export--wrapper' ).toggleClass( 'open' ).find( '.export-field-container' ).toggle();
} );
	$( '.hide-export' ).on( 'click', function ( e ) {
	e.preventDefault();
	$( this ).parents( '.export--wrapper' ).toggleClass( 'open' ).find( '.export-field-container' ).toggle();
} );
$( '.select-export' ).on( 'click', function ( e ) {
	e.preventDefault();
	$( this ).parents( '.export--wrapper' ).find( '.export-field' ).select();
} );

// Importer.  
if ( !$( '#method-upload' ).is( ':checked' ) ) {
	$( '.method-upload' ).hide();
}
if ( !$( '#method-paste' ).is( ':checked' ) ) {
	$( '.method-paste' ).hide();
}
$( '#method-upload' ).on( 'change', function() {
	$( '.method-upload' ).toggle();
	if ( $( '.method-paste' ).is( ':visible' ) ) {
		$( '.method-paste' ).toggle();
	}
} );
$('#method-paste').on( 'change', function() {
	$( '.method-paste' ).toggle();
	if ( $( '.method-upload' ).is( ':visible' ) ) {
		$( '.method-upload' ).toggle();
	}
} );
// Set the machine names for items.
$( document ).on( 'keyup change', 'fieldset.metabox input.metabox-title', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).closest( 'fieldset.metabox' ).find( 'input.metabox-id' ).val( safeString );
} );
$( document ).on( 'keyup change', 'fieldset.metabox-field input.metabox-field-name', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).closest( 'fieldset.metabox-field' ).find( 'input.metabox-field-id' ).val( safeString) ;
} );
$( document ).on( 'keyup change', 'fieldset.post_type input.post_type-name', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).parents( 'fieldset.post_type' ).find( 'input.post_type-machine_name' ).val( safeString );
} );
$( document ).on( 'keyup change', 'fieldset.sidebar input.sidebar-title', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).parents( 'fieldset.sidebar' ).find( 'input.sidebar-machine-name' ).val( safeString );
} );      
$( document ).on( 'keyup change', 'fieldset.taxonomy input.taxonomy-name', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).parents( 'fieldset.taxonomy' ).find( 'input.taxonomy-id' ).val( safeString );
} );   
$( document ).on( 'keyup change', 'fieldset.menu-location input.menu-location-name', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).parents( 'fieldset.menu-location' ).find( 'input.menu-location-machine-name' ).val( safeString );
} );
$( document ).on( 'keyup change', '.customizer-panel-title', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).parents( 'fieldset.customizer-panel' ).find( '.customizer-panel-machine-name' ).val( safeString );
} );
$( document ).on( 'keyup change', '.customizer-section-title', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).parents( 'fieldset.customizer-section' ).find( '.customizer-section-machine-name' ).val( safeString );
} );
$( document ).on( 'keyup change', '.customizer-setting-title', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).parents( 'fieldset.customizer-setting' ).find( '.customizer-setting-machine-name' ).val( safeString );
} );
$( document ).on( 'keyup change', '.customizer-control-label', function() {
	let currentTitle = $( this ).val();
	let safeString = machineSafe( currentTitle );
	$( this ).parents( 'fieldset.customizer-control' ).find( '.customizer-control-machine-name' ).val( safeString );
} );  
function machineSafe( originalString ) {
	originalString = originalString.toLowerCase()
		.replace( /[^a-z\d]*/, '' )
		.replace( /-|\s|\_/g, '_' )
		.replace( /\_{2,}/g, '_' )
		.replace( /[\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|\/\@\!\#\%\^\&\;\:\'\"\<\>\=]/g, '' );
	return originalString;
}	

// Delete
// Delete Field before saving.
$( document ).on( 'click', '.new-field-delete', function ( e ) {
	e.preventDefault();
	let fieldContainer = $( this ).data( 'field' );
	let warning = $( this ).data( 'warning' );
	let c = confirm( warning );
	if ( c == true ) {
		// Escape the selector.
		let escaped = fieldContainer.replace( /(:|\.|\[|\])/g,'\\$1' );
		$( this ).parents( '#' + escaped ).remove();
	}
	else { return false; }
} );
// Delete existing field.
$( document ).on( 'click', '.field-delete', function( e ) {
	e.preventDefault();
	let warning = $( this ).data( 'warning' );
	let c = confirm( warning );
	if ( c == true ) {
		let parentContainer = $( this ).parents( 'li[data-metabox-field="' + $(this).data('field') + '"]' );
		parentContainer.remove();
	}
	else { return false; }
} );
// Delete field option.
$( document ).on( 'click', '.wp_builder-meta-delete-option', function ( e ) {
	e.preventDefault();
	let name = $( this ).data( 'name' );
	let warning = $( this ).data( 'warning' );
	let c = confirm( warning );
	if ( c == true ) {
		let parentFieldset = $(this).closest( '.field-option-item' );
		parentFieldset.remove();
	}
	else { return false; }
} );
// Customizer Settings Add Control Setting.
$( document ).on( 'click', '.add-control-setting', function ( e ) {
	e.preventDefault();
	let controlSetting = $( this ).data( 'control-setting' );
	let fieldNamePrefix = $( this ).data( 'field-name-prefix' );
	let fieldSettingId = $( this ).data( 'field-setting-id' );
	let fieldSettingTypeId = $( this ).data( 'field-' + controlSetting + '-id' );
	let fieldNameId = 'field_' + controlSetting + '_id';
	let button = $( this );
	let container = button.parents( '.control-settings' ).find( '.control-setting-' + controlSetting );   
	let data = {
		'setting_type': controlSetting,
		'field_name_prefix': fieldNamePrefix,
		'field_setting_id': fieldSettingId,
		'field_setting_type_id': fieldSettingTypeId,
		'wp_builder_customizer_settings_nonce': wp_builder_admin_local.customizer_settings_nonce
	};
	let post_data = {
		'action': 'wp_builder_get_customizer_settings',
		'data': JSON.stringify( data )
	};
	$.post( {
		url: wp_builder_admin_local.ajaxurl,
		method: "POST",
		data: post_data
	} )
	.done( function( response ) {
		container.append( response );
		button.data( 'field-' + controlSetting + '-id', ++fieldSettingTypeId );
	} );
} );
// Customizer Settings Delete Control Setting
$( document ).on( 'click', '.wp_builder-customizer-delete-choice', function ( e ) {
	e.preventDefault();
	let button = $( this );
	let choiceId = button.data( 'choice-id' );
	let parentContainer = button.parents( '.control-setting-choices' );
	let container = $('#choice-' + choiceId);
	let addButton = parentContainer.find( '.add-control-setting' );
	container.remove();
} );
// Customizer Change Field Type.
$( document ).on( 'change', '.customizer-setting-field-type', function() {
	let name = $( this ).attr('name');
	let type = $( this ).val();
	let setting_id = $( this ).data( 'setting-id' );
	let container = $( '.control-settings' );
	let data = {
		'setting_type': 'control',
		'type': type,
		'setting_id': setting_id,
		'wp_builder_customizer_settings_nonce': wp_builder_admin_local.customizer_settings_nonce
	};
	let post_data = {
		'action': 'wp_builder_get_customizer_settings',
		'data': JSON.stringify(data)
	};
	$.post( {
		url: wp_builder_admin_local.ajaxurl,
		method: "POST",
		data: post_data    	
	} )
	.done( function ( response ) {
		container.html( response );
	} );
} );
// Customizer add setting attribute.
$( document ).on( 'click', '.add-attribute', function ( e ) {
	e.preventDefault();
	let button = $( this );
	let setting_type = 'input_attrs';
	let attribute = button.data( 'attribute' );
	let name = button.data( 'attribute-name' );
	let type = button.data( 'attribute-type' );
	let label = button.data( 'attribute-label' );
	let container = $( '#input-attribute--' + attribute );
	let data = {
		'setting_type': setting_type,
		'attribute': attribute,
		'type': type,
		'name': name,
		'label': label,
		'action': true, 
		'customizer_settings_nonce': wp_builder_admin_local.customizer_settings_nonce
	};
	let post_data = {
		'action': 'wp_builder_get_customizer_settings',
		'data': JSON.stringify( data )
	}
	$.post( {
		url: wp_builder_admin_local.ajaxurl,
		method: "POST",
		data: post_data
	} )
	.done( function ( response) {
		container.replaceWith( response );
	} );
} );

// Customizer Delete Setting Attribute
$( document ).on( 'click', '.delete-attribute', function ( e ) {
	e.preventDefault();
	let button = $( this );
	let setting_type = 'input_attrs';
	let attribute = button.data( 'attribute' );
	let name = button.data( 'attribute-name' );
	let type = button.data( 'attribute-type' );
	let label = button.data( 'attribute-label' );
	let container = $( '#input-attribute--' + attribute );
	let data = {
		'setting_type': setting_type,
		'attribute': attribute,
		'type': type,
		'name': name,
		'label': label,
		'action': false,
		'customizer_settings_nonce': wp_builder_admin_local.customizer_settings_nonce
	};
	let post_data = {
		'action': 'wp_builder_customizer_settings',
		'data': JSON.stringify( data )
	}
	$.post( {
		url: wp_builder_admin_local.ajaxurl,
		method: "POST",
		data: post_data
	} )
	.done( function ( response ) {
		container.replaceWith( response );
	} );
} );

/**
* Post types.
*
* @since 2.0.0
*/
$( document ).on( 'change', '.map-meta-cap', function() {
	let $cap = $(this).parents( '.post_type' ).find( '.capability-types' );
	if ( $( this ).val() == 'false' ) {
		$cap.show();
	}
	else {
		$cap.hide();
	}
});
$( '.map-meta-cap:checked' ).parents( '.post_type' ).find( '.capability-type' ).show();
$( document ).on( 'click', '.wp_builder-add-capabilities', function() {
	let container = $( this ).parents( '.capability-types' ).find( '.capabilities' );
	let i = container.data( 'item' );
	let c = container.data( 'count' ) + 1;
	let field = '';
	let data = {
		'item': i,
		'count': c,
		'post_type_capabilities_nonce': wp_builder_admin_local.post_type_capabilities_nonce,
	};
	let postData = {
		'action': 'wp_builder_get_post_type_capabilities',
		'data': JSON.stringify( data )
	};

	$.post( {
		async: false,
		url: wp_builder_admin_local.ajaxurl,
		method: "POST", 
		data: postData
	} )
	.done( function ( response ) {
		field = response;
		container.append( field );
		container.data( 'count', c );
	} );
} );

// Delete field option.
$( document ).on( 'click', '.delete-capability', function ( e ) {
	e.preventDefault();
	let item = $( this ).data( 'item' );
	let count = $( this ).data( 'count' );
	let container = $( '#capability\\[' + item + '\\]\\[' + count + '\\]' );
	let warning = $( this ).data( 'warning' );
	let c = confirm( warning );
	if ( c == true ) {
		container.remove();
	}
	else { return false; }
} );
/**
* Metabox fields.
*
* @since 2.0.0
*/
const formUrl = $( '#form-url' ).val();

// Show / Hide custom palette options
$( document ).on( 'click', '.custom-palette', function() {
	$( this ).parents( '.wp_builder-metabox-field-params' ).find( '.custom-palette--wrapper' ).toggle( this.show );
} );
// Metabox delete click handler.
$( document ).on( 'click', '.wp_builder-delete-metabox', function() {
	let deleteInstance = parseInt( $( this ).attr('data-metabox'));
	$( '#metabox-' + deleteInstance ).hide().find( 'input.metabox-id' ).val( '' );
} );
// Metabox field type change event handler.
$( document ).on( 'change', '.wp_builder-field-type', function() {
	let name = $( this ).attr( 'name' );
	let type = $( this ).val();
	let field = getFieldStructure( type, name, 0 );
	let paramContainer = $( this ).closest( '.metabox-field' ).find( '.wp_builder-metabox-field-params' );
	paramContainer.html( field );
});
// Metabox field option handler.
$( document ).on( 'click', '.wp_builder-meta-add-option', function() {
	let counter = $( this );
	let currentCount = parseInt( $(this).attr( 'data-count' ) );
	let count = currentCount + 1;
	let type = $( this ).attr( 'data-type' );
	let name = $( this ).attr( 'data-name' );
	let field = '';
	let parentContainer = $( this ).parents( '.metabox-fields-options-wrapper' ).find( '.sortable-field-options' );
	let id = name.replace( "[options]", "" );
	let data = {
		'type': type,
		'form_url': formUrl,
		'field_name_prefix': id,
		'field_params': "options",
		'count': count,
		'field_options_nonce': wp_builder_admin_local.field_options_nonce,
	};
	let postData = {
		'action': 'wp_builder_get_field_options',
		'data': JSON.stringify( data )
	};

	$.post( {
		async: false,
		url: wp_builder_admin_local.ajaxurl,
		method: "POST", 
		data: postData
	} )
	.done( function ( response ) {
		field = response;
		parentContainer.append( field );
		counter.attr( 'data-count', count );
	} );
} );
// add fields to group
$( document ).on( 'click', '.wp-builder-add-group-fields', function() {
	let button = $( this );
	let name = $( this ).attr( 'data-field-id' );
	let fieldId = parseInt( $( this ).attr( 'data-field-count' ) );
	let parts = name.match( /\[.*?\]/g );
	parts = parts.map( function ( match ) { return match.slice( 1,-1 ); } );
	let type = 'metabox';
	for ( let i = 0; i < parts.length; i++ ) {
		type = type + '[' + parts[i] + ']';
	}
	let metaboxId = parts[2];
	let fieldPrefix = type + '[' + fieldId + ']';
	let data = {
		'field_type': 'text',
		'form_url': formUrl,
		'field_name_prefix': fieldPrefix,
		'i': metaboxId,
		'f': fieldId,
		'field_form_structure_nonce': wp_builder_admin_local.field_form_structure_nonce,
	};
	let postData = {
		'action': 'wp_builder_get_field_form_structure',
		'data': JSON.stringify( data )
	};
	$.post( {
		async: false,
		url: wp_builder_admin_local.ajaxurl,
		method: "POST", 
		data: postData
	} )
	.done( function( response ) {
		$( '#' + type.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" ) ).append( response );
		button.attr( 'data-field-count', fieldId + 1 );
	} );
} );
// add fields to metabox
$( document ).on( 'click', '.wp_builder-add-metabox-fields', function() {
	let button = $( this );
	let f = parseInt( $( this ).attr( 'data-count' ) );
	let i = parseInt( $( this ).attr( 'data-metabox' ) );
	let fieldPrefix = 'metabox[' + i + '][fields][' + f + ']';
	let data = {
		'field_type': 'text',
		'form_url': formUrl,
		'field_name_prefix': fieldPrefix,
		'i': i,
		'f': f,
		'field_form_structure_nonce':  wp_builder_admin_local.field_form_structure_nonce,
	};
	let postData = {
		'action': 'wp_builder_get_field_form_structure',
		'data': JSON.stringify( data )
	};
	$.post( {
		async: false,
		url: wp_builder_admin_local.ajaxurl,
		method: "POST", 
		data: postData
	} )
	.done( function( response ) {
		$( '.fields-wrapper' ).append( response );
		button.attr( 'data-count', f + 1 );
	} );
} );

function getUrlVars() {
	let vars = {};
	let parts = window.location.href.replace( /[?&]+([^=&]+)=([^&]*)/gi, function ( m,key,value ) {
		vars[ key ] = value;
	});
	return vars;
}     

function getFieldStructure( type, id, count ) {
	let field = '';
	id = id.replace( "[type]", "" );
	let data = {
		'type': type,
		'form_url': formUrl,
		'field_name_prefix': id,
		'field_params': "repeatable",
		'field_structure_nonce': wp_builder_admin_local.field_structure_nonce,
	};
	let postData = {
		'action': 'wp_builder_get_field_structure',
		'data': JSON.stringify(data)
	};
	$.post( {
		async: false,
		url: wp_builder_admin_local.ajaxurl,
		method: "POST", 
		data: postData
	} )
	.done( function ( response ) {
		field = response;
		return field;
	} );
	return field;
}
// Dismiss notices.
$( document ).on( 'click', '.notice-dismiss', function ( e ) {
	e.preventDefault();
	let notice = $( this ).parent( '.notice.is-dismissible' );
	let dismissUrl = notice.data( 'dismiss-url' );
	if ( dismissUrl ) {
		$.get( dismissUrl );
	}
} );
} ) ( jQuery );