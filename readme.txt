=== WP Builder ===
Contributors: scottsawyer, kylebaker, benbowen 
Donate link: http://scottsawyerconsulting.com/about
Tags: WP Builder, metaboxes, custom post types, taxonomies
Requires at least: 4.0
Requires PHP: 7.2
Tested up to: 5.6
Stable tag: 5.3
License: GPLv2 or Later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WP Builder. Create powerful structured content in WordPress through a comprehensive UI.

== Description ==

WP Builder is the missing administrative UI. It adds settings that allow creation of custom post types, meta boxes ( and fields ), custom taxonomies and more. It does not elimitate code, but helps you create structured content more efficiently. Checkout the README.md for more information.

[Watch the demo](https://youtu.be/OG6H_pZtN7g) and checkout the [demo theme](https://github.com/scottsawyer/ice-cream-eater).

**This plugin comes with NO WARRANTY**
*Use this plugin at your own risk*
This tool is intended for experienced developers. To get the most out of it, you will likely need to write some PHP. If you don't know at least some PHP, you probably shouldn't use WP Builder.

== Installation ==


1. Install and activate [CMB2](https://wordpress.org/plugins/cmb2/).
2. Install and activate WP Builder.
3. Go to WP Builder settings and start creating structured content!


== Frequently Asked Questions ==

= How do I use my custom post types in my theme? =

By default, custom post types will use your single.php theme file. You may want to create a new single for the post type. Use the machine name in the file name ( single-[my_post_type].php )

If your post type is set up to use posts capabilites, you can also create archives. 

= Can custom taxonomies be added to existing post types? =

Yes.

= How do I call metabox values with a shortcode? =

[wp_builder field="field_id" post_id="123"] (optional: post_id)]

== Screenshots ==





== Changelog ==

= 2.0.3 = 2020-12-17
- Started on tests, but didn't get too far.
- Fixed several coding standards issues.
- Fixed a bug in customizer views.
- Removed some core post types from Metaboxes.
- Removed CMB2. Now either the plugin must be installed, or installed with composer.

= 2.0.2 = 2020-12-14
- Added user entered data sanitization.
- Removed editor_css for security and usability reasons.
- Fixed a bug in post_type forms.
- Updated JS regex.

## [2.0.1 - 2020-12-11]
- Fixed some coding standards, refactored most of the field structures.
- Fixed a bug when saving unchanged settings.
- Added a $perm param to notifications.
- Cleaned up some JavaScript, removed console.log, unused selectors, coding standards.
- Added validation checks when closing metabox fields.
- Added AJAX callback for Group / Group fields.
- Removed Post Type meta_cap settings.

= 2.0.0 = 2020-12-05
Completely refactored! Completely OOP structure, fixed a ton of bugs.

= 0.1.1 = 2020-02-17
New icon.

= 0.1.0 = 
Initial release
