<?php
namespace WpBuilder;

require_once( dirname( __FILE__ ) . '/../includes/class-sidebar-manager.php' );

use PHPUnit\Framework\TestCase;
use phpmock\phpunit\PHPMock;

class SidebarManagerTests extends TestCase {

	use PHPMock;

	public function test_get_sidebars() {
		$get_option = $this->getFunctionMock( 'WpBuilder', 'get_option' );
		$get_option->expects( $this->once() )
					->with( $this->equalTo( 'wp_builder_sidebars' ) )
					->willReturn( ['sidebars'] );

		$sidebarManager = new SidebarManager();
		//$this->assertEquals( , $sidebarManager->get_sidebars() );
	}

	public function test_validate() {
		$get_option = $this->getFunctionMock( 'WpBuilder', 'get_option' );
		$sidebarManager = new SidebarManager();
		$sidebars_empty = [];
		$this->assertFalse( $sidebarManager->validate( $sidebars_empty ) == true );
		$sidebars_faulty_machine_name = ['machine_name' => ''];
		$this->assertFalse( $sidebarManager->validate( $sidebars_faulty_machine_name ) == true );
		$sidebars_correct = ['machine_name' => 'some_name'];
		$this->assertTrue( $sidebarManager->validate( $sidebars_correct ) );		
	}

}