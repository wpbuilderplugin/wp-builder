<?php
/**
 * The initilizer for WP Builder.
 *
 * @category WordPress_Plugin
 * @package  WpBuilder
 * @author   Scott Sawyer <scott@scottsawyerconsulting.com>
 * @license  GPL-2.0+ http://www.opensource.org/licenses/gpl-license.php
 * @link     https://bitbucket.org/wpbuilderplugin/wp-builder
 *
 * Plugin Name: WP Builder
 * Plugin URI: https://bitbucket.org/wpbuilderplugin/wp-builder
 * Description: Create custom post types, metaboxes, taxonomies and more in the WP UI
 * Short Description: Create custom post types, metaboxes, taxonomies and more in the WP UI
 * Version: 2.0.1
 * PHP Version: 7.3.0
 * Author: Scott Sawyer
 * Author URI: http://scottsawyerconsulting.com/about/team/scottsawyer
 *
 * Text Domain: wp-builder
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

/**
 * Plugin version.
 */
define( 'WP_BUILDER_VERSION', '2.0.1' );
/**
 * Minimum WordPress Version.
 */
define( 'WP_BUILDER_MIN_WP_VERSION', '5.0' );
/**
 * Minimum PHP Version.
 */
define( 'WP_BUILDER_MIN_PHP_VERSION', '7.3.0' );
/**
 * Define paths.
 */
define( 'WP_BUILDER_FILE', __FILE__ );
define( 'WP_BUILDER_PATH', trailingslashit( plugin_dir_path( WP_BUILDER_FILE ) ) );
define( 'WP_BUILDER_VENDOR_PATH', WP_BUILDER_PATH . 'vendor/' );
define( 'WP_BUILDER_ASSETS_PATH', trailingslashit( plugin_dir_url( WP_BUILDER_FILE ) ) . 'assets/' );
define( 'WP_BUILDER_ADMIN_PATH', WP_BUILDER_PATH . 'admin/' );
define( 'WP_BUILDER_ADMIN_ASSETS_PATH', trailingslashit( plugin_dir_url( WP_BUILDER_FILE ) ) . 'assets/admin/' );
/**
 * Activation.
 */
function activate_wp_builder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-builder-activator.php';
	Wp_Builder_Activator::activate();
}
/**
 * Deactivation.
 */
function deactivate_wp_builder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-builder-deactivator.php';
	Wp_Builder_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_builder' );
register_deactivation_hook( __FILE__, 'deactivate_wp_builder' );

/**
 * The core plugin class.
 */
require WP_BUILDER_PATH . 'includes/class-wp-builder.php';

/**
 * Begins execution.
 */
function run_wp_builder() {

	$plugin = new \WpBuilder\Wp_Builder();
	$plugin->run();
}
run_wp_builder();
