# Changelog
Update details.

## [2.0.3 - 2020-12-17]
- Started on tests, but didn't get too far.
- Fixed several coding standards issues.
- Fixed a bug in customizer views.
- Removed some core post types from Metaboxes.
- Removed CMB2. Now either the plugin must be installed, or installed with composer.

## [2.0.2 - 2020-12-14]
- Added user entered data sanitization.
- Removed editor_css for security and usability reasons.
- Fixed a bug in post_type forms.
- Updated JS regex.

## [2.0.1 - 2020-12-11]
- Fixed some coding standards, refactored most of the field structures.
- Fixed a bug when saving unchanged settings.
- Added a $perm param to notifications.
- Cleaned up some JavaScript, removed console.log, unused selectors, coding standards.
- Added validation checks when closing metabox fields.
- Added AJAX callback for Group / Group fields.
- Removed Post Type meta_cap settings.


## [2.0.0 - 2020-12-05]
Completely refactored! Completely OOP structure, fixed a ton of bugs.

## [0.1.1 - 2020-02-17]
New icon.

## [0.0.0 - 2016-12-06]
Initial release