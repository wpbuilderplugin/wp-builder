<?php
/**
 * Post Types forms.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder\Forms;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\PostTypeManager;

/**
 * Contains PostTypeForms class.
 */
class PostTypeForms {

	/**
	 * WpBuilder\PostTypeManager definition
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\PostTypeManager
	 */
	protected $post_type_manager;

	/**
	 * Constructs a new PostTypeForms object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}

	/**
	 * Loads class dependencies.
	 *
	 * @since 2.0.0
	 */
	public function load_dependencies() {
		$this->post_type_manager = new PostTypeManager();
	}

	/**
	 * Post Type Form
	 *
	 * @since 1.0.0
	 * @param string $form_url  The form action url.
	 * @param int    $item      The current iteration.
	 * @param array  $post_type An array of post type values.
	 */
	public function post_type_form( $form_url, $item = 0, $post_type = [] ) {
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--post_types" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_post_type_settings', 'post_type_settings' ); ?>  
			<fieldset class="post_type" data-count="<?= $item; ?>">
				<legend><?= ( ! empty( $post_type ) ?  __( 'Edit ', 'wp-builder' ) . $post_type['labels']['name'] :  __( 'Create Post Type', 'wp-builder' ) ); ?></legend>
				<input 
					type="hidden" 
					name="post_type[<?= $item; ?>][machine_name]" 
					<?php
					if ( isset( $post_type['machine_name'] ) ) :
						print 'value="' . $post_type['machine_name'] . '"';
					else :
						print 'class="post_type-machine_name"';
					endif;
					?>
				>
				<input 
					type="hidden" 
					name="post_type[<?= $item; ?>][unique_id]" 
					value="<?= ( isset( $post_type['unique_id'] ) ? $post_type['unique_id'] : uniqid() ); ?>">
				<table class="form-table">
					<tr class="form-field form-required">
						<th class="row">
							<label for="post_type[<?= $item; ?>][labels][name]">
								<?= __( 'Plural Name', 'wp-builder' ); ?> <span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							</label>
						</th>
						<td>
							<input 
								type="text" name="post_type[<?= $item; ?>][labels][name]" 
								value="<?= ( isset( $post_type['labels']['name'] ) ? $post_type['labels']['name'] : '' ); ?>"
								aria-required="true" 
								autocomplete="off"
								required>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="post_type[<?= $item; ?>][labels][singular_name]">
								<?= __( 'Singular Name', 'wp-builder' ); ?> 
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="post_type[<?= $item; ?>][labels][singular_name]" 
								value="<?= ( isset( $post_type['labels']['singular_name'] ) ? $post_type['labels']['singular_name'] : '' ); ?>"
								class="post_type-name" 
								aria-required="true" 
								autocomplete="off" 
								required>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="post_type[<?= $item; ?>][public]">
								<?= __( 'Public', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								id="post_type[<?= $item; ?>][public]" 
								type="checkbox" 
								name="post_type[<?= $item; ?>][public]" 
								value="true" <?= ( ! isset( $post_type['public'] ) || $post_type['public'] == 'true' ? 'checked' : '' ); ?> >
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="post_type[<?= $item; ?>][has_archive]"><?= __( 'Has Archive', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="checkbox" 
								id="post_type[<?= $item; ?>][has_archive]" 
								name="post_type[<?= $item; ?>][has_archive]" 
								value="true" <?= ( isset( $post_type['has_archive'] ) && $post_type['has_archive'] == 'true' ? "checked" : '' ); ?>>
						</td>
					</tr>
					<tr class="form-field form-required">
						<th class="row">
							<label for="post_type[<?= $item; ?>][show_in_rest]">
								<?= __( 'Show in REST', 'wp-builder' ); ?>
								<span class="tooltip" data-tip="<?= __( 'Required for the block editor.', 'wp-builder' ); ?>">
									<span class="dashicons dashicons-editor-help"></span>
								</span>
							</label>
						</th>
						<td>
							<input 
								type="checkbox" 
								id="post_type[<?= $item; ?>][show_in_rest]" 
								name="post_type[<?= $item; ?>][show_in_rest]" 
								value="true" <?= ( isset( $post_type['show_in_rest'] ) && $post_type['show_in_rest'] == 'true' ? "checked" : '' ); ?>>
						</td>
					</tr>
					<tr class="form-field form-required">
						<th class="row">
							<label for="post_type[<?= $item; ?>][rewrite][slug]">
								<?= __( 'Slug', 'wp-builder' ); ?> (<?= __( 'required', 'wp-builder' ); ?>)
							</label>
						</th>
						<td>
		        				<input 
								type="text" 
								id="post_type[<?= $item; ?>][rewrite][slug]" 
								name="post_type[<?= $item; ?>][rewrite][slug]" 
								value="<?= ( isset( $post_type['rewrite']['slug'] ) ? $post_type['rewrite']['slug'] : '' ); ?>" 
								aria-required="true" 
								autocomplete="off"
								required>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="post_type[<?= $item; ?>][description]">
								<?= __( 'Description', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" id="post_type[<?= $item; ?>][description]" 
								name="post_type[<?= $item; ?>][description]" 
								value="<?= ( isset( $post_type['description'] ) ? $post_type['description'] : "" ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr>
						<th class="row">
							<?= __( 'Supported Features', 'wp-builder' ); ?>
						</th>
						<td>
							<fieldset>
								<?php
								$supported_features = $this->post_type_manager->get_supported_features();
								if ( ! empty( $supported_features ) ) :
									foreach ( $supported_features as $key => $label ) :
										?>
										<label for="post_type[<?= $item; ?>][supports][<?= $key; ?>]">
											<input 
												type="checkbox" 
												name="post_type[<?= $item; ?>][supports][]"
												id="post_type[<?= $item; ?>][supports][<?= $key; ?>]"
												value="<?= $key; ?>"
												<?= ( ! isset( $post_type['supports'] ) || ( ! empty( $post_type ) && in_array( $key, $post_type['supports'] ) ) ? 'checked' : '' ); ?>>
											<?= $label; ?>
										</label>
										<br>
										<?php
									endforeach;
								endif;
								?>
							</fieldset>
						</td>
					</tr>
					<tr>
						<th class="row">
							<?= __( 'Taxonomies', 'wp-builder' ); ?>
						</th>
						<td>
							<fieldset>
								<?php
								$taxonomies = get_taxonomies( '', 'objects' );
								foreach ( $taxonomies as $taxonomy ) :
									?>
									<label for="post_type[<?= $item; ?>][taxonomies][<?= $taxonomy->name; ?>]">
										<input
											type="checkbox"
											id="post_type[<?= $item; ?>][taxonomies][<?= $taxonomy->name; ?>]"
											name="post_type[<?= $item; ?>][taxonomies][]"
											value="<?= $taxonomy->name; ?>"
										<?= ( isset( $post_type['taxonomies'] ) && in_array( $taxonomy->name, $post_type['taxonomies'] ) ? 'checked' : '' ); ?>>
										<?= $taxonomy->labels->name; ?>
									</label>
									<br>
								<?php endforeach; ?>
							</fieldset>
						</td>
					</tr>
					<tr>
						<th class="row">
							<label for="post_type[<?= $item; ?>][hierarchical]"><?= __( 'Hierarchical', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="checkbox" 
								name="post_type[<?= $item; ?>][hierarchical]" 
								value="true" 
								<?= ( isset( $post_type['hierarchical'] ) && $post_type['hierarchical'] == true ? 'checked' : '' ); ?>>
						</td>
					</tr>
					<tr>
						<th class="row">
							<?= __( 'Capability Type', 'wp-builder' ); ?>
						</th>
						<td>
							<fieldset>
								<label for="post_type[<?= $item; ?>][capability_type][post]">
									<input 
										type="radio" 
										name="post_type[<?= $item; ?>][capability_type]" 
										id="post_type[<?= $item; ?>][capability_type][post]" 
										value="post" 
										<?= ( isset( $post_type['capability_type'] ) && $post_type['capability_type'] != 'page' ? 'checked' : '' ); ?>>
									<?= __( 'Post', 'wp-builder' ); ?>
								</label>
								<br>
								<label for="post_type[<?= $item; ?>][capability_type][page]">
									<input 
										type="radio" 
										name="post_type[<?= $item; ?>][capability_type]" 
										id="post_type[<?= $item; ?>][capability_type][page]" 
										value="page" 
										<?= ( isset( $post_type['capability_type'] ) && $post_type['capability_type'] == 'page' ? 'checked' : '' ); ?>>
									<?= __( 'Page', 'wp-builder' ); ?>
								</label>
							</fieldset>
						</td>
					</tr>
				</table>
				<p class="submit">
					<?php submit_button( __( 'Save Post Type', 'wp-builder' ), 'primary', 'submit' ); ?>
					<input type="hidden" name="wp_builder_submit" value="Y">
				</p>
			</fieldset>
		</form>
		<?php
	}

	/**
	 * Capabilities form.
	 *
	 * @todo this is currently disabled, even though it technically works.
	 * @since 2.0.1
	 * @param int   $item  The current item.
	 * @param int   $count The current count.
	 * @param array $cap   The capability.
	 */
	public function capabilities_form_structure( $item, $count, $cap = [] ) {
/**
					<tr>
						<th class="row">
							<?=  __( 'Use Default Meta Capability', 'wp-builder' ); ?>
							<span class="tooltip" data-tip="<?= __( 'Warning! Please refer the the Codex', 'wp-builder' ); ?>">
								<span class="dashicons dashicons-editor-help"></span>
							</span>
						</th>
						<td>
							<fieldset>
								<label for="post_type[<?= $item; ?>][map_meta_cap][true]">
									<input
										type="radio"
										name="post_type[<?= $item; ?>][map_meta_cap]"
										value="true"
										id="post_type[<?= $item; ?>][map_meta_cap][true]"
										<?= ( isset( $post_type['map_meta_cap'] ) && $post_type['map_meta_cap'] == true ? 'checked' : '' ); ?>>
									<?= __( 'True', 'wp-builder' ); ?>
								</label>
								<br>
								<label for="post_type[<?= $item; ?>][map_meta_cap][false]">
									<input
										type="radio"
										name="post_type[<?= $item; ?>][map_meta_cap]"
										value="false"
										id="post_type[<?= $item; ?>][map_meta_cap][false]"
										class="map-meta-cap"
										<?= ( isset( $post_type['map_meta_cap'] ) && $post_type['map_meta_cap'] == false ? 'checked' : '' ); ?>>
									<?= __( 'False', 'wp-builder' ); ?>
								</label>
								<div class="capability-types" <?= ( isset( $post_type['map_meta_cap']['false'] ) ? 'style="display:block;"' : 'style="display:none;"' ); ?>>
									<h3><?= __( 'Capabilities', 'wp-builder' ); ?></h3>
									<div class="capabilities" data-item="<?= $item; ?>" data-count="<?= isset( $post_type['capabilities'] ) && is_countable( $post_type['capabilities'] ) ? count( $post_type['capabilities'] ) : 0; ?>">
										<?php 
										if ( isset( $post_type['capabilities'] ) && is_array( $post_type['capabilities'] ) ) :
											foreach ( $post_type['capabilities'] as $c => $ct ) :
												$this->capabilities_form_structure( $item, $c, $ct );
											endforeach;
										else :
											$this->capabilities_form_structure( $item, 0 );
										endif;
										?>
									</div>
									<p class="submit">
										<input
											type="button"
											class="wp_builder-add-capabilities"
											value="<?= __( 'Add Capabilities', 'wp-builder' ); ?>">
									</p>
								</div>
							</fieldset>
						</td>
					</tr>
/**/
		?>
		<fieldset class="capability postbox" id="capability[<?= $item; ?>][<?= $count; ?>]">
			<table class="form-table">
				<tr>
					<th class="row">
						<label for="post_type[<?= $item; ?>][capabilities][<?= $count; ?>][key]">
							<?= __( 'Key', 'wp-builder' ); ?>
						</label>
					</th>
					<td>
						<input 
							type="text" 
							name="post_type[<?= $item; ?>][capabilities][<?= $count; ?>][key]" 
							value="<?= isset( $cap['key'] ) ? $cap['key'] : ''; ?>" 
							id="post_type[<?= $item; ?>][capabilities][<?= $count; ?>][key]"
							required>
					</td>
				</tr>
				<tr>
					<th class="row">
						<label for="post_type[<?= $item; ?>][capabilities][<?= $count; ?>][value]">
							<?= __( 'Value', 'wp-builder' ); ?>
						</label>
					</th>
					<td>
						<input 
							type="text" 
							name="post_type[<?= $item; ?>][capabilities][<?= $count; ?>][value]" 
							value="<?= isset( $cap['value'] ) ? $cap['value'] : ''; ?>" 
							id="post_type[<?= $item; ?>][capabilities][<?= $count; ?>][value]"
							required>
					</td>
				</tr>
			</table>
			<button 
				class="wp_builder-trash delete-capability" 
				data-item="<?= $item; ?>" 
				data-count="<?= $count; ?>"
				data-warning="<?= __( 'Are you sure you want to delete this Capability?', 'wp-builder' ); ?>">
				<span class="dashicons dashicons-trash"></span>
				<?= __( 'Delete Capability', 'wp-builder' ); ?>
			</button>
		</fieldset>
		<?php
	}

	/**
	 * Post type delete form.
	 *
	 * @since 2.0.0
	 * @param string $form_url   The form URL.
	 * @param array  $post_type  The post_type.
	 */
	public function delete_form( $form_url, $post_type ) {
		?>
		<form method="post" action="<?= $form_url; ?>" >
			<input type="hidden" name="post_type[unique_id]" value="<?= ( isset( $post_type['unique_id'] ) ? $post_type['unique_id'] : '' ); ?>" >
			<input 
				type="hidden" 
				name="post_type[machine_name]" 
				value="<?= ( isset( $post_type['machine_name'] ) ? $post_type['machine_name'] : '' ); ?>" 
				class="post-type-id">
			<input type="hidden" name="action" value="delete">
			<input type="hidden" name="type" value="post_type">
			<?php wp_nonce_field( 'wp_builder_post_type_delete', 'post_type_delete' ); ?>
			<h2><?= __( 'Are you sure you want to delete this post type?', 'wp-builder' ); ?></h2>
			<p><?= $post_type['labels']['name']; ?></p>
			<p class="submit">
				<?php submit_button( __( 'Delete post type', 'wp_builder' ), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>		
		</form>
		<?php
	}
}
