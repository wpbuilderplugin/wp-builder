<?php
/**
 * Sidebar Form class.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder\Forms;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Contains SidebarForms class.
 */
class SidebarForms {
	/**
	 * Sidebar form.
	 *
	 * @since 1.0.0
	 * @param string $form_url The form action.
	 * @param int    $i        The current interation.
	 * @param array  $sidebar  An array of sidebar information.
	 */
	public function sidebar_form( $form_url, $i = 0, $sidebar = [] ) {
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--sidebar" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_sidebar_settings', 'sidebar_settings' ); ?>
			<fieldset class="sidebar">
				<legend><?= ( isset( $sidebar['title'] ) ? __( 'Edit ', 'wp-builder' ) . $sidebar['title'] : __( 'Create Sidebar', 'wp-builder' ) ); ?></legend>
				<input 
					type="hidden" 
					name="sidebar[<?= $i; ?>][machine_name]"
					<?php
					if ( isset( $sidebar['machine_name'] ) ) :
						print 'value="' . $sidebar['machine_name'] . '"';
					else :
						print 'class="sidebar-machine-name"';
					endif;
					?>
				>
				<input 
					type="hidden" 
					name="sidebar[<?= $i; ?>][unique_id]" 
					value="<?= ( isset( $sidebar['unique_id'] ) ? $sidebar['unique_id'] : uniqid() ); ?>">
				<table class="form-table">
					<tr class="form-field form-required">
						<th class="row">
							<label for="sidebar[<?= $i; ?>][title]"><?= __( 'Name', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="sidebar[<?= $i; ?>][title]"
								value="<?= ( isset( $sidebar['title'] ) ? $sidebar['title'] : '' ); ?>"
								class="sidebar-title" 
								autocomplete="off"
								aria-required="true" required>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="sidebar[<?= $i; ?>][description]"><?= __( 'Description', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="text" 
								name="sidebar[<?= $i; ?>][description]"
								value="<?= ( isset( $sidebar['description'] ) ? $sidebar['description'] : '' ); ?>"
								autocomplete="off"
								>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="sidebar[<?= $i; ?>][widget_wrapper]"><?= __( 'Sidebar widgets wrapper element', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
						   </label>
						</th>
						<td>
							<?php
							$wrapper_elements = [
								'div' => 'Div',
								'article' => 'Article',
							];
							?>
							<select name="sidebar[<?= $i; ?>][widget_wrapper]" aria-required="true" required>
								<?php foreach ( $wrapper_elements as $el => $label ) : ?>
									<option value="<?= $el; ?>" <?= ( isset( $sidebar['widget_wrapper'] ) && $sidebar['widget_wrapper'] == $el ? 'selected' : '' ); ?>>
										<?= $label; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="sidebar[<?= $i; ?>][widget_class]"><?= __( 'Sidebar widgets class.', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="text" 
								name="sidebar[<?= $i; ?>][widget_class]" 
								value="<?= ( isset( $sidebar['widget_class'] ) ? $sidebar['widget_class'] : '' ); ?>"
								autocomplete="off"
								>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="sidebar[<?= $i; ?>][widget_title_wrapper]"><?= __( 'Sidebar widgets title wrapper', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							</label>
						</th>
						<td>
							<?php
							$title_wrapper_elements = [
								'h2' => 'H2',
								'h3' => 'H3',
								'h4' => 'H4',
								'span' => 'Span',
							];
							?>
							<select name="sidebar[<?= $i; ?>][widget_title_wrapper]"  aria-required="true" required>
								<?php foreach ( $title_wrapper_elements as $el => $label ) : ?>
									<option value="<?= $el; ?>" <?= ( isset( $sidebar['widget_title_wrapper'] ) && $sidebar['widget_title_wrapper'] == $el ? 'selected' : '' ); ?>>
										<?= $label; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="sidebar[<?= $i; ?>][widget_title_class]"><?= __( 'Sidebar widgets title class', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="text"
								name="sidebar[<?= $i; ?>][widget_title_class]" 
								value="<?= ( isset( $sidebar['widget_title_class'] ) ? $sidebar['widget_title_class'] : '' ); ?>"
								autocomplete="off"
								>
						</td>
					</tr>
				</table>
			</fieldset>
			<p class="submit">
				<?php submit_button( __( 'Save Sidebar', 'wp_builder' ), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>
		</form>
		<?php
	}

	/**
	 * Sidebar delete form.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form URL.
	 * @param array  $sidebar  The sidebar.
	 */
	public function delete_form( $form_url, $sidebar ) {
		?>
		<form method="post" action="<?= $form_url; ?>" class="delete-form">
			<input type="hidden" name="sidebar[unique_id]" value="<?= ( isset( $sidebar['unique_id'] ) ? $sidebar['unique_id'] : '' ); ?>" >
			<input type="hidden" name="sidebar[machine_name]" value="<?= ( isset( $sidebar['machine_name'] ) ? $sidebar['machine_name'] : '' ); ?>" class="sidebar-id">
			<input type="hidden" name="action" value="delete">
			<input type="hidden" name="type" value="sidebar">
			<?php wp_nonce_field( 'wp_builder_sidebar_delete', 'sidebar_delete' ); ?>
			<h2><?= __( 'Are you sure you want to delete this sidebar?', 'wp-builder' ); ?></h2>
			<p><?= $sidebar['title']; ?></p>
			<p class="submit">
				<?php
				submit_button(
					__( 'Delete Sidebar', 'wp-builder' ),
					'delete button-primary',
					'submit',
					false
				);
				?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>
		</form>
		<?php
	}
}
