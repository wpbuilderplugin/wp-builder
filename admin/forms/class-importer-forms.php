<?php
/**
 * Importer Forms.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder\Forms;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Contains ImporterForms class.
 */
class ImporterForms {

	/**
	 * Form for importing configuration.
	 *
	 * @since 1.0.0
	 * @param string $form_url The form url.
	 */
	function import_form( $form_url ) {
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--importer" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_import_settings', 'import_settings' ); ?>
			<input type="hidden" name="_wp_builder_import" value="configuration">
			<h2><?= __( 'Import Configuration', 'wp-builder' ); ?></h2>
			<p><?= __( 'Use this form to import configuration.  It\'s advisable to only use this form to import from another site, such as a staging server, and not to author your own json.  This can really break your site!!', 'wp-builder' ); ?><p>
			<table class="form-table">
				<tr class="form-field">
					<th class="row"><?= __( 'What type of configuration?', 'wp-builder' ); ?></th>
					<td>
						<fieldset>
							<label for="type[sidebars]">
								<input type="radio" name="type" id="type[sidebars]" value="sidebars" required>
								<?= __( 'Sidebars', 'wp-builder' ); ?>
							</label><br>
							<label for="type[post-types]">
								<input type="radio" name="type" id="type[post-types]" value="post_types" required>
								<?= __( 'Post Types', 'wp-builder' ); ?>
							</label><br>
							<label for="type[metaboxes]">
								<input type="radio" name="type" id="type[metaboxes]" value="metaboxes" required>
								<?= __( 'Metaboxes', 'wp-builder' ); ?>
							</label><br>
							<label for="type[menu_locations]">
								<input type="radio" name="type" id="type[menu_locations]" value="menu_locations" required>
								<?= __( 'Menu Locations', 'wp-builder' ); ?>
							</label><br>
							<label for="type[taxonomies]">
								<input type="radio" name="type" id="type[taxonomies]" value="taxonomies" required>
								<?= __( 'Taxonomies', 'wp-builder' ); ?>
							</label><br>
							<label for="type[customizer-panels]">
								<input type="radio" name="type" id="type[customizer-panels]" value="customizer-panels" required>
								<?= __( 'Customizer Panels', 'wp-builder' ); ?>
							</label><br>
							<label for="type[customizer-sections]">
								<input type="radio" name="type" id="type[customizer-sections]" value="customizer-sections" required>
								<?= __( 'Customizer Sections', 'wp-builder' ); ?>
							</label><br>
							<label for="type[customizer-settings]">
								<input type="radio" name="type" id="type[customizer-settings]" value="customizer-settings" required>
								<?= __( 'Customizer Settings', 'wp-builder' ); ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr class="form-field">
					<th class="row"><?= __( 'Import strategy', 'wp-builder' ); ?></th>
					<td>
						<fieldset>
							<label for="strategy[delete]">
								<input type="radio" name="strategy" id="strategy[delete]" value="delete" required checked>
								<?= __( 'Delete', 'wp-builder' ); ?>
								<span class="tooltip" data-tip="<?= __( 'Delete all existing configuration, then import new configuration.', 'wp-builder' ); ?>">
									<span class="dashicons dashicons-editor-help"></span>
								</span>
							</label><br>
							<label for="strategy[overwrite]">
								<input type="radio" name="strategy" id="strategy[overwrite]" value="overwrite" required>
								<?= __( 'Overwrite', 'wp-builder' ); ?>
								<span class="tooltip" data-tip="<?= __( 'Import new configuration, replace any matching configuration.', 'wp-builder' ); ?>">
									<span class="dashicons dashicons-editor-help"></span>
								</span>
							</label><br>
							<label for="strategy[add]">
								<input type="radio" name="strategy" id="strategy[add]" value="add" required>
								<?= __( 'Add', 'wp-builder' ); ?>
								<span class="tooltip" data-tip="<?= __( 'Import new configuration, skip any matches.', 'wp-builder' ); ?>">
									<span class="dashicons dashicons-editor-help"></span>
								</span>
							</label><br>
						</fieldset>
					</td>
				</tr>
				 <tr class="form-field">
					<th class="row"><?= __( 'Import Method', 'wp-builder' ); ?></th>
					 <td>
						<fieldset>
							<label for="method-paste">
								<input type="radio" id="method-paste" name="method" value="paste" required><?= __( 'Paste Code', 'wp-builder' ); ?>
							</label><br>
							<label for="method-upload">
								<input type="radio" id="method-upload" name="method" value="upload" required><?= __( 'Upload file', 'wp-builder' ); ?>
							</label><br>
						</fieldset>
					</td>
				</tr>
			</table>
			<fieldset class="method-paste">
				<table class="form-table">
					<tr class="form-field">
						<th class="row"><?= __( 'Paste configuration', 'wp-builder' ); ?></th>
						<td><textarea name="pasted"></textarea></td>
					</tr>
				</table>
			</fieldset>
			<fieldset class="method-upload">
				<table class="form-table">
					<tr class="form-field">
						<th class="row"><label for="upload"><?= __( 'Upload Configuration File', 'wp-builder' ); ?></label></th>
						<td><input type="file" name="upload" id="upload"><p><?= __( 'Files must be valid json.', 'wp-builder' ); ?></p></td>
					</tr>
				</table>
			</fieldset>
			<p class="submit">
				<?php submit_button( __( 'Begin Import', 'wp-builder' ), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>
		</form>
		<?php
	}
}
