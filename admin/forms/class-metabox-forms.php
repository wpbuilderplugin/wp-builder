<?php
/**
 * Metabox forms.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder\Forms;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\MetaboxManager;

/**
 * Container MetaboxForms class.
 */
class MetaboxForms {

	/**
	 * WpBuilder\MetaboxManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MetaboxManager $metabox_manager
	 */
	protected $metabox_manager;
	/**
	 * Constructs a new MetaboxForms object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}

	/**
	 * Loads class dependencies.
	 *
	 * @since 2.0.0
	 */
	public function load_dependencies() {
		$this->metabox_manager = new MetaboxManager();
	}
	/**
	 * Metabox Form.
	 *
	 * @param string $form_url The url for the form.
	 * @param int    $i        The current interation.
	 * @param array  $metabox  An array of metabox information.
	 */
	public function metabox_form( $form_url, $i = 0, $metabox = [] ) {
		$prefix = '_wp_builder_';
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--metabox" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_metabox_settings', 'metabox_settings' ); ?>
			<h3><?= __( 'Edit Metabox', 'wp-builder' ); ?></h3>
			<fieldset id="metabox-<?= $i; ?>" class="metabox hidden-form" >
				<legend><?= ( isset( $metabox ) && array_key_exists( 'title', $metabox ) ? __( 'Edit Metabox: ', 'wp-builder' ) . $metabox['title'] : __( 'Create Metabox', 'wp-builder' )  ); ?></legend>
				<input type="hidden" name="form-url" value="<?= $form_url; ?>" id="form-url">
				<input 
					type="hidden" 
					name="metabox[<?= $i; ?>][id]" 
					<?php
					if ( isset( $metabox['id'] ) ) :
						print 'value="' . $metabox['id'] . '"';
					else :
						print 'class="metabox-id"';
					endif;
					?>>
				<input type="hidden" name="metabox[<?= $i; ?>][unique_id]" value="<?= ( isset( $metabox['unique_id'] ) ? $metabox['unique_id'] : uniqid() ); ?>">
				<div class="form-panel">
					<h4><?= __( 'Metabox Core Properties', 'wp-builder' ); ?></h4>
					<table class="form-table">
						<tr class="form-field form-required">
							<th class="row">
								<label for="metabox[<?= $i; ?>][title]"><?= __( 'Title', 'wp-builder' ); ?>
									<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)
										<span class="tooltip" data-tip="No special characters.">
											<span class="dashicons dashicons-editor-help"></span>
										</span>
									</span>
								</label>
							</th>
							<td>
								<input 
									type="text" 
									name="metabox[<?= $i; ?>][title]" 
									value="<?= ( isset( $metabox['title'] ) ? $metabox['title'] : '' ); ?>"
									class="metabox-title" 
									aria-required="true" 
									autocomplete="off" required>
							</td>
						</tr>
						<tr>
							<th class="row"><?= __( 'Show on Post Types', 'wp-builder' ); ?></th>
							<td>
								<fieldset>
									<?php
									//$current_post_types = get_post_types( '', 'objects' );
									$current_post_types = $this->metabox_manager->supported_post_types();
									foreach ( $current_post_types as $ps ) {
										echo '<label for="metabox[' . $i . '][pages][' . $ps->name . ']"><input type="checkbox" name="metabox[' . $i . '][pages][]" id="metabox[' . $i . '][pages][' . $ps->name . ']" value="' . $ps->name . '" ';
										if ( $metabox && array_key_exists( 'pages', $metabox ) && in_array($ps->name, $metabox['pages'] ) ) {
											echo ' checked="checked"';
										}
										echo '>' . $ps->labels->name . '</label><br>';
									}
									?>
								</fieldset>
							</td>
						</tr>
						<?php if ( ! empty( get_page_templates() ) ) : ?>
							<tr class="form-field form-required">
								<th class="row"><?= __( 'Show on Page Templates', 'wp-builder' ); ?> <span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span></th>
								<td>
									<fieldset>
										<?php
										foreach ( get_page_templates() as $name => $filename ) {
											$metabox_id = "metabox[{$i}][tpls][{$filename}]";
											echo "<label for='{$metabox_id}'>";
											echo "<input type='checkbox' name='metabox[{$i}][tpls][]' id='{$metabox_id}' value='{$filename}'";
											if ( isset( $metabox['tpls'] ) && in_array( $filename, $metabox['tpls'] ) ) {
												echo "checked";
											}
											echo ">{$name}</label><br>";
										}
										?>
									</fieldset>
								</td>
							</tr>
						<?php endif ?>
						<tr class="form-field">
							<th class="row"><?= __( 'Context', 'wp-builder' ); ?></th>
							<td>
								<fieldset>
									<label for="metabox[<?= $i; ?>][context][normal]">
										<input 
											type="radio" 
											name="metabox[<?= $i; ?>][context]" 
											id="metabox[<?= $i; ?>][context][normal]" 
											value="normal" 
											<?= ( ( ! isset( $metabox['context'] ) || $metabox['context'] == 'normal' ) ? 'checked' : '' ); ?>
										>
										<?= __( 'Normal', 'wp-builder' );?>
									</label><br>
									<label for="metabox[<?= $i; ?>][context][advanced]">
										<input 
											type="radio" 
											name="metabox[<?= $i; ?>][context]" 
											id="metabox[<?= $i; ?>][context][advanced]" 
											value="advanced" 
											<?= ( ( isset( $metabox['context'] ) && $metabox['context'] == 'advanced' ) ? 'checked' : '' ); ?>
										>
										<?= __( 'Advanced', 'wp-builder' ); ?>
									</label><br>
									<label for="metabox[<?= $i; ?>][context][side]">
										<input 
											type="radio" 
											name="metabox[<?= $i;  ?>][context]" 
											id="metabox[<?= $i; ?>][context][side]" 
											value="side" 
											<?= ( ( isset( $metabox['context'] ) && $metabox['context'] == 'side' ) ? 'checked' : '' ); ?>
										>
										<?= __( 'Side', 'wp-builder' ); ?>
									</label><br>
								</fieldset>
							</td>
						</tr>
						<tr class="form-field">
							<th class="row"><?= __( 'Priority', 'wp-builder' ); ?></th>
							<td>
								<fieldset>
									<label for="metabox[<?= $i; ?>][priority][high]">
										<input 
											type="radio" 
											name="metabox[<?= $i; ?>][priority]" 
											id="metabox[<?= $i; ?>][priority][high]" 
											value="high" 
											<?= ( ( isset( $metabox['priority'] ) && $metabox['priority'] == 'high' ) ? 'checked' : '' ); ?>
										>
										<?= __( 'High', 'wp-builder' ); ?>
									</label><br>
									<label for="metabox[<?= $i; ?>][priority][core]">
										<input 
											type="radio" 
											name="metabox[<?= $i; ?>][priority]" 
											id="metabox[<?= $i; ?>][priority][core]" 
											value="core" 
											<?= ( ( isset( $metabox['priority'] ) && $metabox['priority'] == 'core' ) ? 'checked' : '' ); ?>
										>
										<?= __( 'Core', 'wp-builder' ); ?>
									</label><br>
									<label for="metabox[<?= $i; ?>][priority][default]">
										<input 
											type="radio" 
											name="metabox[<?= $i; ?>][priority]" 
											id="metabox[<?= $i; ?>][priority][default]" 
											value="default" 
											<?= ( ( ! isset( $metabox['priority'] ) || $metabox['priority'] == 'default' ) ? 'checked' : '' ); ?>
										>
										<?= __( 'Default', 'wp-builder' ); ?>
									</label><br>
									<label for="metabox[<?= $i; ?>][priority][low]">
										<input 
											type="radio" 
											name="metabox[<?= $i; ?>][priority]" 
											id="metabox[<?= $i; ?>][priority][low]" 
											value="low" 
											<?= ( ( isset( $metabox['priority'] ) && $metabox['priority'] == 'low' ) ? 'checked' : '' ); ?>
										>
										<?= __( 'Low', 'wp-builder' ); ?>
									</label>
								</fieldset>
							</td>
						</tr>
						<tr class="form-field">
							<th class="row">
								<label for="metabox[<?= $i; ?>][show_names]">
									<?= __( 'Show Names', 'wp-builder' ); ?>
									<span class="tooltip" data-tip="Show field names on the left.">
										<span class="dashicons dashicons-editor-help"></span>
									</span>
								</label>
							</th>
							<td>
								<input 
									type="checkbox" 
									name="metabox[<?= $i; ?>][show_names]" 
									id="metabox[<?= $i; ?>][show_names]" 
									value="1" 
									<?= ( ( ! isset( $metabox['show_names'] ) || $metabox['show_names'] == true ) ? 'checked' : '' ); ?>
								>
							</td>
						</tr>
					</table>
				</div>
				<h3><?= __( 'Fields', 'wp-builder' ); ?></h3>
				<div class="wp_builder-metabox-fields">
					<ul class="fields-wrapper sortable">
					<?php
					$count = 0;
					if ( isset( $metabox['fields'] ) && ! empty( $metabox['fields'] ) ) {
						$field_count = [];
						foreach ( $metabox['fields'] as $f => $field_settings ) {
							$field_name_prefix = 'metabox[' . $i . '][fields][' . $f . ']';
							$type = $field_settings['type'];
							$this->field_type_form_structure( $type, $form_url, $i, $f, $field_name_prefix, $field_settings, $metabox );
							$field_count[] = $f;
						}
						rsort( $field_count );
						reset( $field_count );
						$count = $field_count[0] + 1;
					}
					?>
					</ul>
					<input 
						type="button" 
						class="wp_builder-add-metabox-fields test-1" 
						value="<?= __( 'Add Field', 'wp-builder' ); ?>" 
						data-metabox="<?= $i; ?>" 
						data-count="<?= $count; ?>">
				</div>
			</fieldset>
			<?php submit_button( __( 'Save Metabox', 'wp-builder' ), 'primary', 'submit' ); ?>
			<input type="hidden" name="wp_builder_submit" value="Y">
		</form>
		<!-- edit metabox -->
		<?php
	}

	/**
	 * Metabox delete form.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form URL.
	 * @param array  $metabox  The metabox.
	 */
	public function metabox_delete_form( $form_url, $metabox ) {
		?>
		<form method="post" action="<?= $form_url; ?>">
			<input type="hidden" name="metabox[unique_id]" value="<?= ( isset( $metabox['unique_id'] ) ? $metabox['unique_id'] : '' ); ?>" class="metabox-id">
			<input type="hidden" name="action" value="delete">
			<input type="hidden" name="type" value="metabox">
			<?php wp_nonce_field( 'wp_builder_metabox_delete', 'metabox_delete' ); ?>
			<h2><?= __( 'Are you sure you want to delete this metabox?', 'wp-builder' ); ?></h2>
			<p><?= $metabox['title']; ?></p>
			<?php submit_button( __( 'Delete Metabox', 'wp-builder' ), 'primary', 'submit' ); ?>
			<input type="hidden" name="wp_builder_submit" value="Y">
		</form>
		<?php
	}

	/**
	 *  Build Fields.
	 *
	 * @since 1.0.0
	 * @param string $type              The type of field to build.
	 * @param string $form_url          The form url.
	 * @param int    $i                 The current metabox iteration.
	 * @param int    $f                 The current field iteration.
	 * @param string $field_name_prefix The prefix for the field.
	 * @param array  $field_params      An array of field parameters.
	 * @param array  $metabox           An array of the metabox.
	 */
	public function field_type_form_structure( $type, $form_url, $i, $f, $field_name_prefix = '', $field_params = [], $metabox = [] ) {
		$prefix = '_wp_builder_';
		$types = $this->metabox_manager->field_types();
		?>
		<!-- wp_builder_field_type_form_structure -->
		<li data-metabox-field="<?= $field_name_prefix; ?>">
			<div class="metabox-fields-form">
				<fieldset class="metabox-field">
					<input 
						type="hidden" 
						name="<?= $field_name_prefix; ?>[id]" 
						value="<?= ( isset( $field_params['id'] ) ? str_replace( $prefix . $metabox['id'] . '_', '', $field_params['id'] ) : '' ); ?>" 
						<?= ( ! isset( $field_params['id'] ) ? 'class="metabox-field-id"' : '' ); ?>>
					<legend class="metabox-field-legend">
						<div>
							<span class="dashicons dashicons-move"></span>
							<span class="field-title"><?= ( isset( $field_params['name'] ) ?  __( 'Field: ', 'wp-builder' ) . $field_params['name'] : __( 'New Field', 'wp-builder' ) ); ?></span>
							<button class="collapser <?= empty( $field_params ) ? 'open' : ''; ?>">
								<span class="dashicons dashicons-arrow-up"></span>
							</button>
						</div>
					</legend>
					<div class="field-settings-container" <?= ! empty( $field_params ) ? 'style="display:none;"' : ''; ?>>
						<table class="form-table">
							<tr class="form-field">
								<th class="row">
									<label for="<?= $field_name_prefix; ?>[name]"><?= __( 'Field Name', 'wp-builder' ); ?>
								 		<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)
								  			<span class="tooltip" data-tip="<?= __( 'Field names must be unique per metabox.', 'wp-builder' ); ?>">
												<span class="dashicons dashicons-editor-help"></span>
											</span>
										</span>
									</label>
								</th>
								<td>
									<input 
										type="text" 
										name="<?= $field_name_prefix; ?>[name]" 
										value="<?= ( isset( $field_params['name'] ) ? $field_params['name'] : '' ); ?>"
										class="metabox-field-name"
										autocomplete="off" 
										required>
								</td>
							</tr>
							<tr class="form-field">
								<th class="row">
									<label for="<?= $field_name_prefix; ?>[desc]"><?= __( 'Description', 'wp-builder' ); ?></label>
								</th>
								<td>
									<input 
										type="text" 
										name="<?= $field_name_prefix; ?>[desc]" 
										value="<?= ( isset( $field_params['desc'] ) ? $field_params['desc'] : '' ); ?>">
								</td>
							</tr>
							<tr class="form-field">
								<th class="row"><label for="<?= $field_name_prefix; ?>[type]"><?= __( 'Field Type', 'wp-builder' ); ?></label></th>
								<td>
									<?php if ( isset( $field_params['type'] ) ) : ?>
										<input 
											type="hidden" 
											name="<?= $field_name_prefix; ?>[type]" 
											value="<?= $field_params['type']; ?>">
										<p><?= $types[ $field_params['type'] ]; ?>
											<span class="tooltip" data-tip="<?= __( 'Field type can not be changed.', 'wp-builder' ); ?>">
												<span class="dashicons dashicons-editor-help"></span>
											</span>
										</p>
									<?php else : ?>
										<select 
											name="<?= $field_name_prefix; ?>[type]" 
											class="wp_builder-field-type <?= ( isset( $field_params['type'] ) ? 'disabled' : '' ); ?>" 
											data-count="<?= $f; ?>" 
											data-metabox="<?= $i; ?>"
											<?= ( isset( $field_params['type'] ) ? 'readonly' : '' ); ?>>
											<?php foreach ( $types as $type_key => $type_value ) : ?>
												<option 
													value="<?= $type_key; ?>" 
													<?= ( ( isset( $field_params['type'] ) && $field_params['type'] == $type_key ) ? 'selected' : '' ); ?>>
													<?= $type_value; ?>
												</option>
											<?php endforeach; ?>
										</select>
									<?php endif; ?>
								</td>
							</tr>
							<tr class="form-field">
								<td colspan="2">
									<div class="wp_builder-metabox-field-params">
										<?php if ( $type === 'group' ) : ?>
											<table class="form-table">
												<tr class="form-field">
													<th class="row">
														<label for="<?= $field_name_prefix; ?>[repeatable]">
															<?= __( 'Repeatable', 'wp-builder' ); ?>
														</label>
													</th>
													<td>
														<input 
															type="checkbox" 
															name="<?= $field_name_prefix; ?>[repeatable]" 
															id="<?= $field_name_prefix; ?>[repeatable]" 
															value="true" <?= ( isset( $field_params['repeatable'] ) ? 'checked' : '' ); ?>>
													</td>
												</tr>
												<tr class="form-field">
													<th class="row"><?= __( 'Group Fields', 'wp-builder' ); ?></th>
													<td>
														<div class="group-fields">
															<ul class="group-fields-wrapper sortable ui-sortable" id="<?= $field_name_prefix; ?>[fields]">
																<?php 
																if ( isset( $field_params['fields'] ) ) :
																	foreach ( $field_params['fields'] as $group_field => $group_field_values ) :
																		$this->field_type_form_structure(
																			$group_field_values['type'],
																			$form_url,
																			$i,
																			$group_field,
																			$field_name_prefix . '[fields][' . $group_field . ']',
																			$group_field_values,
																			$metabox
																		);
																	endforeach;
																endif;
																?>
															</ul>
														</div>
													</td>
												</tr>
												<tr>
													<th class="row"><?= __( 'Add Fields to Group', 'wp-builder' ); ?></th>
													<td>
														<input 
															type="button" 
															value="<?= __( 'Add Field', 'wp-builder' ); ?>" 
															data-field-count="<?= ( isset( $field_params['fields'] ) ? count( $field_params['fields'] ) + 1 : '0' ); ?>" 
															data-field-id="<?= $field_name_prefix; ?>[fields]" 
															class="wp-builder-add-group-fields">
													</td>
												</tr>
											</table>
											<?php
											else :
												$args = [
													'field_name_prefix' => $field_name_prefix,
													'field_params' => $field_params,
													'metabox_id' => $i,
													'field_id' => $f,
													'url' => $form_url,
												];
												$this->get_field_structure( $type, $args );
											endif;
											?>
									</div>
									<hr>
								</td>
							</tr>
						</table>
						<div class="button--wrapper">
							<p class="button--container">
								<a 
									class="wp_builder-trash field-delete" 
									href="<?= $form_url; ?>&metabox=<?= $i; ?>&action=delete&item=field&item_id=<?= $field_name_prefix; ?>" 
									data-warning="<?= __( 'Are you sure you want to delete this field?', 'wp-builder' ); ?>"
									data-field="<?= $field_name_prefix; ?>"
								>
									<span class="dashicons dashicons-trash"></span>
									<?= __( 'Delete Field', 'wp-builder' ); ?>
								</a>
							</p>
						</div>
					</div> <!-- /.field-settings-container -->
				</fieldset>
			</div>
		</li> <!-- End field -->
		<?php
	}

	/**
	 * Get Field Structure
	 *
	 * @since 1.0.0
	 * @param string $type The type of field.
	 * @param array  $args An array of field params.
	 *
	 * @return string $field
	 *  Callable
	 */
	public function get_field_structure( $type = 'text', $args ) {
		$field_types = $this->metabox_manager->field_types();
		if ( array_key_exists( $type, $field_types ) ) {
			$file_name = WP_BUILDER_ADMIN_PATH . '/fields/field_structure_' . $type . '.php';
			clearstatcache();
			if ( file_exists( $file_name ) ) {
				include_once $file_name;
				$field = 'field_structure_' . $type;
				return $field( $args );
			}
		}
	}

	/**
	 * Get Field Structure Options.
	 *
	 * @since 1.0.0
	 * @param string $type The type of field.
	 * @param array  $args An array of arguments.
	 *
	 * @return string $field
	 *  Callable.
	 */
	public function get_field_options( $type = 'text', $args ) {
		$field_types = $this->metabox_manager->field_types();
		if ( array_key_exists( $type, $field_types ) ) {
			$file_name = WP_BUILDER_ADMIN_PATH . '/fields/field_structure_' . $type . '.php';
			clearstatcache();
			if ( file_exists( $file_name ) ) {
				include_once $file_name;
				$field = 'field_options_' . $type;
				return $field( $args );
			}
		}
	}

}
