<?php
/**
 * Customizer Forms.
 *
 * @since 2.0.0
 * @package WpBuilder
 */

namespace WpBuilder\Forms;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\CustomizerManager;

/**
 * Contains CustimizerForms class.
 */
class CustomizerForms {

	/**
	 * WpBuilder\CustomizerManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\CustomizerManager $customizer_manager
	 */
	protected $customizer_manager;

	/**
	 * Constructs a new \WpBuilder\CustomerForms object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}

	/**
	 * Loads dependencies.
	 *
	 * @since 2.0.0
	 */
	public function load_dependencies() {
		$this->customizer_manager = new CustomizerManager();
	}

	/**
	 * Customizer Panel Form.
	 * Add / Edit Customizer Panels.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form url
	 * @param int    $i        The iteration.
	 * @param array  $panel    The panel array.
	 */
	public function panel_form( $form_url, $i = 0, $panel = [] ) {
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--customizer" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_customizer_settings', 'customizer_settings' ); ?>		
			<fieldset class="customizer-panel">
				<legend>
					<?= isset( $panel['title'] ) ? __( 'Edit Customizer Panel ' . $panel['title'], 'wp-builder' ) : __( 'Create New Customizer Panel', 'wp-builder' ); ?>
				</legend>
				<input type="hidden" name="component" value="panel">
				<input type="hidden" name="panel[<?= $i; ?>][id]" value="<?= $i; ?>">
				<input 
					type="hidden" 
					name="panel[<?= $i; ?>][machine_name]" 
					value="<?= isset( $panel['machine_name'] ) ? $panel['machine_name'] : ''; ?>" 
					class="customizer-panel-machine-name" >
				<table class="form-table">
					<tr class="form-field form-required">
						<th class="row">
							<label for="panel[<?= $i; ?>][title]"><?= __( ' Panel Name', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="panel[<?= $i; ?>][title]" 
								value="<?= isset( $panel['title'] ) ? $panel['title'] : ''; ?>" 
								class="customizer-panel-title" 
								aria-required="true" 
								required autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="panel[<?= $i; ?>][priority]"><?= __( 'Priority', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="number" 
								name="panel[<?= $i; ?>][priority]" 
								value="<?= isset( $panel['priority'] ) ? $panel['priority'] : '0'; ?>">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="panel[<?= $i; ?>][description]"><?= __( 'Panel Description', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="text" 
								name="panel[<?= $i; ?>][description]" 
								value="<?= isset( $panel['description'] ) ? $panel['description'] : ''; ?>" 
								autocomplete="off">
						</td>
					</tr>
				</table>
			</fieldset>
			<?php
			submit_button( __( 'Save Customizer Panel', 'wp-builder' ), 'primary', 'submit' );
			echo '<input type="hidden" name="wp_builder_submit" value="Y">';
			?>
		</form>
		<?php
	}

	/**
	 * Panel delete form.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form url.
	 * @param array  $panel    The panel array.
	 */
	public function panel_delete_form( $form_url, $panel ) {
		?>
		<form method="post" action="<?= $form_url; ?>" >
			<input type="hidden" name="panel[machine_name]" value="<?= ( isset( $panel['machine_name'] ) ? $panel['machine_name'] : '' ); ?>">
			<input type="hidden" name="action" value="delete">
			<input type="hidden" name="type" value="customizer">
			<input type="hidden" name="component" value="panel">
			<?php wp_nonce_field( 'wp_builder_customizer_delete', 'customizer_delete' ); ?>
			<h2><?= __( 'Are you sure you want to delete this customizer panel?', 'wp-builder' ); ?></h2>
			<p><?= $panel['title']; ?></p>
			<p class="submit">
				<?php submit_button( __( 'Delete Customizer Panel', 'wp_builder' ), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>
		</form>
		<?php
	}

	/**
	 * Customizer Section Form.
	 * Add / Edit Customizer Sections.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form url.
	 * @param int    $i        The section ID.
	 * @param array  $section  The section array.
	 */
	public function section_form( $form_url, $i = 0, $section = [] ) {
		$panels = $this->customizer_manager->get_all_panels();
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--customizer" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_customizer_settings', 'customizer_settings' ); ?>		
			<fieldset class="customizer-section">
				<legend>
					<?php if ( isset( $section['title'] ) ) : ?>
						<?= __( 'Edit Customizer Section ' . $section['title'], 'wp-builder' ); ?>
					<?php else : ?>
						<?= __( 'Create New Customizer Section', 'wp-builder' ); ?>
					<?php endif; ?>
				</legend>
				<input type="hidden" name="component" value="section">
				<input type="hidden" name="section[<?= $i; ?>][id]" value="<?= $i; ?>">
				<input 
					type="hidden" 
					name="section[<?= $i; ?>][machine_name]" 
					value="<?= isset( $section['machine_name'] ) ? $section['machine_name'] : ''; ?>" 
					class="customizer-section-machine-name" >
				<table class="form-table">
					<tr class="form-field form-required">
						<th class="row">
							<label for="section[<?= $i; ?>][title]"><?= __( 'Section Name', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							<label>
						</th>
						<td>
							<input 
								type="text" 
								name="section[<?= $i; ?>][title]" 
								value="<?= isset( $section['title'] ) ? $section['title'] : '' ?>" 
								class="customizer-section-title" 
								aria-required="true" 
								required autocomplete="off">
						</td>
					</tr>
					<?php if ( ! empty( $panels ) ) : ?>
						<tr class="form-field">
							<th class="row">
								<label for="section[<?= $i; ?>][panel]"><?= __( 'Panel', 'wp-builder' ); ?></label>
							</th>
							<td>
								<select name="section[<?= $i; ?>][panel]" required>
									<option value="_none" <?php isset( $section['panel'] ) ? selected( $section['panel'], '_none' ) : ''; ?>>
										<?= __( 'No Panel', 'wp-builder' ); ?>
									</option>
									<?php foreach ( $panels as $panel_key => $panel_values ) : ?>
										<option value="<?= $panel_key; ?>"
										<?php
										if ( isset( $section['panel'] ) ) {
											selected( $section['panel'], $panel_key );
										}
										?>
										><?= $panel_values['title']; ?>
										</option>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					<?php endif; ?>
					<tr class="form-field">
						<th class="row">
							<label for="section[<?= $i; ?>][prioriy]"><?= __( 'Section Priority', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="number" 
								name="section[<?= $i; ?>][priority]" 
								value="<?= isset( $section['priority'] ) ? $section['priority'] : '0'; ?>" 
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="section[<?= $i; ?>][description]"><?= __( 'Section Description', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="text" 
								name="section[<?= $i; ?>][description]" 
								value="<?= isset( $section['description'] ) ? $section['description'] : ''; ?>" 
								autocomplete="off">
						</td>
					</tr>
				</table>
			</fieldset>
			<?php
			submit_button( __( 'Save Customizer Section', 'wp-builder' ), 'primary', 'submit' );
			echo '<input type="hidden" name="wp_builder_submit" value="Y">';
			?>
		</form>
		<?php
	}

	/**
	 * Section delete form.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form url.
	 * @param array  $section  The section array.
	 */
	public function section_delete_form( $form_url, $section ) {
		?>
		<form method="post" action="<?= $form_url; ?>" >
			<input type="hidden" name="section[machine_name]" value="<?= ( isset( $section['machine_name'] ) ? $section['machine_name'] : '' ); ?>">
			<input type="hidden" name="action" value="delete">
			<input type="hidden" name="type" value="customizer">
			<input type="hidden" name="component" value="section">
			<?php wp_nonce_field( 'wp_builder_customizer_delete', 'customizer_delete' ); ?>
			<h2><?= __( 'Are you sure you want to delete this customizer section?', 'wp-builder' ); ?></h2>
			<p><?= $section['title']; ?></p>
			<p class="submit">
				<?php submit_button( __( 'Delete Customizer Section', 'wp-builder' ), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>
		</form>
		<?php
	}

	/**
	 * Customizer Setting Form.
	 *
	 * @param string $form_url The form url.
	 * @param int    $i        The setting id.
	 * @param array  $setting  The setting array.
	 */
	public function setting_form( $form_url, $i = 0, $setting = [] ) {
		$sections = $this->customizer_manager->get_all_sections();
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--customizer" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_customizer_settings', 'customizer_settings' ); ?>		
			<fieldset class="customizer-setting">
				<legend>
					<?php if ( isset( $setting['title'] ) ) : ?>
						<?= __( 'Edit Customizer Setting ', 'wp-builder' ) . $setting['title']; ?>
					<?php else : ?>
						<?= __( 'Create New Customizer Setting', 'wp-builder' ); ?>
					<?php endif; ?>
				</legend>
				<input type="hidden" name="component" value="setting">
				<input type="hidden" name="setting[<?= $i; ?>][id]" value="<?= $i; ?>">
				<input 
					type="hidden" 
					name="setting[<?= $i; ?>][machine_name]" 
					value="<?= isset( $setting['machine_name'] ) ? $setting['machine_name'] : ''; ?>" 
					class="customizer-setting-machine-name" >
				<table class="form-table">
					<tr class="form-field form-required">
						<th class="row">
							<label for="setting[<?= $i; ?>][title]"><?= __( 'Setting Name', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="setting[<?= $i; ?>][title]" 
								value="<?= isset( $setting['title'] ) ? $setting['title'] : ''; ?>" 
								class="customizer-setting-title" 
								aria-required="true" 
								required 
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field form-required">
						<th class="row>">
							<?= __( 'Choose setting type', 'wp-builder' ); ?>
						</th>
						<td>
							<?php foreach ( $this->customizer_manager->get_setting_types() as $type => $label ) : ?>
								<label for="setting[<?= $i; ?>][type][<?= $type; ?>]">
									<input 
										type="radio" 
										name="setting[<?= $i; ?>][type]" 
										id="setting[<?= $i; ?>][type][<?= $type; ?>]" 
										value="<?= $type; ?>" 
										required
										<?php isset( $setting['type'] ) ? checked( $setting['type'], $type ) : ''; ?>>
										  <?= $label; ?>
								</label><br>
							<?php endforeach; ?>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="setting[<?= $i; ?>][default]"><?= __( 'Default value', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="text" 
								name="setting[<?= $i; ?>][default]" 
								value="<?= isset( $setting['default'] ) ? $setting['default'] : ''; ?>" 
								autocomplete="off">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<fieldset class="customizer-control">
								<legend><?= __( 'Customizer Control', 'wp-builder' ); ?></legend>
								<input 
									type="hidden" 
									name="setting[<?= $i; ?>][control][machine_name]" 
									value="<?= isset( $setting['control']['machine_name'] ) ? $setting['control']['machine_name'] : ''; ?>" 
									class="customizer-control-machine-name">
								<table class="form-table">
									<tr class="form-field form-required">
										<th class="row">
											<label for="setting[<?= $i; ?>][control][label]"><?= __( 'Control Label', 'wp-builder' ); ?>
												<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
											</label>
										</th>
										<td>
											<input 
												type="text" 
												name="setting[<?= $i; ?>][control][label]" 
												value="<?= isset( $setting['control']['label'] ) ? $setting['control']['label'] : ''; ?>" 
												class="customizer-control-label" 
												autocomplete="off"
												aria-required="true" 
												required>
										</td>
									</tr>
									<tr class="form-field">
										<th class="row">
											<label for="setting[<?= $i; ?>][control][description]"><?= __( 'Description', 'wp-builder' ); ?></label>
										</th>
										<td>
											<input 
												type="text" 
												name="setting[<?= $i; ?>][control][description]" 
												value="<?= isset( $setting['control']['description'] ) ? $setting['control']['description'] : ''; ?>"
												autocomplete="off"
											>
										</td>
									</tr>
									<tr class="form-field">
										<th class="row">
											<label for="setting[<?= $i; ?>][control][section]"><?= __( 'Section', 'wp-builder' ); ?></label>
										</th>
										<td>
											<?php if ( ! empty( $sections ) ) : ?>
												<select name="setting[<?= $i; ?>][control][section]" class="control-field-type" aria-required="true" required>
													<option value="_none_" disabled><?= __( 'None', 'wp-builder' ); ?></option>
													<?php foreach ( $sections as $section_key => $section_values ) : ?>
														<option value="<?= $section_key; ?>" 
															<?php
															if ( isset( $setting['control']['section'] ) ) {
																selected( $setting['control']['section'], $section_key );
															}
															?>
															>
															<?= $section_values['title']; ?>
														</option>
													<?php endforeach; ?>
												</select>
											<?php else : ?>
												<a href="<?= $form_url; ?>">
													<?= __( 'If you want to add this control to a section, please a section first.', 'wp-builder' ); ?>
												</a>
											<?php endif; ?>
										</td>
									</tr>
									<tr class="form-field">
										<th class="row">
											<label for="setting[<?= $i; ?>][control][type]"><?= __( 'Control Type', 'wp-builder' ); ?></label>
										</th>
										<td>
											<select name="setting[<?= $i; ?>][control][type]" class="customizer-setting-field-type" data-setting-id="<?= $i; ?>">
												<option value="_none_" disabled><?= __( '-- Choose --', 'wp-builder' ); ?></option>
												<?php foreach ( $this->customizer_manager->field_types() as $key => $value ) : ?>
													<option value="<?= $key; ?>" 
														<?php
														if ( isset( $setting['control']['type'] ) ) {
															selected( $setting['control']['type'], $key );
														}
														?>
														>
														<?= $value['label']; ?>
													</option>
												<?php endforeach; ?>
											</select>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<div class="control-settings" data-setting-id="<?= $i; ?>">
												<?php
												if ( isset( $setting['control']['type'] ) ) {
													$type = $setting['control']['type'];
													$this->control_setting_form( $type, $i, $setting['control'] );
												} else {
													$this->control_setting_form( 'text', $i );
												}
												?>
											</div>
										</td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
				</table>
			</fieldset>
			<?php
			submit_button( __( 'Save Customizer Setting', 'wp-builder' ), 'primary', 'submit' );
			echo '<input type="hidden" name="wp_builder_submit" value="Y">';
			?>
		</form>
		<?php
	}

	/**
	 * Setting delete form.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form URL.
	 * @param array  $setting  The setting array.
	 */
	public function setting_delete_form( $form_url, $setting ) {
		?>
		<form method="post" action="<?= $form_url; ?>" >
			<input type="hidden" name="setting[machine_name]" value="<?= ( isset( $setting['machine_name'] ) ? $setting['machine_name'] : '' ); ?>">
			<input type="hidden" name="action" value="delete">
			<input type="hidden" name="type" value="customizer">
			<input type="hidden" name="component" value="setting">
			<?php wp_nonce_field( 'wp_builder_customizer_delete', 'customizer_delete' ); ?>
			<h2><?= __( 'Are you sure you want to delete this customizer setting?', 'wp-builder' ); ?></h2>
			<p><?= $setting['title']; ?></p>
			<p class="submit">
				<?php submit_button( __( 'Delete Customizer Setting', 'wp-builder' ), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>
		</form>
		<?php
	}

	/**
	 * Control Setting form.
	 *
	 * @since 2.0.0
	 * @param string $type       The setting type.
	 * @param int    $i          The setting ID.
	 * @param array  $control    The control array.
	 */
	public function control_setting_form( $type = 'text', $i = 0, $control = [] ) {
		$field_types = $this->customizer_manager->field_types();
		if ( isset( $field_types[ $type ] ) && isset( $field_types[ $type ]['settings'] ) ) {
			foreach ( $field_types[ $type ]['settings'] as $field_setting => $field_properties ) {
				$f = 0;
				$field_name_prefix = 'setting[' . $i . '][control][' . $field_setting . ']';
				/**
				 * The field setting.
				 * $field_setting str ( one of the field setting types )
				 * 'field_name' str the control field settings names
				 * 'field_setting_id'
				 * 'field_' . $field_setting . '_id' str
				 * 'field_values' array ( already created values )
				 */
				$args = [
					'field_name_prefix' => $field_name_prefix,
					'field_setting_id' => $i,
				];
				$get_settings = 'field_' . $field_setting . '_form';
				if ( 'choices' == $field_setting ) :
					?>
					<div class="choices-container">
						<h3><?= __( 'Choices', 'wp-builder' ); ?></h3> 
						<ul class="sortable sortable-field-settings control-setting-<?= $field_setting; ?>">
							<?php
							if ( array_key_exists( $field_setting, $control ) && ! empty( $control[ $field_setting ] ) ) {
								foreach ( $control[ $field_setting ] as $fs_id => $fs_values ) {
									$f = $fs_id;
									$args['field_values'] = $fs_values;
									$args['field_' . $field_setting . '_id'] = $f;
									$this->$get_settings( $args );
								}
								$ids = array_keys( $control[ $field_setting ] );
								rsort( $ids );
								reset( $ids );
								$f = $ids[0];
							}
							else {
								$args['field_' . $field_setting . '_id'] = $f;
								$this->$get_settings( $args );
							}
							?>
						</ul>
						<button 
							class="add-control-setting add-control-<?= $field_setting; ?>" 
							data-control-setting="<?= $field_setting; ?>" 
							data-field-name-prefix="<?= $field_name_prefix; ?>" 
							data-field-setting-id="<?= $i; ?>" 
							data-field-<?= $field_setting; ?>-id="<?= ++$f; ?>">
							<?= __( 'Add Choice', 'wp-builder' ); ?>
						</button>
					</div>
					<?php
				elseif ( 'input_attrs' == $field_setting ) :
					?>
					<div class="input-attrs-container">
						<h3><?= __( 'Input Attributes', 'wp-builder' ); ?></h3>
						<table class="form-table">
							<?php
							if ( array_key_exists( 'default_attributes', $field_properties ) ) {
								foreach ( $field_properties['default_attributes'] as $attribute => $attribute_parts ) {
									$type = $attribute_parts['field_type'];
									$name = $field_name_prefix . '[' . $attribute . ']';
									$label = $attribute_parts['label'];
									$value = isset( $control['input_attrs'][ $attribute ] ) ? $control['input_attrs'][ $attribute ] : '';
									$field = [
										'attribute' => $attribute,
										'name' => $name,
										'label' => $label,
										'type' => $type,
										'value' => $value,
									];
									$default = true;
									$this->input_attribute_field_form( $field, true, $default );
								}
							}
							$optional_attributes = $this->customizer_manager->input_attribute_types();
							foreach ( $optional_attributes as $attribute => $attribute_parts ) :
								$type = $attribute_parts['field_type'];
								$name = $field_name_prefix . '[' . $attribute . ']';
								$label = $attribute_parts['label'];
								$field = [
									'attribute' => $attribute,
									'name' => $name,
									'label' => $label,
									'type' => $type,
								];
								if ( isset( $control['input_attrs'][ $attribute ] ) ) :
									$field['value'] = $control['input_attrs'][ $attribute ];
									$this->input_attribute_field_form( $field );
								else :
									$this->input_attribute_field_form( $field, false );
								endif;
							endforeach;
							?>
						</table>
					</div>
					<?php
				endif;
			}
		}
	}

	/**
	 * Field choices.
	 *
	 * @since 2.0.0
	 * @param array $args An array of arguments.
	 */
	public function field_choices_form( $args ) {
		$field_name_prefix = $args['field_name_prefix'];
		$i = $args['field_setting_id'];
		$f = $args['field_choices_id'];
		$field_values = array_key_exists( 'field_values', $args ) ? $args['field_values'] : [];
		?>
		<li class="ui-sortable-handle" id="choice-<?= $f; ?>">
			<div class="postbox">
				<table class="form-table" >
					<tr class="row">
						<th><?= __( 'Choice Label', 'wp-builder' ); ?></th>
						<td>
							<input 
								type="text" 
								name="<?= $field_name_prefix; ?>[<?= $f; ?>][label]" 
								value="<?= isset( $field_values['label'] ) ? $field_values['label'] : ''; ?>" 
								autocomplete="off">
						</td>
					</tr>
					<tr class="row">
						<th><?= __( 'Choice Value', 'wp_builder' ); ?></th>
						<td>
							<input 
								type="text" 
								name="<?= $field_name_prefix; ?>[<?= $f; ?>][value]" 
								value="<?= isset( $field_values['value'] ) ? $field_values['value'] : ''; ?>" 
								autocomplete="off">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<p class="button--wrapper">
								<a 
									class="wp_builder-customizer-delete-choice wp_builder-trash" 
									href="&amp;action=delete&amp;setting=<?= $i; ?>&amp;item=choice&amp;item_id=<?= $f; ?>"  
									data-name="<?= $field_name_prefix; ?>[choices][<?= $f; ?>]"
									data-choice-id="<?= $f; ?>"  
									data-warning="<?= __( 'If you delete this item, this page is refreshed and you will lose any other changes made.', 'wp-builder' ); ?>">
									<span class="dashicons dashicons-trash"></span>
									<?= __( 'Delete Choice', 'wp_builder' ); ?>
								</a>
							</p>
						</td>
					</tr>
				</table>
			</div>
		</li>
		<?php
	}

	/**
	 * Input attr field form.
	 *
	 * @since 2.0.0
	 * @param array $field             The field.
	 * {
	 *   @type string $attribute The attribute.
	 *   @type string $type      The type.
	 *   @type string $name      The name.
	 *   @type string $label     The label.
	 *   @type string $value     The value.
	 * }
	 * @param bool  $add               The opperation.
	 * @param bool  $default_attribute The default attribute.
	 */
	public function input_attribute_field_form( $field, $add = true, $default_attribute = false ) {
		$attribute = $field['attribute'];
		$name = $field['name'];
		$label = $field['label'];
		$type = array_key_exists( 'type', $field ) ? $field['type'] : 'text';
		$value = array_key_exists( 'value', $field ) ? $field['value'] : '';
		if ( $add == true ) :
			?>
			<tr class="form-field" id="input-attribute--<?= $attribute; ?>">
				<th class="row">
					<label for="<?= $name; ?>"><?= $label; ?></label>
					<?php if ( $default_attribute == false ) : ?>
						<br>
						<a 
							href="#" 
							class="wp_builder-trash delete-button delete-attribute" 
							data-attribute="<?= $attribute; ?>"
							data-attribute-name="<?= $name; ?>"
							data-attribute-type="<?= $type; ?>"
							data-attribute-label="<?= $label; ?>"
						>
						<span class="dashicons dashicons-trash"></span> <?= __( 'Delete ', 'wp-builder' ) . $label; ?></a>
					<?php endif; ?>
				</th>
				<td>
					<input type="<?= $type ?>" name="<?= $name; ?>" value="<?= $value; ?>" autocomplete="off">
				</td>
			</tr>
		<?php else : ?>
			<tr class="form-field" id="input-attribute--<?= $attribute; ?>">
				<th class="row">
					<label for="<?= $name; ?>]"><?= $label; ?></label>
				</th>
				<td>
					<a 
						href="#" 
						class="button add-attribute" 
						data-attribute="<?= $attribute; ?>"
						data-attribute-name="<?= $name; ?>" 
						data-attribute-type="<?= $type; ?>" 
						data-attribute-label="<?= $label; ?>">
						<?= __( 'Add ', 'wp-builder' ) . $label; ?>
					</a>
				</td>
			</tr>
			<?php
		endif;
	}

}
