<?php
/**
 * Taxonomy Form class.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder\Forms;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Container TaxonomyForms class.
 */
class TaxonomyForms {

	/**
	 * Taxonomy Form.
	 *
	 * @since 1.0.0
	 * @param string $form_url
	 *  The url for the form. 
	 * @param int $i
	 *  The current iteration.
	 * @param array $taxonomy
	 *  An array of taxonomy information.
	 */
	public function taxonomy_form( $form_url, $i = 0, $taxonomy = [] ) {
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--taxonomy" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_taxonomy_settings', 'taxonomy_settings' ); ?>
			<fieldset class="taxonomy">
				<legend><?= ( isset( $taxonomy['args'] ) ? __( 'Edit ', 'wp-builder' ) . $taxonomy['args']['labels']['name'] : __( 'Create Taxonomy', 'wp-builder' )  ); ?></legend>
				<input 
					type="hidden" 
					name="custom_taxonomy[<?= $i; ?>][machine_name]" 
					<?php
					if ( isset( $taxonomy['machine_name'] ) ) :
						print 'value="' . $taxonomy['machine_name'] . '"';
					else :
						print 'class="taxonomy-id"';
					endif;
					?>>
				<input type="hidden" name="custom_taxonomy[<?= $i; ?>][unique_id]" value="<?= ( isset( $taxonomy['unique_id'] ) ? $taxonomy['unique_id'] : uniqid() ); ?>">
				<table class="form-table">
					<tr class="form-field form-required">
						<th class="row">
							<?= __( 'Show on Post Types', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
						</th>
						<td>
							<fieldset>
								<?php
								$current_post_types = get_post_types( '', 'objects' );
								foreach( $current_post_types as $pt ) :
									?>
									<label for="custom_taxonomy[<?= $i; ?>][post_types][<?= $pt->name; ?>]">
										<input
											type="checkbox"
											name="custom_taxonomy[<?= $i; ?>][post_types][]"
											value="<?= $pt->name; ?>"
											id="custom_taxonomy[<?= $i; ?>][post_types][<?= $pt->name; ?>]"
											<?= ( isset( $taxonomy['post_types'] ) && in_array( $pt->name, $taxonomy['post_types'] ) ? 'checked' : '' ); ?>>
										<?= $pt->labels->name; ?>
									</label>
									<br>
								<?php endforeach; ?>
							</fieldset>
						</td>
					</tr>
					<tr>
						<th class="row" colspan="2"><h3><?= __( 'Labels', 'wp-builder' ); ?></h3></th>
					</tr>
					<tr class="form-field form-required">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][name]">
								<?= __( 'Taxonomy Name', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							</label>
						</th>
						<td>
							<input 
								type="text"
								name="custom_taxonomy[<?= $i; ?>][args][labels][name]"
								value="<?= ( isset( $taxonomy['args']['labels']['name'] ) ? $taxonomy['args']['labels']['name'] : '' ); ?>"
								autocomplete="off"
								maxlength="32"
								class="taxonomy-name"
								aria-required="true" required>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][singular_name]">
								<?= __( 'Singular Name', 'wp-builder' ); ?>
								<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
							</label>
						</th>
						<td>
							<input
								type="text"
								name="custom_taxonomy[<?= $i; ?>][args][labels][singular_name]"
								value="<?= ( isset( $taxonomy['args']['labels']['singular_name'] ) ? $taxonomy['args']['labels']['singular_name'] : '' ); ?>"
								class="taxonomy-name-singular"
								autocomplete="off" required>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][search_items]">
								<?= __( 'Search Categories', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="custom_taxonomy[<?= $i; ?>][args][labels][search_items]" 
								value="<?= ( isset( $taxonomy['args']['labels']['search_items'] ) ? $taxonomy['args']['labels']['search_items'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][all_items]">
								<?= __( 'All Items', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="custom_taxonomy[<?= $i; ?>][args][labels][all_items]" 
								value="<?= ( isset( $taxonomy['args']['labels']['all_items'] ) ? $taxonomy['args']['labels']['all_items'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][parent_item]">
								<?= __( 'Parent Item', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="custom_taxonomy[<?= $i; ?>][args][labels][parent_item]" 
								value="<?= ( isset( $taxonomy['args']['labels']['parent_item'] ) ? $taxonomy['args']['labels']['parent_item'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
	 						<label for="custom_taxonomy[<?= $i; ?>][args][labels][edit_item]">
								<?= __( 'Edit Item', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="custom_taxonomy[<?= $i; ?>][args][labels][edit_item]" 
								value="<?= ( isset( $taxonomy['args']['labels']['edit_item'] ) ? $taxonomy['args']['labels']['edit_item'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][update_item]">
								<?= __( 'Update Item', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="custom_taxonomy[<?= $i; ?>][args][labels][update_item]" 
								value="<?= ( isset( $taxonomy['args']['labels']['update_item'] ) ? $taxonomy['args']['labels']['update_item'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][add_new_item]">
								<?= __( 'Add New Item', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="custom_taxonomy[<?= $i; ?>][args][labels][add_new_item]" 
								value="<?= ( isset( $taxonomy['args']['labels']['add_new_item'] ) ? $taxonomy['args']['labels']['add_new_item'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][new_item_name]">
								<?= __( 'New Item', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="custom_taxonomy[<?= $i; ?>][args][labels][new_item_name]" 
								value="<?= ( isset( $taxonomy['args']['labels']['new_item_name'] ) ? $taxonomy['args']['labels']['new_item_name'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][labels][menu_name]">
								<?= __( 'Menu Name', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text" 
								name="custom_taxonomy[<?= $i; ?>][args][labels][menu_name]" 
								value="<?= ( isset( $taxonomy['args']['labels']['menu_name'] ) ? $taxonomy['args']['labels']['menu_name'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][description]">
								<?= __( 'Description', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="text"
								name="custom_taxonomy[<?= $i; ?>][args][description]"
								value="<?= ( isset( $taxonomy['args']['description'] ) ? $taxonomy['args']['description'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<?= __( 'Display Settings', 'wp-builder' ); ?>
						</th>
						<td>
							<fieldset>
								<label for="custom_taxonomy[<?= $i; ?>][args][show_ui]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][show_ui]"
										value="1"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['show_ui'] ) && $taxonomy['args'] == true )  ? 'checked' : '' ); ?>>
									<?= __( 'Show UI', 'wp-builder' ); ?>
								</label>
								<br>
								<label for="custom_taxonomy[<?= $i; ?>][args][show_in_rest]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][show_in_rest]"
										value="1"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['show_in_rest'] ) && $taxonomy['args']['show_in_rest'] == true ) ? 'checked' : '' ); ?>>
									<?= __( 'Show in REST', 'wp-builder' ); ?>
									<span class="tooltip" data-tip="Must be checked to display in block editor.">
										<span class="dashicons dashicons-editor-help"></span>
									</span>
								</label>
								<br>
								<label for="custom_taxonomy[<?= $i; ?>][args][show_admin_column]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][show_admin_column]"
										value="1"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['show_admin_column'] ) && $taxonomy['args']['show_admin_column'] == true ) ? 'checked' : '' ); ?>>
									<?= __( 'Show Admin Column', 'wp-builder' ); ?>
								</label>
								<br>
								<label for="custom_taxonomy[<?= $i; ?>][args][show_in_nav_menus]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][show_in_nav_menus]"
										value="1"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['show_in_nav_menus'] ) && $taxonomy['args']['show_in_nav_menus'] == true ) ? 'checked' : '' ); ?>>
									<?= __( 'Show in Nav Menus', 'wp-builder' ); ?>
								</label>
								<br>
								<label for="custom_taxonomy[<?= $i; ?>][args][show_tagcloud]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][show_tagcloud]"
										value="1"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['show_tagcloud'] ) ** $taxonomy['args']['show_tagcloud'] == true ) ? 'checked' : '' ); ?>>
									<?= __( 'Show Tag Cloud', 'wp_builder' ); ?>
								</label>
								<br>
							</fieldset>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][public]">
								<?= __( 'Public', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="checkbox" 
								name="custom_taxonomy[<?= $i; ?>][args][public]" 
								value="1" 
								<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['public'] ) && $taxonomy['args']['public'] == true )  ? 'checked' : '' ); ?>>
						</td>
					</tr>

					<tr class="form-field">
						<th class="row">
	 						<label for="custom_taxonomy[<?= $i; ?>][args][hierarchical]">
								<?= __( 'Hierarchical', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input 
								type="checkbox" 
								name="custom_taxonomy[<?= $i; ?>][args][hierarchical]" 
								value="1" 
								<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['hierarchical'] ) && $taxonomy['args']['hierarchical'] == true ) ? 'checked' : '' ); ?>>
						</td>
					</tr>
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][query_var]">
								<?= __( 'Query Variable', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input
								type="checkbox"
								name="custom_taxonomy[<?= $i; ?>][args][query_var]"
								value="1"
								<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['query_var'] ) && $taxonomy['args']['query_var'] == true ) ? 'checked' : '' ); ?>>
						</td>
					</tr>			
				</table>
				<h3><?= __( 'Rewrite Settings', 'wp-builder' ); ?></h3>
				<table class="form-table">
					<tr class="form-field">
						<th class="row">
							<label for="custom_taxonomy[<?= $i; ?>][args][rewrite][slug]">
								<?= __( 'Slug', 'wp-builder' ); ?>
							</label>
						</th>
						<td>
							<input
								type="text"
								name="custom_taxonomy[<?= $i; ?>][args][rewrite][slug]"
								value="<?= ( isset( $taxonomy['args']['rewrite']['slug'] ) ? $taxonomy['args']['rewrite']['slug'] : '' ); ?>"
								autocomplete="off">
						</td>
					</tr>

					<tr class="form-field">
						<th class="row">
							<?= __( 'Capabilities', 'wp-builder' ); ?>
						</th>
						<td>
							<fieldset>
								<label for="custom_taxonomy[<?= $i; ?>][args][capabilities][manage_terms][manage_categories]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][capabilities][manage_terms]"
										id="custom_taxonomy[<?= $i; ?>][args][capabilities][manage_terms][manage_categories]"
										value="manage_categories"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['capabilities']['manage_terms'] ) && $taxonomy['args']['capabilities']['manage_terms'] == 'manage_categories' ) ? 'checked' : '' ); ?>>
									<?= __( 'Manage Terms', 'wp-builder' ); ?>
								</label>
								<br>
								<label for="custom_taxonomy[<?= $i; ?>][args][capabilities[manage_categories][edit_terms]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][capabilities][edit_terms]"
										id="custom_taxonomy[<?= $i; ?>][args][capabilities][edit_terms][manage_categories]"
										value="manage_categories"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['capabilities']['edit_terms'] ) && $taxonomy['args']['capabilities']['edit_terms'] == 'manage_categories' ) ? 'checked' : '' ); ?>>
									<?= __( 'Edit Terms', 'wp-builder' ); ?>
								</label>
								<br>
								<label for="custom_taxonomy[args][capabilities][delete_terms][manage_categories]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][capabilities][delete_terms]"
										id="custom_taxonomy[args][capabilities][delete_terms][manage_categories]"
										value="manage_categories"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['capabilities']['delete_terms'] ) && $taxonomy['args']['capabilities']['delete_terms'] == 'manage_categories' ) ? 'checked' : '' ); ?>>
									<?=  __( 'Delete Terms', 'wp-builder' ); ?>
								</label>
								<br>
								<label for="custom_taxonomy[args][capabilities][assign_terms][edit_posts]">
									<input
										type="checkbox"
										name="custom_taxonomy[<?= $i; ?>][args][capabilities][assign_terms]"
										id="custom_taxonomy[args][capabilities][assign_terms][edit_posts]"
										value="edit_posts"
										<?= ( ! isset( $taxonomy['args'] ) || ( isset( $taxonomy['args']['capabilities']['assign_terms'] ) && $taxonomy['args']['capabilities']['assign_terms'] == 'edit_posts' ) ? 'checked' : '' ); ?>>
									<?=  __( 'Assign Terms', 'wp-builder' ); ?>
								</label>
								<br>
							</fieldset>
						</td>
					</tr>
				</table>
			</fieldset>
			<p class="submit">
				<?php submit_button( __( 'Save Taxonomy', 'wp-builder' ), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>
		</form>
		<?php
	}

	/**
	 * Taxonomy delete form.
	 *
	 * @since 2.0.0
	 * @param string $form_url The URL.
	 * @param array  $taxonomy The taxonomy.
	 */
	public function taxonomy_delete_form( $form_url, $taxonomy ) {
		?>
		<form method="post" action="<?= $form_url; ?>" >
			<input type="hidden" name="taxonomy[machine_name]" value="<?= ( isset( $taxonomy['machine_name'] ) ? $taxonomy['machine_name'] : '' ); ?>">
			<input type="hidden" name="taxonomy[unique_id]" value="<?= ( isset( $taxonomy['unique_id'] ) ? $taxonomy['unique_id'] : '' ); ?>">
			<input type="hidden" name="action" value="delete">
			<input type="hidden" name="type" value="taxonomy">
			<?php wp_nonce_field( 'wp_builder_taxonomy_delete', 'taxonomy_delete' ); ?>
			<h2><?= __( 'Are you sure you want to delete this taxonomy?', 'wp-builder' ); ?></h2>
			<p><?= $taxonomy['args']['labels']['name']; ?></p>
			<p class="submit">
				<?php submit_button( __( 'Delete taxonomy', 'wp-builder' ), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>		
		</form>
		<?php
	}
}
