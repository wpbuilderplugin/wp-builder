<?php
/**
 * Menu Location Form class.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder\Forms;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Container MenuLocationForms class.
 */
class MenuLocationForms {
	/**
	 * Menu Locations Form.
	 *
	 * @since 1.0.0
	 * @param string $form_url      The form URL.
	 * @param int    $i             The current iteration.
	 * @param array  $menu_location An array of menu location info.
	 */
	public function menu_location_form( $form_url, $i = 0, $menu_location = [] ) {
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--<?= $tab; ?>" enctype="multipart/form-data">
			<?php wp_nonce_field( 'wp_builder_menu_location_settings', 'menu_location_settings' ); ?>
			<fieldset class="menu-location">
				<legend><?= ( isset( $menu_location['name'] ) ? __( 'Edit ' . $menu_location['name'], 'wp-builder' ) : __( 'Create Menu Location', 'wp-builder' ) ); ?></legend>
				<input 
					type="hidden" 
					name="menu_location[<?= $i; ?>][machine_name]" 
					<?php
					if ( isset( $menu_location['machine_name'] ) ) :
						print 'value="' . $menu_location['machine_name'] . '"';
					else:
						print 'class="menu-location-machine-name"';
					endif;
					?>>
				<table class="form-table">
					<tr class="form-field form-required">
						<th class="row">
							<label for="menu_location[<?= $i; ?>][name]"><?= __( 'Menu Location Name', 'wp-builder' ); ?></label>
						</th>
						<td>
							<input 
								type="text" 
								name="menu_location[<?= $i; ?>][name]" 
								value="<?= ( isset( $menu_location['name'] ) ? $menu_location['name'] : '' ); ?>"
								class="menu-location-name" 
								aria-required="true" 
								autocomplete="off"
								required>
						</td>
					</tr>
				</table>
			</fieldset>
			<?php submit_button( __( 'Save Menu', 'wp-builder' ), 'primary', 'submit' ); ?>
			<input type="hidden" name="wp_builder_submit" value="Y">
		</form>
		<?php
	}

	/**
	 * Menu location delete form.
	 *
	 * @since 2.0.0
	 * @param string $form_url      The form URL.
	 * @param array  $menu_location The menu location to delete.
	 */
	public function menu_location_delete_form( $form_url, $menu_location ) {
		?>
		<form method="post" action="<?= $form_url; ?>" class="wp-builder-page wp-builder-tab--<?= $tab; ?>">
			<input type="hidden" name="menu_location[machine_name]" value="<?= isset( $menu_location['machine_name'] ) ? $menu_location['machine_name'] : ''; ?>">
			<input type="hidden" name="action" value="delete">
			<input type="hidden" name="type" value="menu_location">
			<?php wp_nonce_field( 'wp_builder_menu_location_delete', 'menu_location_delete' ); ?>
			<h2><?= __( 'Are you sure you want to delete this menu location?', 'wp-builder' ); ?></h2>
			<p><?= $menu_location['name']; ?></p>
			<p class="submit">
				<?php submit_button( __( 'Delete Menu Location', 'wp_builder'), 'primary', 'submit' ); ?>
				<input type="hidden" name="wp_builder_submit" value="Y">
			</p>
		</form>
		<?php
	}
}
