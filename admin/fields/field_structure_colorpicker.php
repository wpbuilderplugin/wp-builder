<?php
/**
 * Taxonomy Views.
 *
 * @since 1.0.0
 *
 * @package wp-builder
 */

/**
 * Field structure colorpicker.
 *
 * @since 1.0.0
 * @param array $args The field configuration.
 */
function field_structure_colorpicker  ( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<table class="form-table">
		<tr class="form-field">
			<th class="row">
				<label for="<?= $field_name_prefix; ?>[default]">
					<?= __( 'Default Color', 'wp-builder' ); ?>
				</label>
			</th>
			<td>
				<input 
					type="text" 
					name="<?= $field_name_prefix; ?>[default]" 
					value="<?= ( isset( $field_params['default'] ) ? $field_params['default'] : '#ffffff' ); ?>">
			</td>
		</tr>
		<tr class="form-field">
			<th class="row">
				<label for="<?= $field_name_prefix; ?>[custom_palette]">
					<?= __( 'Custom Palette', 'wp-builder' ); ?>
				</label>
			</th>
			<td>
				<input 
					type="checkbox" 
					name="<?= $field_name_prefix; ?>[custom_palette]" 
					id="<?= $field_name_prefix; ?>[custom_palette]" 
					class="custom-palette" 
					value="true" 
					<?= ( isset( $field_params["custom_palette"]) ? 'checked' : '' ); ?>>
			</td>
		</tr>
	</table>
	<div class="metabox-fields-options-wrapper">
		<div class="custom-palette--wrapper" <?= ( isset( $field_params['custom_palette'] ) && $field_params['custom_palette'] == true ? '' : ' style="display:none;"' ); ?>>
			<h4><?= __( 'Colors', 'wp-builder' ); ?></h4>
			<ul class="sortable-field-options">
				<?php 
					if ( isset( $field_params['options'] ) && is_array( $field_params['options'] ) ) : 
						foreach ( $field_params['options'] as $key => $value ) : 
							$args['count'] = $key;
							field_options_colorpicker( $args );
						endforeach;
					endif; 
				?>
			</ul>
			<input 
				type="button" 
				value="<?= __( 'Add a Color', 'wp-builder' ); ?>"  
				data-name="<?= $field_name_prefix; ?>[options]" 
				data-count="<?= ( isset( $field_params['options'] ) ? count( $field_params['options'] ) - 1 : -1 ); ?>" 
				data-type="colorpicker" 
				class="wp_builder-meta-add-option">
		</div>
	</div>
	<?php
}

function field_options_colorpicker( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	$count = $args['count'];
	?>
	<li class="field-option-item field-option-item-<?= $count; ?>" data-field-option="<?= $count; ?>" data-pickles="dill">
		<div class="postbox">
			<fieldset class="<?= $field_name_prefix; ?>[options]" data-count="<?= $count; ?>">
				<table class="form-table">
					<tr class="form-field">
						<th class="row">
							<legend><?= __( 'Color', 'wp-builder' ) . ' ' . $count; ?></legend>
							<p class="button--wrapper">
								<a 
									class="wp_builder-meta-delete-option wp_builder-trash" 
									href="<?= $form_url; ?>&action=delete&metabox=<?= $i; ?>&item=option&item_id=<?= $count; ?>&parent_id=<?= $f; ?>" 
									data-name="<?= $field_name_prefix; ?>[options][colors]" 
									data-warning="<?= __( 'Are you sure you want to delete this option?', 'wp-builder' ); ?>">
									<span class="dashicons dashicons-trash"></span>
									<?= __( 'Delete Color', 'wp-builder' ); ?>
								</a>
							</p>
						</th>
						<td>
							<table class="form-table">
								<tr class="form-field">
									<th class="row">
										<label for="<?= $field_name_prefix; ?>[options][<?= $count; ?>][value]">
											<?= __( 'Color Value', 'wp-builder' ); ?>
										</label>
									</th>
									<td>
										<p>
											<input 
												type="text" 
												name="<?= $field_name_prefix; ?>[options][<?= $count; ?>][value]" 
												value="<?= ( isset( $field_params['options'][ $count ]['value'] ) ? $field_params['options'][ $count ]['value'] : '' ); ?>">
										</p>
										<p class="description">
											<?= __( 'Enter the color value in HEX ( #fafafa )', 'wp-builder' ); ?>
										</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</fieldset>
		</div>
	</li>
	<?php
}
