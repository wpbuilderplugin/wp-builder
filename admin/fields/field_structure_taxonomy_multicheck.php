<?php
/**
 * Taxonomy Multicheck field structure.
 *
 * @since 1.0.0
 *
 * @package wp-builder
 */

/**
 * Field Structure for 'taxonomy_multicheck'.
 *
 * @since 1.0.0
 * @param array $args The field configuration.
 */
function field_structure_taxonomy_multicheck( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<fieldset class="<?= $field_name_prefix; ?>[taxonomy]">
		<table class="form-table">
			<tr class="form-field form-required">
				<th class="row">
					<label for="<?= $field_name_prefix; ?>[taxonomy]">
						<?= __( 'Choose Taxonomy', 'wp-builder' ); ?>
						<span class="description">(<?= __( 'required', 'wp-builder' ); ?>)</span>
					</label>
				</th>
				<td>
					<select name="<?= $field_name_prefix; ?>[taxonomy]" id="<?= $field_name_prefix; ?>[taxonomy]" aria-required="true" required>
						<?php
						$taxonomies = get_taxonomies( '', 'objects');
						foreach ( $taxonomies as $taxonomy) {
							echo '<option value="' . $taxonomy->name . '"';
							if ( isset( $field_params['taxonomy'] ) && $field_params['taxonomy'] == $taxonomy->name ) {
 								echo ' selected';
							}
							echo '>' . $taxonomy->labels->name . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
		</table>
	</fieldset>
	<?php
}
