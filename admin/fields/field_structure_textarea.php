<?php
/**
 * Textarea field structure.
 *
 * @since 1.0.0
 *
 * @package wp-builder
 */

/**
 * Field Structure for textarea.
 *
 * @since 1.0.0
 * @param array $args The field configuration.
 */
function field_structure_textarea( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<table class="form-table">
		<tr class="form-field">
			<th class="row">
				<label for="<?= $field_name_prefix; ?>[std]">
					<?= __( 'Standard Value ( optional )', 'wp-builder' ); ?>
				</label>
			</th>
			<td>
				<input
					type="text"
					name="<?= $field_name_prefix; ?>[std]"
					value="<?=  ( isset( $field_params['std'] ) ? $field_params['std'] : '' ) ?>" >
			</td>
		</tr>
	</table>
	<?php 
}
