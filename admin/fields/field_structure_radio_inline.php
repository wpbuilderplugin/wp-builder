<?php
/**
 * Radio Inline field structure.
 *
 * @since 1.0.0
 *
 * @package wp-builder
 */

/**
 * Field structure 'radio_inline'.
 *
 * @since 1.0.0
 * @param array $args The field configuration.
 */
function field_structure_radio_inline( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<div class="metabox-fields-options-wrapper">
		<h4><?= __( 'Options', 'wp-builder' ); ?></h4>
		<ul class="sortable-field-options">
			<?php
			if ( isset( $field_params['options'] ) && is_array( $field_params['options'] ) ) :
				foreach( $field_params['options'] as $key => $value ) :
					$args['count'] = $key;
					field_options_radio_inline( $args );
				endforeach;
			else :
				$count = 0;
				$args['count'] = $count;
				field_options_radio_inline( $args );
			endif;
			?>
		</ul>
		<input
			type="button"
			value="<?= __( 'Add Another Option', 'wp-builder' ); ?>"
			data-name="<?= $field_name_prefix; ?>[options]"
			data-count="<?= ( array_key_exists( 'options', $field_params ) ? count( $field_params['options'] ) - 1 : 0 ); ?>"
			data-type="radio_inline"
			class="wp_builder-meta-add-option">
	</div>
	<?php
}

/**
 * Options.
 *
 * @since 1.0.0
 * @param array $args The options configuration.
 */
function field_options_radio_inline( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	$count = $args['count'];
	?>
	<li class="field-option-item field-option-item-<?= $count; ?>" data-field-option="<?= $count; ?>">
		<div class="postbox">
			<fieldset class="<?= $field_name_prefix; ?>[options]" data-count="<?= $count; ?>">
				<table class="form-table">
					<tr class="form-field">
						<th class="row">
							<legend><?= __( 'Option', 'wp-builder' ) . ' ' . $count; ?></legend>
							<p class="button--wrapper">
								<a
									class="wp_builder-meta-delete-option wp_builder-trash"
									href="<?= $form_url; ?>&action=delete&metabox=<?= $i; ?>&item=option&item_id=0&parent_id=<?= $f; ?>"
									data-name="<?= $field_name_prefix; ?>[options]"
									data-warning="<?= __( 'Are you sure you want to delete this option?', 'wp-builder' ); ?>">
									<span class="dashicons dashicons-trash"></span>
									<?= __( 'Delete Option', 'wp-builder'); ?>
								</a>
							</p>
						</th>
						<td>
							<table class="form-table">
								<tr class="form-field">
									<th class="row">
										<label for="<?= $field_name_prefix; ?>[options][<?= $count; ?>][name]">
											<?= __( 'Option Name', 'wp-builder' ); ?>
										</label>
									</th>
									<td>
										<p>
											<input
												type="text"
												name="<?= $field_name_prefix; ?>[options][<?= $count; ?>][name]"
												value="<?= ( isset( $field_params['options'][ $count ]['name'] ) ? $field_params['options'][ $count ]['name'] : '' ); ?>"
												required>
										</p>
										<p class="description"><?= __( 'The human readable name or label for this option.', 'wp-builder' ); ?></p>
									</td>
								</tr>
								<tr>
									<th class="row">
										<label for="<?= $field_name_prefix; ?>[options][<?= $count; ?>][value]">
											<?= __( 'Option Value', 'wp-builder' ); ?>
										</label>
									</th>
									<td>
										<p>
											<input
												type="text"
												name="<?= $field_name_prefix; ?>[options][<?= $count; ?>][value]"
												value="<?= ( isset( $field_params['options'][ $count ]['value'] ) ? $field_params['options'][ $count ]['name'] : '' ); ?>"
												required>
										</p>
										<p class="description"><?= __( 'The machine name or key for this option.', 'wp-builder' ); ?></p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</fieldset>
		</div>
	</li>
	<?php
}
