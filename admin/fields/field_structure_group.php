<?php
/**
 * Get field structure.
 *
 * @since 2.0.1
 * @package wp-builder
 */

/**
 * Field structure group.
 *
 * @since 2.0.0
 * @param array $args The field configuration.
 */
function field_structure_group( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<table class="form-table">
		<tr class="form-field">
			<th class="row">
				<label for="<?= $field_name_prefix; ?>[repeatable]">
					<?= __( 'Repeatable', 'wp-builder' ); ?>
				</label>
			</th>
			<td>
				<input
					type="checkbox"
					name="<?= $field_name_prefix; ?>[repeatable]"
					id="<?= $field_name_prefix; ?>[repeatable]"
					value="true"
					<?= ( isset( $field_params["repeatable"]) ? 'checked' : '' ); ?>>
			</td>
		</tr>
		<tr class="form-field">
			<th class="row"><?= __( 'Group Fields', 'wp-builder' ); ?></th>
			<td>
				<div class="group-fields">
					<ul class="group-fields-wrapper sortable ui-sortable" id="<?= $field_name_prefix; ?>[fields]">
					</ul>
				</div>
			</td>
		</tr>
		<tr>
			<th class="row"><?= __( 'Add Fields to Group', 'wp-builder' ); ?></th>
			<td>
				<input
					type="button"
					value="<?= __( 'Add Field', 'wp-builder' ); ?>"
					data-field-count="<?= ( isset( $field_params['fields'] ) ? count( $field_params['fields'] ) + 1 : '0' ); ?>"
					data-field-id="<?= $field_name_prefix; ?>[fields]"
					class="wp-builder-add-group-fields">
			</td>
		</tr>
	</table>
	<?php	
}
