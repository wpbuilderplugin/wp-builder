<?php
/**
 * Contains field structure 'file'.
 *
 * @since 1.0.0
 *
 * @package wp-builder
 */

/**
 * Field structure 'file'.
 *
 * @since 1.0.0
 * @param array $args The field configuration.
 */
function field_structure_file( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<fieldset class="<?= $field_name_prefix; ?>[options]" >
		<legend><?= __( 'File Options', 'wp-builder' ); ?></legend>
			<table class="form-table">
				<tr class="form-field">
					<th class="row"><?= __( 'Show Text Input for the file URL', 'wp-builder' ); ?></th>
					<td>
						<label for="<?= $field_name_prefix; ?>[options][url][yes]">
							<input
								type="radio"
								name="<?= $field_name_prefix; ?>[options][url]"
								id="<?= $field_name_prefix; ?>[options][url][yes]"
								value="true"
								<?= ( ( isset( $field_params['options']['url'] ) && true == $field_params['options']['url'] ) ? 'checked' : '' ); ?>
							>
							<?= __( 'Show URL Field', 'wp-builder' ); ?>
						</label>
						<br>
						<label for="<?= $field_name_prefix; ?>[options][url][no]">
							<input
								type="radio"
								name="<?= $field_name_prefix; ?>[options][url]"
								id="<?= $field_name_prefix; ?>[options][url][no]"
								value="false"
								<?= ( ( isset( $field_params['options']['url'] ) && false == $field_params['options']['url'] ) ? 'checked' : '' ); ?>
							>
							<?= __( 'Hide URL Field', 'wp-builder' ); ?>
						</label>
					</td>
				</tr>
			</table>
	</fieldset>
	<?php
}
