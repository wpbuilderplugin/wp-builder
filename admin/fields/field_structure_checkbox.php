<?php
/**
 * Checkbox field structure.
 *
 * @since 2.0.0
 *
 * @package wp-builder
 */

/**
 * Field Structure for checkbox.
 *
 * @since 1.0.0
 * @param array $args The configuration.
 */
function field_structure_checkbox( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<div class="metabox-fields-options-wrapper">
		<h4><?= __( 'Option', 'wp-builder' ); ?></h4>
		<ul>
			<?php
			if ( isset( $field_params['options'] ) && is_array( $field_params['options'] ) ) :
				foreach ( $field_params['options'] as $key => $value ) :
					$args['count'] = $key;
					field_options_checkbox( $args );
				endforeach;
			else :
				$count = 0;
				$args['count'] = $count;
				field_options_checkbox( $args );
			endif;
			?>
		</ul>
	</div>
	<?php
}

/**
 * Options.
 *
 * @since 1.0.0
 * @param array $args The options configuration.
 */
function field_options_checkbox( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	$count = $args['count'];
	?>
	<li class="field-option-item field-option-item-'<?= $count; ?>" data-field-option="<?= $count; ?>">
		<div class="postbox">
			<fieldset class="<?= $field_name_prefix; ?>[options]" data-count="<?= $count; ?>">
				<table class="form-table">
					<tr class="form-field">
						<th class="row">
							<legend><?= __( 'Option', 'wp-builder' ) . ' ' . $count; ?></legend>
						</th>
						<td>
							<table class="form-table">
								<tr class="form-field">
									<th class="row">
										<label for="<?= $field_name_prefix; ?>[options][<?= $count; ?>][name]"><?= __( 'Option Name', 'wp-builder' ); ?></label>
									</th>
									<td>
										<p>
											<input 
												type="text" 
												name="<?= $field_name_prefix; ?>[options][<?= $count; ?>][name]" 
												value="<?= ( isset( $field_params['options'][ $count ]['name'] ) ? $field_params['options'][ $count ]['name'] : '' ); ?>"
												required
											>
										</p>
										<p class="description"><?= __( 'The human readable name or label for this option.', 'wp-builder' ); ?></p>
									</td>
								</tr>
								<tr>
									<th class="row">
										<label for="<?= $field_name_prefix; ?>[options][<?= $count; ?>][value]"><?= __( 'Option Value', 'wp-builder' ); ?></label>
									</th>
									<td>
										<p>
											<input 
												type="text" 
												name="<?= $field_name_prefix; ?>[options][<?= $count; ?>][value]" 
												value="<?= ( isset( $field_params['options'][ $count ]['value'] ) ? $field_params['options'][ $count ]['value'] : '' ); ?>"
												required
											>
										</p>
										<p class="description"><?= __( 'The machine name or key for this option.', 'wp-builder' ); ?></p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</fieldset>
		</div>
	</li>
	<?php
}
