<?php
/**
 * Post Search Ajax field structure.
 *
 * @since 1.0.1
 *
 * @package wp-builder
 */

/**
 * Field structure for cmb2-field-post-search-ajax 
 *
 * @ref https://github.com/alexis-magina/cmb2-field-post-search-ajax
 * @since 1.0.1
 * @param array $args The field configuration
 */
function field_structure_post_search_ajax( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $ars['field_id'];
	$form_url = $args['url'];
	?>
	<table class="form-table">
		<tr class="form-field">
			<th class="row">
				<label for="<?= $field_name_prefix; ?>[limit]"><?= __( 'Limit', 'wp-builder' ); ?></label>
			</th>
			<td>
				<input
					type="number"
					min="-1"
					name="<?= $field_name_prefix; ?>[limit]" id="<?= $field_name_prefix; ?>[limit]"
					value="<?= ( isset( $field_params['limit'] ) ? $field_params['limit'] : '1' ); ?>"
				>
			</td>
		<tr class="form-field">
			<th class="row">
				<label for="<?= $field_name_prefix; ?>[sortable]"><?= __( 'Sortable', 'wp-builder' ); ?></label>
			</th>
			<td>
				<input
					type="checkbox"
					name="<?= $field_name_prefix; ?>[sortable]"
					id="<?= $field_name_prefix; ?>[sortable]"
					value="1"
					<?= ( isset( $field_params['sortable'] ) ? 'checked' : '' ); ?>>
			</td>
		</tr>
		<tr class="form-field">
			<th class="row">
				<?= __( 'Post types', 'wp-builder' ); ?>
			</th>
			<td>
			<?php
			$post_types = get_post_types( '', 'objects' );
			foreach ( $post_types as $post_type ) :
				?>
				<label for="<?= $field_name_prefix; ?>[post_type][<?= $post_type->name; ?>]">
					<input
						type="checkbox"
						name="<?= $field_name_prefix; ?>[post_type][]"
						id="<?= $field_name_prefix; ?>[post_type][<?= $post_type->name; ?>]"
						value="<?= $post_type->name; ?>"
						<?= ( isset( $field_params['post_type'] ) && in_array( $post_type->name, $field_params['post_type'] ) ? 'checked' : '' ); ?>
					>
					<?= $post_type->labels->name; ;?>
				</label>
				<br>
			<?php endforeach; ?>
			</td>
		</tr>
	</table>
	<?php
}
