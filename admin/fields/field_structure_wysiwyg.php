<?php
/**
 * WYSIWYG field structure.
 *
 * @since 1.0.0
 *
 * @package wp-builder
 */

/**
 * Field Structure for wysiwyg.
 *
 * @since 1.0.0
 * @param array $args The field configuration.
 */
function field_structure_wysiwyg( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<div class="postbox">
		<fieldset class="<?= $field_name_prefix; ?>[options]">
			<legend><?= __( 'WYSIWYG options', 'wp-builder' ); ?></legend>
			<input type="hidden" name="<?= $field_name_prefix; ?>[options][dfw]" value="0" >
			<input type="hidden" name="<?= $field_name_prefix; ?>[options][tinymce]" value="1" >
			<table class="form-table">
				<tr class="form-field">
					<th class="row">
						<label for="<?= $field_name_prefix; ?>[wpautop]"><?= __( 'Use WPAutop?', 'wp-builder' ); ?></label>
					</th>
					<td>
						<input
							type="checkbox"
							name="<?= $field_name_prefix; ?>[options][wpautop]"
							value="1"
							<?= ( ! isset( $field_params['options'] ) || ( isset( $field_params['options']['wpautop'] ) && $field_params['options']['wpautop'] == 1 ) ? 'checked' : '' ); ?>
						>
					</td>
				</tr>
				<tr class="form-field">
					<th class="row">
						<label for="<?= $field_name_prefix; ?>[options][media_buttons]"><?= __( 'Show insert / upload buttons', 'wp-builder' ); ?></label>
					</th>
					<td>
						<input
							type="checkbox"
							name="<?= $field_name_prefix; ?>[options][media_buttons]"
							value="1"
							<?= ( isset( $field_params['options']['media_buttons'] ) && $field_params['options']['media_buttons'] == true ? 'checked' : '' ); ?>>
					</td>
				</tr>
				<tr class="form-field">
					<th class="row">
						<label for="<?= $field_name_prefix; ?>[options][textarea_name]"><?= __( 'Textarea name', 'wp-builder' ); ?></label>
					</th>
					<td>
						<input
							type="text"
							name="<?= $field_name_prefix; ?>[options][textarea_name]"
							value="<?= isset( $field_params['options']['textarea_name'] ) ? $field_params['options']['textarea_name'] : ''; ?>">
					</td>
				</tr>
				<tr class="form-field">
					<th class="row">
						<label for="<?= $field_name_prefix; ?>[options][textarea_rows]"><?= __( 'Textarea Rows', 'wp-builder' ); ?></label>
					</th>
					<td>
						<input
							type="number"
							min="0"
							name="<?= $field_name_prefix; ?>[options][textarea_rows]"
							value="<?= isset ( $field_params['options']['textarea_rows'] ) && ! empty( $field_params['options']['textarea_rows'] ) ? $field_params['options']['textarea_rows'] : '10'; ?>">
					</td>
				</tr>
				<tr>
					<th class="row">
						<label for="<?= $field_name_prefix; ?>[options][tabindex]"><?= __( 'Tab Index', 'wp-builder' ); ?></label>
					</th>
					<td>
						<input
							type="number"
							name="<?= $field_name_prefix; ?>[options][tabindex]"
							value="<?= isset( $field_params['options']['tabindex'] ) ? $field_params['options']['tabindex'] : ''; ?>"
						>
					</td>
				</tr>
				<tr class="form-field">
					<th class="row">
						<label for="<?= $field_name_prefix; ?>[options][editor_class]">
							<?= __( 'Add extra class(es] to the editor', 'wp-builder' ); ?>
						</label>
					</th>
					<td>
						<?php
						$editor_class = '';
						if ( isset( $field_params['options']['editor_class'] ) ) {
							$editor_class = $field_params['options']['editor_class'];
						}
						?>
						<input type="text" name="<?= $field_name_prefix; ?>[options][editor_class]" value="<?= $editor_class; ?>">
					</td>
				</tr>
				<tr class="form-field">
					<th class="row">
						<label for="<?= $field_name_prefix; ?>[options][teeny]">
							<?= __( 'Output the minimal editor config user in PressThis', 'wp-builder' ); ?>
						</label>
					</th>
					<td>
						<input
							type="checkbox"
							name="<?= $field_name_prefix; ?>[<?= $f; ?>][options][teeny]"
							value="1" 
							<?php
							if ( isset( $field_params['options']['teeny'] ) && $field_params['options']['teeny'] == 1 ) {
								echo ' checked="checked" ';
							}
							?>
						>
					</td>
				</tr>
				<tr class="form-field">
					<th class="row">
						<label for="<?= $field_name_prefix; ?>[options][quicktags]"><?= __( 'Load Quicktags', 'wp-builder' ); ?></label>
						<p><?= __( 'Unselect to disable the editor\'s Visual and Text tabs.', 'wp-builder' ); ?></p>
					</th>
					<td>
						<input type="checkbox" name="<?= $field_name_prefix; ?>[options][quicktags]" value="1" 
						<?php 
							if ( ! isset( $field_params['options'] ) || ( isset( $field_params['options']['quicktags'] ) && $field_params['options']['quicktags'] == true ) ) {
								echo 'checked="checked"';
							}
						?>
						>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	<?php
}

/**
 * Provides a preview of this field.
 *
 * @since 2.0.0
 * @param array $args The preview configuration.
 */
function field_preview_wysiwyg ( $args ) {
	?>
	<table class="form-table">
		<tr>
		</tr>
	</table>
	<?php
}
