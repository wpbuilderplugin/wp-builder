<?php
/**
 * Title field structure.
 *
 * @since 1.0.0
 *
 * @package wp-builder
 */

/**
 * Field Structure for title.
 *
 * @since 1.0.0
 * @param array $args The field configuration.
 */
function field_structure_title( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<input
		type="hidden"
		name="<?= $field_name_prefix; ?>[on_front]"
		value="<?= ( isset( $field_params['on_front'] ) && $field_params['on_front'] == true ) ? $field_params['on_front'] ? 0 ); ?>">
	<?php
}
