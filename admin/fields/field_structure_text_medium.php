<?php
/**
 * Text Medium field structure.
 *
 * @since 1.0.0
 *
 * @package wp-builder
 */

/**
 * Field Structure for text_medium.
 *
 * @since 1.0.0
 * @param array $args The field configuration.
 */
function field_structure_text_medium( $args ) {
	$field_name_prefix = $args['field_name_prefix'];
	$field_params = $args['field_params'];
	$i = $args['metabox_id'];
	$f = $args['field_id'];
	$form_url = $args['url'];
	?>
	<table class="form-table">
		<tr class="form-field">
			<th class="row">
				<label for="<?= $field_name_prefix; ?>[repeatable]"><?= __( 'Repeatable', 'wp-builder' ); ?></label>
			</th>
			<td>
				<input
					type="checkbox"
					name="<?= $field_name_prefix; ?>[repeatable]"
					id="<?= $field_name_prefix; ?>[repeatable]"
					value="true"
					<?= ( isset( $field_params["repeatable"]) ? 'checked' : '' ); ?>>
			</td>
		</tr>
	</table>
	<?php
}
