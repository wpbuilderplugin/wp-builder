	<h2><?= __( 'WP Builder', 'wp-builder' ); ?></h2>
	<p><?= __( 'Configured options for ', 'wp-builder' ); ?>
		<?php bloginfo( 'name' ); ?>
	</p>
	<div class="notice notice-warning">
		<p><?= __( 'WARNING: This is not a toy.  Changing these settings can have disasterous affects on your site if you do not have a solid understanding of WordPress themes, PHP, etc.  You should always test changes on a development site.  When you are happy with your changes, you can deploy to production using the Import tool.', 'wp-builder' ); ?></p>
	</div>
	<p><?= __( 'WP Builder provides a way to quickly configure structured content in WordPress.  This tools is designed to allow site builders, information architechs, and developers to create powerful WordPress features much faster.', 'wp-builder' ); ?></p>
	<?php $this->sidebar_views->options_view( $form_url ); ?>
	<?php $this->post_type_views->options_view( $form_url ); ?>
	<?php $this->taxonomy_views->options_view( $form_url ); ?>
	<?php $this->metabox_views->options_view( $form_url ); ?>
	<?php $this->menu_location_views->options_view( $form_url ); ?>
	<?php $this->customizer_views->panels_view( $form_url ); ?>
	<?php $this->customizer_views->sections_view( $form_url ); ?>
	<?php $this->customizer_views->settings_view( $form_url ); ?>