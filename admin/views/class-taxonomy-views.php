<?php
/**
 * Taxonomy Views.
 *
 * @since 2.0.0
 *
 * @package wp-builder
 */

namespace WpBuilder\Views;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\TaxonomyManager;

/**
 * Contains TaxonomyViews class.
 */
class TaxonomyViews {

	/**
	 * WpBuilder\TaxonomyManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\TaxonomyManager $taxonomy_manager
	 */
	protected $taxonomy_manager;

	/**
	 * Constructs a new \WpBuilder\Views\TaxonomyViews object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}
	/**
	 * Load dependencies.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function load_dependencies() {
		$this->taxonomy_manager = new \WpBuilder\TaxonomyManager();
	}

	/**
	 * Options preview.
	 *
	 * @since 1.0.0
	 * @param string $form_url The form URL.
	 */
	public function options_view( $form_url ) {
		$taxonomies = $this->taxonomy_manager->get_taxonomies();
		?>
		<section class="overview">
			<header class="overview-header">
				<h3 class="overview-title"><?= __( 'Taxonomies', 'wp-builder' ); ?>
					<a href="<?= wp_nonce_url( $form_url . '&tab=taxonomies&action=add', 'wp-builder' ); ?>" class="page-title-action">
						<?= __( 'Add Taxonomy', 'wp-builder' ); ?>
					</a>
				</h3>
			</header>
			<?php
			if ( ! empty( $taxonomies ) ) :
				$taxonomy_json = json_encode( $taxonomies );
				?>
				<aside class="export--wrapper">
					<div class="export-field-container postbox">
						<div class="button--wrapper">
							<button
								class="select-export" 
								data-select="export-taxonomies" 
								aria-label="<?= __( 'Select Taxonomies configuration.', 'wp-builder' ); ?>" 
								title="<?= __( 'Select Taxonomies configuration.', 'wp-builder' ); ?>" 
								type="button">
								<span class="dashicons dashicons-admin-page" aria-hidden="true"></span>
							</button>
							<button
								class="hide-export close-button" 
								data-close="export-taxonomies" 
								aria-label="<?= __( 'Close Taxonomies export', 'wp-builder' ); ?>" 
								title="<?= __( 'Close Taxonomies export', 'wp-builder' ); ?>" 
								type="button">
								<span class="dashicons dashicons-no" aria-hidden="true"></span>
							</button>
						</div>
						<textarea class="export-field export-taxonomies"><?= $taxonomy_json; ?></textarea>
					</div>
					<button
						class="show-export button" 
						data-show="export-taxonomies" 
						aria-label="<?= __( 'Show taxonomy export', 'wp-builder' ); ?>">
						<?= __( 'Export Taxonomy Configuration', 'wp-builder' ); ?>
					</button>
				</aside>
				<div class="wp_builder-accordion">
					<?php foreach ( $taxonomies as $i => $taxonomy ) : ?>
						<h3 class="accordion-title"><?= $taxonomy['args']['labels']['name']; ?></h3>
						<div class="accordion-contents">
							<div class="options-preview">		
								<div>
									<p><strong><?= __( 'Machine Name' , 'wp-builder' ); ?>: </strong> <?= $taxonomy['machine_name']; ?></p>
									<p>
										<strong><?= __( 'Description' , 'wp-builder' ); ?>: </strong>
										<?= isset( $taxonomy['args']['description'] ) ? $taxonomy['args']['description'] : ''; ?>
									</p>
									<div class="options-preview">
										<p><strong><?= __( 'Show on:', 'wp-builder' ); ?></strong></p>
										<ul>
											<?php foreach ( $taxonomy['post_types'] as $post_type ) : ?>
												<?php
												$post_type_object = get_post_type_object( $post_type );
												if ( $post_type_object ) :
													?>
													<li><?= $post_type_object->label; ?></li>
												<?php endif; ?>
											<?php endforeach ?>
										</ul>
									</div>
									<hr>
									<div class="options-preview">
										<p><strong><?= __( 'Labels', 'wp-builder' ); ?></strong></p>
										<ul>
											<?php foreach ( $taxonomy['args']['labels'] as $label => $name ) : ?>
												<?php
												if ( $name == '' ) :
													$name = '<em>(' . __( 'Not Set', 'wp-builder' ) . ')</em>';
												endif;
												?>
												<li><strong><?= $label; ?></strong>: <?= $name; ?></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<hr>
									<p>
										<strong><?= __( 'Show UI', 'wp-builder' ); ?>: </strong> 
										<?= ( isset( $taxonomy['args']['show_ui'] ) && true == $taxonomy['args']['show_ui'] ? __( 'True', 'wp-builder' ) : __( 'False', 'wp-builder' ) ); ?>
									</p>
									<p>
										<strong><?= __( 'Show in REST', 'wp-builder' ); ?>: </strong> 
										<?= ( isset( $taxonomy['args']['show_in_rest'] ) && true == $taxonomy['args']['show_in_rest'] ? __( 'True', 'wp-builder' ) : __( 'False', 'wp-builder' ) ); ?>
									</p>
									<p>
										<strong><?= __( 'Hierarchical', 'wp-builder' ); ?>: </strong> 
										<?= ( isset( $taxonomy['args']['hierarchical'] ) && true == $taxonomy['args']['hierarchical'] ? __( 'True', 'wp-builder' ) : __( 'False', 'wp-builder' ) ); ?>
									</p>
									<p>
										<strong><?= __( 'Show Admin Columns', 'wp-builder' ); ?>: </strong> 
										<?= ( isset( $taxonomy['args']['show_admin_column'] ) && true == $taxonomy['args']['show_admin_column'] ? __( 'True', 'wp-builder' ) : __( 'False', 'wp-builder' ) ); ?>
									</p>
									<p>
										<strong><?= __( 'Query Var', 'wp-builder' ); ?>: </strong> 
										<?= ( isset( $taxonomy['args']['query_var'] ) && true == $taxonomy['args']['query_var'] ? __( 'True', 'wp-builder' ) : __( 'False', 'wp-builder' ) ); ?>
									</p>
									<p>
										<strong><?= __( 'Public', 'wp-builder' ); ?>: </strong> 
										<?= ( isset( $taxonomy['args']['public'] ) && true == $taxonomy['args']['public'] ? __( 'True', 'wp-builder' ) : __( 'False', 'wp-builder' ) ); ?>
									</p>
									<p>
										<strong><?= __( 'Show in nav menus' , 'wp-builder' ); ?>: </strong>
										<?= ( isset( $taxonomy['args']['show_in_nav_menus'] ) && true == $taxonomy['args']['show_in_nav_menus'] ? __( 'True', 'wp-builder' ) : __( 'False', 'wp-builder' ) ); ?>
									</p>
									<p>
										<strong><?= __( 'Show Tagcloud', 'wp-builder' ); ?>: </strong> 
										<?= ( isset( $taxonomy['args']['show_tagcloud'] ) && true == $taxonomy['args']['show_tagcloud'] ? __( 'True', 'wp-builder' ) : __( 'False', 'wp-builder' ) ); ?>
									</p>
									<div class="options-preview">
										<p><strong><?= __( 'Rewrite', 'wp-builder' ); ?></strong></p>
										<ul>
											<?php foreach ( $taxonomy['args']['rewrite'] as $label => $name ) : ?>
												<li><strong><?= $label; ?></strong>: <?= $name; ?></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<hr>
									<?php if ( array_key_exists( 'capabilities', $taxonomy['args'] ) ) : ?>
										<div class="options-preview">
											<p><strong><?= __( 'Capabilities', 'wp-builder' ); ?></strong></p>
											<ul>
											<?php
											foreach ( $taxonomy['args']['capabilities'] as $label => $name ) :
												if ( '' == $name ) {
													$name = '<em>(' . __( 'Not Set', 'wp-builder' ) . ')</em>';
												}
												print "<li><strong>{$label}</strong>: {$name}</li>";
											endforeach;
											?>
											</ul>
										</div>
										<hr>
									<?php endif; ?>
								</div>
							</div>
							<hr>
							<p class="button--wrapper">
								<a href="<?= wp_nonce_url( $form_url . '&tab=taxonomies&action=edit&item=' . $i, 'wp-builder' ); ?>" class="button">
									<?= __( 'Edit Taxonomy', 'wp-builder' ); ?>
								</a>
								<a href="<?= wp_nonce_url( $form_url . '&tab=taxonomies&action=delete&item=' . $i, 'wp-builder' ); ?>" class="wp_builder-trash delete--taxonomy">
									<span class="dashicons dashicons-trash"></span><?= __( 'Delete Taxonomy', 'wp-builder' ); ?>
								</a>
							</p>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else : ?>
				<p><?= __( 'No custom taxonomies defined', 'wp-builder' ); ?></p>
			<?php endif; ?>
			</section>					
		<?php
	}
}
