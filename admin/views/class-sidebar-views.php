<?php
/**
 * Sidebar Views.
 *
 * @since 2.0.0
 *
 * @package wp-builder
 */

namespace WpBuilder\Views;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\SidebarManager;

/**
 * Contains SidebarViews class.
 */
class SidebarViews {

	/**
	 * WpBuilder\SidebarManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\SidebarManager $sidebar_manager
	 */
	protected $sidebar_manager;

	/**
	 * Constructs a new \WpBuilder\Views\SidebarViews object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}
	/**
	 * Load dependencies.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 */
	private function load_dependencies() {
		$this->sidebar_manager = new \WpBuilder\SidebarManager();
	}

	/**
	 * Options preview.
	 *
	 * @since 1.0.0
 	 * @param string $form_url The form URL.
 	 */
	public function options_view( $form_url ) {
		$sidebars = $this->sidebar_manager->get_sidebars();
		?>
		<section class="overview">
			<header class="overview-header">
				<h3 class="overview-title"><?= __( 'Sidebars', 'wp-builder' ); ?>
					<a href="<?= wp_nonce_url( $form_url . '&tab=sidebars&action=add', 'wp-builder' ); ?>" class="page-title-action">
						<?= __( 'Add Sidebar', 'wp-builder' ); ?>
					</a>
				</h3>
			</header>
			<?php
			if ( ! empty( $sidebars ) ) :
				$sidebar_json = json_encode( $sidebars );
				?>
				<aside class="export--wrapper">
					<div class="export-field-container postbox">
						<div class="button--wrapper">
							<button
								class="select-export" 
								data-select="export-sidebars" 
								aria-label="<?= __( 'Select Sidebar configuration.', 'wp-builder' ); ?>" 
								title="<?= __( 'Select Sidebar configuration.', 'wp-builder' ); ?>" 
								type="button">
								<span class="dashicons dashicons-admin-page" aria-hidden="true"></span>
							</button>
							<button 
								class="hide-export close-button" 
								data-close="export-sidebars" 
								aria-label="<?= __( 'Close Sidebars export', 'wp-builder' ); ?>" 
								title="<?= __( 'Close Sidebars export', 'wp-builder' ); ?>" 
								type="button">
								<span class="dashicons dashicons-no" aria-hidden="true"></span>
							</button>
						</div>					
						<textarea class="export-field export-sidebars"><?= $sidebar_json; ?></textarea>
					</div>
					<button class="show-export button" data-show="export-sidebars"><?= __( 'Export Sidebar Configuration', 'wp-builder' ); ?></button>
				</aside>
				<div class="wp_builder-accordion">
					<?php foreach ( $sidebars as $i => $sidebar ) : ?>
						<h3 class="accordion-title"><?= $sidebar['title']; ?></h3>
						<div class="accordion-contents">
							<div class="options-preview">
								<div class="left">
									<p><strong><?= __( 'ID', 'wp-builder' ); ?></strong>: <?= $sidebar['machine_name']; ?></p>
									<p><strong><?= __( 'Title', 'wp-builder' ); ?></strong>: <?= $sidebar['title']; ?></p>
									<p><strong><?= __( 'Description', 'wp-builder' ); ?></strong>: <?= $sidebar['description']; ?></p>
								</div>
								<div class="right postbox">
									<h2 class="hndle"><span><?= __( 'Preview', 'wp-builder' ); ?></span></h2>
									<h3><?= $sidebar['title']; ?></h3>
									<div class="inside">
										<?php $this->show_usage_sidebar( $sidebar ); ?>
									</div>
								</div>					
							</div>
							<hr>
							<p class="button--wrapper">
								<a href="<?= wp_nonce_url( $form_url . '&tab=sidebars&action=edit&item=' . $i, 'wp-builder' ); ?>" class="button">
									<?= __( 'Edit Sidebar', 'wp-builder' ); ?>														
								</a>
								<a href="<?= wp_nonce_url( $form_url . '&tab=sidebars&action=delete&item=' . $i, 'wp-builder' ); ?>" class="wp_builder-trash">
									<span class="dashicons dashicons-trash"></span><?= __( 'Delete Sidebar', 'wp-builder' ); ?>
								</a>
							</p>
						</div> <!-- /.accordion-contents -->
					<?php endforeach; ?>
				</div> <!-- /.wp_builder-accordion -->
			<?php else : ?>
				<p><?= __( 'No sidebars defined.', 'wp-builder' ); ?></p>
			<?php endif; ?>
		</section>
		<?php
	}

	/**
	 * Sidebar preview.
	 *
	 * @since 1.0.0
	 * @param array $sidebar The sidebar array.
	 */
	function show_usage_sidebar( $sidebar ) {
		?>
		<div class="field-canvas">
			<h3><?= __( 'How to access ' . $sidebar['title'] . ' in your theme.', 'wp-bullder' ); ?></h3>
			<div><code class="wp-builder-example">&lt;?php dynamic_sidebar( '<?= $sidebar['machine_name']; ?>' ); ?&gt;</code></div>
		</div>
		<?php
	}

}
