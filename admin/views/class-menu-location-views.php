<?php
/**
 * Menu Location Views.
 *
 * @since 2.0.0
 *
 * @package wp-builder
 */

namespace WpBuilder\Views;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\MenuLocationManager;

/**
 * Contains MenuLocationViews class.
 */
class MenuLocationViews {

	/**
	 * WpBuilder\MenuLocationManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MenuLocationManager $menu_location_manager
	 */
	protected $menu_location_manager;

	/**
	 * Constructs a new \WpBuilder\Views\MenuLocationViews object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}
	/**
	 * Load dependencies.
	 *
	 * @since 2.0.0
	 * @access private
	 *
	 */
	private function load_dependencies() {
		$this->menu_location_manager = new \WpBuilder\MenuLocationManager();
	}

	/**
	 * Options preview.
	 *
	 * @since 1.0.0
	 * @param string $form_url        The form URL.
	 * @param array  $menu_locations  An array of menu_locations.
	 */
	public function options_view( $form_url ) {
		$menu_locations = $this->menu_location_manager->get_menu_locations();
		?>
		<section class="overview">
				<header class="overview-header">
					<h3 class="overview-title"><?= __( 'Menu Locations', 'wp-builder' ); ?>
						<a href="<?= wp_nonce_url( $form_url . '&tab=menu_locations&action=add', 'wp-builder' ); ?>" class="page-title-action">
							<?= __( 'Add Menu Location', 'wp-builder' ); ?>
						</a>
					</h3>
				</header>
				<?php
				if ( ! empty( $menu_locations ) ) :
					$menu_locations_json = json_encode( $menu_locations );
					?>
					<aside class="export--wrapper">
						<div class="export-field-container postbox">
							<div class="button--wrapper">
								<button
									class="select-export" 
									data-select="export-menu-locations" 
									aria-label="<?= __( 'Select Menu Locations configuration.', 'wp-builder' ); ?>" 
									title="<?= __( 'Select Menu Locations configuration.', 'wp-builder' ); ?>" 
									type="button">
									<span class="dashicons dashicons-admin-page" aria-hidden="true"></span>
								</button>
								<button 
									class="hide-export close-button" 
									data-close="export-menu-locations" 
									aria-label="<?= __( 'Close Menu Locations export', 'wp-builder' ); ?>" 
									title="<?= __( 'Close Menu Locations export', 'wp-builder' ); ?>" 
									type="button">
									<span class="dashicons dashicons-no" aria-hidden="true"></span>
								</button>
							</div>
							<textarea class="export-field export-menu-locations"><?= $menu_locations_json; ?></textarea>
						</div>
						<button class="show-export button" data-show="export-menu-locations"><?= __( 'Export Menu Locations Configuration', 'wp-builder' ); ?></button>
					</aside>						
					<div class="wp_builder-accordion">
						<?php foreach ( $menu_locations as $i => $menu_location ) : ?>
							<h3 class="accordion-title"><?= $menu_location['name']; ?></h3>
							<div class="accordion-contents">
								<div class="options-preview">		
									<div class="left">
										<p><strong><?= __( 'ID', 'wp-builder' ); ?></strong>: <?= $menu_location['machine_name']; ?></p>
										<p><strong><?= __( 'Name', 'wp-builder' ); ?></strong>: <?= $menu_location['name']; ?></p>
									</div>
									<div class="right postbox">
										<h2 class="hndle"><span><?= __( 'Preview', 'wp-builder' ); ?></span></h2>
										<h3><?= $menu_location['name']; ?></h3>
										<div class="inside">
											<?php $this->show_usage_menu_location( $menu_location ); ?>
										</div>
									</div>
								</div>
								<hr>
								<p class="button--wrapper">
									<a href="<?= wp_nonce_url( $form_url . '&tab=menu_locations&action=edit&item=' . $i, 'wp-builder' ); ?>" class="button">
										<?= __( 'Edit Menu Location', 'wp-builder' ); ?>
									</a>
									<a href="<?= wp_nonce_url( $form_url . '&tab=menu_locations&action=delete&item=' . $i, 'wp-builder' ); ?>" class="wp_builder-trash delete--menu-location">
										<span class="dashicons dashicons-trash"></span><?= __( 'Delete Menu Location', 'wp-builder' ); ?>
									</a>
								</p>
							</div>
						<?php endforeach; ?>
					</div>
				<?php else : ?>
					<p><?= __( 'No menu locations defined.', 'wp-builder' ); ?></p>
				<?php endif; ?>
			</section>
		<?php
	}

	/**
	 * Menu Location preview.
	 *
	 * @since 1.0.0
	 * @param array $menu_location The menu location.
	 */
	public function show_usage_menu_location( $menu_location ) {
		?>
		<div class="field-canvas" >
			<h3><?= __( 'How to access ' . $menu_location['name'] . ' in your theme.', 'wp-builder' ); ?></h3>
			<div><code class="wp-builder-example">&lt;?php wp_nav_menu( ['theme_location' => '<?= $menu_location['machine_name']; ?>'] ); ?&gt;</code></div>
		</div>
		<?php
	}

}
