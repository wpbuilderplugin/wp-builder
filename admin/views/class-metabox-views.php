<?php
/**
 * Metabox Views.
 *
 * @since 2.0.0
 *
 * @package wp-builder
 */

namespace WpBuilder\Views;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\MetaboxManager;

/**
 * Contains MetaboxViews class.
 */
class MetaboxViews {

	/**
	 * WpBuilder\MetaboxManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MetaboxManager $metabox_manager
	 */
	protected $metabox_manager;

	/**
	 * Constructs a new \WpBuilder\Views\MetaboxViews object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}
	/**
	 * Load dependencies.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function load_dependencies() {
		$this->metabox_manager = new MetaboxManager();
	}

	/**
	 * Options preview.
	 *
	 * @since 1.0.0
	 * @param string $form_url The form URL.
	 */
	public function options_view( $form_url ) {
		$metaboxes = $this->metabox_manager->get_metaboxes();
		?>
		<section class="overview">
			<header class="overview-header">
				<h3 class="overview-title"><?= __( 'Metaboxes', 'wp-builder' ); ?> 
					<a href="<?= wp_nonce_url( $form_url . '&tab=metaboxes&action=add', 'wp-builder' ); ?>" class="page-title-action">
						<?= __( 'Add Metabox', 'wp-builder' ); ?>
					</a>
				</h3>
			</header>
			<?php
			if ( ! empty( $metaboxes ) ) :
				$metaboxes_json = json_encode( $metaboxes );
				?>
				<aside class="export--wrapper">
					<div class="export-field-container postbox">
						<div class="button--wrapper">
							<button
								class="select-export" 
								data-select="export-metaboxes" 
								aria-label="<?= __( 'Select Metaboxes configuration.', 'wp-builder' ); ?>" 
								title="<?= __( 'Select Metaboxes configuration.', 'wp-builder' ); ?>" 
								type="button">
								<span class="dashicons dashicons-admin-page" aria-hidden="true"></span>
							</button>
							<button 
								class="hide-export close-button" 
								data-close="export-metaboxes" 
								aria-label="<?= __( 'Close Metaboxes export', 'wp-builder' ); ?>" 
								title="<?= __( 'Close Metaboxes export', 'wp-builder' ); ?>" 
								type="button">
								<span class="dashicons dashicons-no" aria-hidden="true"></span>
							</button>
						</div>
						<textarea class="export-field export-metaboxes"><?= $metaboxes_json; ?></textarea>
					</div>
					<button class="show-export button" data-show="export-metaboxes"><?= __( 'Export Metabox Configuration', 'wp-builder' ); ?></button>
				</aside>					
				<div class="wp_builder-accordion">
					<?php foreach ( $metaboxes as $i => $metabox ) : ?>
						<h3 class="accordion-title"><?= $metabox['title']; ?></h3>
						<div class="accordion-contents">
							<p><strong><?= __( 'ID', 'wp-builder' ); ?>: </strong><?= $metabox['id']; ?></p>
							<p><strong><?= __( 'Title', 'wp-builder' ); ?>: </strong><?= $metabox['title']; ?></p>
							<p><strong><?= __( 'Post Types', 'wp-builder' ); ?>: </strong></p>
							<?php
							if ( isset( $metabox['pages'] ) && ! empty( $metabox['pages'] ) ) :
								$stored_post_types = get_post_types( '', 'objects' );
								print '<ul class="post-types">';
								foreach ( $stored_post_types as $pt ) :
									if ( in_array( $pt->name, $metabox['pages'] ) ) :
										print "<li>{$pt->labels->name}</li>";
									endif;
								endforeach;
								print '</ul>';
							else :
								print "<p>{ __( 'No Post Types selected.', 'wp-builder' ) }</p>";
							endif;
							?>
							<p><strong><?= __( 'Templates', 'wp-builder' ); ?></strong>:</p>
							<?php
							if ( isset( $metabox['tpls'] ) && ! empty( $metabox['tpls'] ) ) :
								$templates = get_page_templates();
								print '<ul class="post-types">';
								foreach ( $templates as $template_name => $template_filename ) :
									if ( in_array( $template_filename, $metabox['tpls'] ) ) :
										print "<li>{$name}</li>";
									endif;
								endforeach;
								print '</ul>';
							else :
								print '<p>' . __( 'No templates selected.', 'wp-builder' ) . '</p>';
							endif;
							?>
							<p><strong><?= __( 'Context', 'wp-builder' ); ?>: </strong> <?= $metabox['context']; ?></p>
							<p><strong><?= __( 'Priority', 'wp-builder' ); ?>: </strong> <?= $metabox['priority']; ?></p>
							<p><strong><?= __( 'Show names', 'wp-builder' ); ?>: </strong> <?= $metabox['show_names']; ?></p>
							<?php if ( array_key_exists( 'fields', $metabox ) ) : ?>
								<p><strong><?= __( 'Fields', 'wp-builder' ); ?>: </strong></p>
								<?php foreach ( $metabox['fields'] as $field ) : ?>
									<div class="options-preview">
										<div class="left field-definition">
											<ul>
												<li>
													<strong><?= __( 'Field Name', 'wp-builder' ); ?>: </strong> 
													<?= ( isset( $field['name'] ) ? $field['name'] : '' ); ?>
												</li>
												<li><strong><?= __( 'Description', 'wp-builder' ); ?>: </strong>
													<?= ( isset( $field['desc'] ) ? $field['desc'] : '' ); ?></li>
												<li><strong><?= __( 'Field ID', 'wp-builder' ); ?>: </strong> 
													<?= ( isset( $field['id'] ) ? $field['id'] : '' ); ?></li>
												<li><strong><?= __( 'Field type', 'wp-builder' ); ?>: </strong>
													<?= ( isset( $field['type'] ) ? $field['type'] : '' ); ?></li>
												<li><strong><?= __( 'Field Options', 'wp-builder' ); ?>: </strong>
													<?php 
													if ( isset( $field['options'] ) ) {
														switch ( $field['type'] ) {
															case 'colorpicker':
																if ( isset( $field['default'] ) ) :
																	?>
																	<p><strong><?= __( 'Default Color', 'wp-builder' ); ?>: </strong><?= $field['default']; ?> -
																		<span style="display: inline-block; width: 50px; height: 30px; background-color: <?= $field['default']; ?>;">
																			&nbsp;
																		</span>
																	</p>
																	<?php
																endif;
																if ( isset( $field['custom_palette'] ) && $field['custom_palette'] == true ) :
																	echo '<p><strong>' . __( 'Custom Palette', 'wp-builder' ) . '</strong>: True</p>';
																	foreach ( $field['options'] as $option => $settings ) {
																		echo '<p><strong>' . __( 'Color', 'wp-builder' ) . ' #' . $option . ': </strong>';
																		echo $settings['value'] . ' - ';
																		echo '<span style="display: inline-block; width: 50px; height: 30px; background-color: ' . $settings['value'] . ';"></span></p>';
																	}
																else :
																	echo '<p><strong>' . __( 'Default Palette', 'wp-builder' ) . ':</strong> True</p>';
																endif;
																break;
															case 'checkbox':
																foreach ( $field['options'] as $option => $settings ) {
																	echo '<p><strong>' . __( 'Option', 'wp-builder' ) . ':</strong><br>';
																	echo __( 'Name: ', 'wp-builder' ) . $settings['name'] . '<br>';
																	echo __( 'Value: ', 'wp-builder' ) . $settings['value'] . '</p>';
																}
																break;
															case 'multicheck':
																foreach ( $field['options'] as $option => $settings ) {
																	echo '<p><strong>' . __( 'Option #', 'wp-builder' ) . $option . ':</strong><br>';
																	echo __( 'Name: ', 'wp-builder' ) . $settings['name'] . '<br>';
																	echo __( 'Value: ', 'wp-builder' ) . $settings['value'] . '</p>';
																}
																break;
															case 'select':
																foreach ( $field['options'] as $option => $settings ) {
																	echo '<p><strong>' . __( 'Option #', 'wp-builder' ) . $option . ':</strong><br>';
																	echo __( 'Name: ', 'wp-builder' ) . $settings['name'] . '<br>';
																	echo __( 'Value: ', 'wp-builder' ) . $settings['value'] . '</p>';
																}
																break;
															case 'radio':
																foreach ( $field['options'] as $option => $settings ) {
																	echo '<p><strong>' . __( 'Option #', 'wp-builder' ) . $option . '</strong><br>';
																	echo __( 'Name: ', 'wp-builder' ) . $settings['name'] . '<br>';
																	echo __( 'Value: ', 'wp-builder' ) . $settings['value'] . '</p>';
																}
																break;
															case 'file':
																echo '<p>';
																echo __( 'Show URL field ', 'wp-builder' );
																echo ( ( isset( $field['options']['url'] ) && $field['options']['url'] == true ) ? 'TRUE' : 'FALSE' );
																echo '</p>';
																break;
															case 'wysiwyg':
																foreach ( $field['options'] as $option => $settings ) {
																	if ( $settings != '' ) {
																		echo '<p><strong>' . $option . '</strong> ';
																		if ( $settings == 'true' || $settings == '1' ) :
																			echo __( 'True', 'wp-builder' );
																		else :
																			echo $settings;
																		endif;
																		echo '</p>';
																	}
																}
																break;
															default:
																if ( array_key_exists( 'options', $field ) ) {
																	print '<pre>';
																	print_r( $field['options'] );
																	print '</pre>';
																}
																break;
														}
													} else {
														echo __( 'Field type does not have options', 'wp-builder' );
													}
													?>
												</li>
											</ul>
										</div>
										<div class="right postbox">
											<h2 class="hndle"><span><?= __( 'Preview', 'wp-builder' ); ?></span></h2>
											<div class="inside">
												<div class="postbox">
													<?php $this->field_preview( $field ); ?>
													<?php $this->show_usage_field( $field ); ?>
												</div>
											</div>
										</div><!-- // preview .postbox -->
									</div>
									<?php endforeach; ?>
								<?php else : ?>
									<?= __( 'No fields have been added. Edit Metabox to add fields.', 'wp-builder' ); ?>
								<?php endif; ?>
							<hr>
							<p class="button--wrapper">
								<a href="<?= wp_nonce_url( $form_url . '&tab=metaboxes&action=edit&item=' . $i, 'wp-builder' ); ?>" class="button">
									<?= __( 'Edit Metabox', 'wp-builder' ); ?>
								</a>
								<a href="<?= wp_nonce_url( $form_url . '&tab=metaboxes&action=delete&item=' . $i, 'wp-builder' ); ?>" class="wp_builder-trash delete--metabox">
									<span class="dashicons dashicons-trash"></span><?= __( 'Delete Metabox', 'wp-builder' ); ?>
								</a>
							</p>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else : ?>
				<p><?= __( 'No metaboxes defined', 'wp-builder' ); ?></p>
			<?php endif; ?>
		</section>
		<?php
	}

	/**
	 * Create previews of fields.
	 *
	 * @since 1.0.0
	 * @param array $field An array representing the field.
	 * $field = [
	 *   'type' => string,
	 *   'id' => string,
	 *   'name' => string,
	 * ]
	 */
	public function field_preview( $field ) {
		echo '<table class="form-table"><tr>';
		switch ( $field['type'] ) {
			case 'text':
				?>
				<th class="row"><label for="<?= $field['id']; ?>"><?= $field['name']; ?></label></th>
				<td><input type="text" name="<?= $field['id']; ?>"></td>
				<?php
				break;
			case 'text_small':
				echo '<th class="row"><label for="' . $field['id'] . '"> ' . $field['name'] . '</label></th>';
				echo '<td><input type="text" name="' . $field['id'] . '" size="16"></td>';
				break;
			case 'text_medium':
				echo '<th class="row"><label for="' . $field['id'] . '"> ' . $field['name'] . '</label></th>';
				echo '<td><input type="text" name="' . $field['id'] . '" size="32"></td>';
				break;
			case 'text_large':
				echo '<th class="row"><label for="' . $field['id'] . '"> ' . $field['name'] . '</label></th>';
				echo '<td><input type="text" name="' . $field['id'] . '" size="64"></td>';
				break;
			case 'text_email':
				echo '<th class="row"><label for="' . $field['id'] . '"> ' . $field['name'] . '</label></th>';
				echo '<td><input type="text" name="' . $field['id'] . '"></td>';
				break;
			case 'text_url':
				echo '<th class="row"><label for="' . $field['id'] . '"> ' . $field['name'] . '</label></th>';
				echo '<td><input type="text" name="' . $field['id'] . '"></td>';
				break;
			case 'text_money':
				?>
				<th class="row cmb-row">
					<label for="<?= $field['id']; ?>"><?= $field['name']; ?></label>
				</th>
				<td>
					<?= ( isset( $field['std'] ) && ! empty( $field['std'] ) ? $field['std'] : '' ); ?>
					<input type="text" name="<?= $field['id']; ?>" class="cmb2-text-money">
				</td>
				<?php
				break;
			case 'textarea':
				echo '<th class="row"><label for="' . $field['id'] . '"> ' . $field['name'] . '</label></th>';
				echo '<td><textarea name="' . $field['id'] . '"></textarea></td>';
				break;
			case 'select':
				echo '<th class="row"><label for="' . $field['id'] . '"> ' . $field['name'] . '</label></th>';
				echo '<td><select name="' . $field['id'] . '">';
				foreach ( $field['options'] as $option ) {
					echo '<option value="' . $option['value'] . ' ">' . $option['name'] . '</option>';
				}
				echo '</select><td>';
				break;
			case 'checkbox':
				echo '<th class="row">' . $field['name'] . '</th>';
				echo '<td>';
				foreach ( $field['options'] as $option_id => $option ) :
					echo '<label for="' . $field['id'] . '[' . $option_id . ']">';
					echo '<input type="checkbox" name="' . $field['id'] . '" value="' . $option['value'] . '" id="' . $field['id'] . '[' . $option_id . ']">' . $option['name'] . '</label><br>';
				endforeach;
				echo '</td>';
				break;
			case 'multicheck':
				echo '<th class="row">' . $field['name'] . '</th>';
				echo '<td>';
				foreach ( $field['options'] as $option_id => $option ) :
					echo '<label for="' . $field['id'] . '[' . $option_id . ']">';
					echo '<input type="checkbox" name="' . $field['id'] . '" value="' . $option['value'] . '" id="' . $field['id'] . '[' . $option_id . ']">' . $option['name'] . '</label><br>';
				endforeach;
				echo '</td>';
				break;
			case 'colorpicker':
				?>
				<th class="row">
					<label for="<?= $field['id']; ?>"><?= $field['name']; ?></label>
				</th>
				<td>
					<input 
						type="text" 
						class="cmb2-colorpicker cmb2-text-small" 
						name="<?= $field['id']; ?>" 
						id="<?= $field['id']; ?>" 
						value="<?= ( isset( $field['default'] ) ? $field['default'] : '' ); ?>"
						<?php
						if ( isset( $field['custom_palette'] ) && $field['custom_palette'] == true ) :
							if ( isset( $field['options'] ) && is_array( $field['options'] ) ) :
								print 'data-colorpicker="{"palettes":[';
								foreach ( $field['options'] as $option => $value ) :
									print '"' . $value['value'] . '",';
								endforeach;
								print ']}"';
							endif;
						endif;
						?>
					>
				</td>
				<?php
				break;
			case 'file':
				?>
				<th class="row"><label for="<?= $field['id']; ?>"><?= $field['name']; ?></label></th>
				<td>
					<input
						type="text"
						class="cmb2-upload-file regular-text"
						name="<?= $field['id']; ?>"
						id="<?= $field['id']; ?>"
						value=""
						size="45"
						data-previewsize="[350,350]">
					<input class="cmb2-upload-button button" type="button" value="<?= __( 'Add or Upload File', 'wp-builder' ); ?>">
				</td>
				<?php
				break;
			case 'radio':
				$i = 1;
				?>
				<th class="row"><?= __( $field['name'], 'wp_builder' ); ?></th>
				<td>
					<?php foreach ( $field['options'] as $option_id => $option ) : ?>
						<label for="<?= $field['id']; ?>[<?= $option_id; ?>]">
							<input 
								type="radio" 
								name="<?= $field['id']; ?>" 
								value="<?= $option['value']; ?>" 
								id="<?= $field['id']; ?>[<?= $option_id; ?>]">
								<?= $option['name']; ?>
						</label>
						<br>
						<?php ++$i; ?>
					<?php endforeach; ?>
				</td>
				<?php
				break;
			case 'radio_inline':
				echo '<th class="row">' . $field['name'] . '</th>';
				$i = 1;
				echo '<td>';
				foreach ( $field['options'] as $option_id => $option ) :
					echo '<label for="' . $field['id'] . $i . '"><input type="radio" name="' . $field['id'] . '" value="' . $option['value'] . '" id="' . $field['id'] . $i . '">' . $option['name'] . '</label><br>';
					++$i;
				endforeach;
				echo '</td>';
				break;
			case 'wysiwyg':
				?>
				<td colspan="2"><label for="<?= $field['id']; ?>"><?= $field['name']; ?></label>
					<div class="wp-core-ui tmce-active">
						<div class="-tools hide-if-no-js">
							<div class="wp-media-buttons">
								<button type="button" class="button insert-media add_media" data-editor="_wp_builder_textarea_small">
									<span class="wp-media-buttons-icon"></span> <?= __( 'Add Media', 'wp-builder' ); ?>
								</button>
							</div>
							<div class="wp-editor-tabs">
								<button type="button" class="wp-switch-editor switch-tmce" data-wp-editor-id="_wp_builder_textarea_small"><?= __( 'Visual', 'wp-builder' ); ?></button>
								<button type="button" id="_wp_builder_textarea_small-html" class="wp-switch-editor switch-html" data-wp-editor-id="_wp_builder_textarea_small"><?= __( 'Text', 'wp-builder' ); ?></button>
							</div>
						</div>
						<div class="wp-editor-container">
							<div class="quicktags-toolbar">
								<input type="button" class="ed_button button button-small" aria-label="Bold" value="b">
								<input type="button" class="ed_button button button-small" aria-label="Italic" value="i">
								<input type="button" class="ed_button button button-small" aria-label="Insert link" value="link">
								<input type="button" class="ed_button button button-small" aria-label="Blockquote" value="b-quote">
								<input type="button" class="ed_button button button-small" aria-label="Deleted text (strikethrough)" value="del">
								<input type="button" class="ed_button button button-small" aria-label="Inserted text" value="ins">
								<input type="button" class="ed_button button button-small" aria-label="Insert image" value="img">
								<input type="button" class="ed_button button button-small" aria-label="Bulleted list" value="ul">
								<input type="button" class="ed_button button button-small" aria-label="Numbered list" value="ol">
								<input type="button" class="ed_button button button-small" aria-label="List item" value="li">
								<input type="button" class="ed_button button button-small" aria-label="Code" value="code">
								<input type="button" class="ed_button button button-small" aria-label="Insert Read More tag" value="more">
								<input type="button" class="ed_button button button-small" title="Close all open tags" value="close tags">
							</div>
							<div id="mceu_95" class="mce-edit-area mce-container mce-panel mce-stack-layout-item" style="border-width: 1px 0px 0px;">
								<iframe frameborder="0" allowtransparency="true" style="width: 100%; height: 220px; display: block;"></iframe>
							</div>
							<div id="mceu_96" class="mce-statusbar mce-container mce-panel mce-stack-layout-item mce-last" hidefocus="1" tabindex="-1" role="group" style="border-width: 1px 0px 0px;">
								<div id="mceu_96-body" class="mce-container-body mce-flow-layout">
									<div id="mceu_97" class="mce-path mce-flow-layout-item mce-first">
										<div role="button" class="mce-path-item mce-last" data-index="0" tabindex="-1" id="mceu_97-0" aria-level="0">p</div>
									</div>
								</div>
							</div>
							<div class="uploader-editor">
								<div class="uploader-editor-content">
									<div class="uploader-editor-title"><?= __( 'Drop files to upload', 'wp-builder' ); ?></div>
								</div>
							</div>
						</div>
						<p class="cmb2-metabox-description"><?= __( 'small textarea', 'wp-builder' ); ?></p>
					</div>
				</td>
				<?php
				break;
			case 'group':
				break;
			default:
				print '<p>' . __( 'No preview available.', 'wp-builder' ) . '</p>';
				break;
		}
		echo '</tr></table>';
	}

	/**
	 * Show field usage.
	 *
	 * @since 1.0.0
	 * @param array $field An array of the field values.
	 */
	public function show_usage_field( $field ) {
		?>
		<div class="field-canvas">
			<h3><?= __( 'How to access ' . $field['name'] . ' in your theme.', 'wp-builder' ); ?></h3>
			<?php if ( array_key_exists( 'repeatable', $field ) && $field['repeatable'] == true ) : ?>
				<div>
					<code class="wp-builder-example">
						&lt;?php <br> $variable = get_post_meta( get_the_ID(), '<?= $field['id']; ?>', true ); <br> 
						foreach ( $variable as $key => $value ) : 
						<br> &nbsp;&nbsp;print_r( $value )<br> 
						endforeach;
						<br> ?&gt;
					</code>
				</div>
			<?php else : ?>
				<div><code class="wp-builder-example">&lt;?php echo $variable = get_post_meta( get_the_ID(), '<?= $field['id']; ?>', true ); ?&gt;</code></div>
				<p><strong><?= __( 'Shortcode', 'wp-builder' ); ?></strong></p>
				<div>
					<code class="wp-builder-example"> [wp_builder field="<?= $field['id']; ?>" post_id="get_the_ID()"]</code>
					<p class="description"><em><?= __( "Post Id required on non-post pages", 'wp-builder' ); ?></em></p>
				</div>
			<?php endif; ?>
			<?php if ( 'file' == $field['type'] ) : ?>
				<p><strong><?= __( 'To return the file ID, append \'_id\' to the field.', 'wp-builder' ); ?>:</strong> <?= $field['id']; ?>_id</p>
			<?php endif; ?>
			<?php if ( 'group' == $field['type'] ) : ?>
				<p><strong><?= __( 'To access child fields, loop over $variable', 'wp-builder' ); ?></strong></p>
				<div><code class="wp-builder-example"> &lt;? foreach ( $variable as $key => $fields ) { <br> print_r( $fields ); <br> } ?&gt;</code></div>
			<?php endif; ?>
		</div>
		<?php
	}
}
