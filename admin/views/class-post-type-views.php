<?php
/**
 * Post Type Views.
 *
 * @since 2.0.0
 *
 * @package wp-builder
 */

namespace WpBuilder\Views;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\PostTypeManager;

/**
 * Contains PostTypeViews class.
 */
class PostTypeViews {

	/**
	 * WpBuilder\PostTypeManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\PostTypeManager $post_type_manager
	 */
	protected $post_type_manager;

	/**
	 * Constructs a new \WpBuilder\Views\PostTypeViews object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}
	/**
	 * Load dependencies.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function load_dependencies() {
		$this->post_type_manager = new PostTypeManager();
	}

	/**
	 * Options preview.
	 *
	 * @since 1.0.0
	 * @param string $form_url The form URL.
	 */
	public function options_view( $form_url ) {
		$post_types = $this->post_type_manager->get_post_types();
		?>
		<section class="overview">
			<header class="overview-header">
				<h3 class="overview-title">
					<?= __( 'Post Types', 'wp-builder' ); ?>
					<a href="<?= wp_nonce_url( $form_url . '&tab=post_types&action=add', 'wp-builder' ); ?>" class="page-title-action">
						<?= __( 'Add Post Type', 'wp-builder' ); ?>
					</a>
				</h3>
			</header>
			<?php
			if ( ! empty( $post_types ) ) :
				$post_types_json = json_encode( $post_types );
				?>
				<aside class="export--wrapper">
					<div class="export-field-container postbox">
						<div class="button--wrapper">
							<button
								class="select-export" 
								data-select="export-sidebars" 
								aria-label="<?= __( 'Select Post Types configuration.', 'wp-builder' ); ?>" 
								title="<?= __( 'Select Post Types configuration.', 'wp-builder' ); ?>" 
								type="button">
								<span class="dashicons dashicons-admin-page" aria-hidden="true"></span>
							</button>
							<button 
								class="hide-export close-button" 
								data-close="export-sidebars" 
								aria-label="<?= __( 'Close Post Types export', 'wp-builder' ); ?>" 
								title="<?= __( 'Close Post Types export', 'wp-builder' ); ?>" 
								type="button">
								<span class="dashicons dashicons-no" aria-hidden="true"></span>
							</button>
						</div>								
						<textarea class="export-field export-post-types"><?= $post_types_json; ?></textarea>
					</div>
					<button 
						class="show-export button" 
						data-show="export-post-types">
						<?= __( 'Export Post Type Configuration', 'wp-builder' ); ?>
					</button>
				</aside>
				<div class="wp_builder-accordion">
					<?php foreach ( $post_types as $i => $post_type ) : ?>
						<h3 class="accordion-title"><?= $post_type['labels']['name']; ?></h3>
						<div class="accordion-contents">
							<div class="options-preview">		
							 	<div>
									<p><strong><?= __( 'ID', 'wp-builder' ); ?></strong>: <?= $post_type['machine_name']; ?></p>
									<p><strong><?= __( 'Description', 'wp-builder' ); ?></strong>: <?= $post_type['description']; ?></p>
									<div class="options-preview">
										<p><strong><?= __( 'Labels', 'wp-builder' ); ?></strong>:</p>
										<div class="right">
											<ul>
												<li><strong><?= __( 'Name', 'wp-builder' ); ?></strong>: <?= $post_type['labels']['name']; ?></li>
												<li><strong><?= __( 'Singular Name', 'wp-builder' ); ?></strong>: <?= $post_type['labels']['singular_name']; ?></li>
											</ul>
										</div>
									</div>
									<div class="options-preview">
										<div class="left">
											<p><strong><?= __( 'Visibility Settings', 'wp-builder' ); ?></strong></p>
										</div>
										<div class="right">
											<?php if ( isset( $post_type['public'] ) && $post_type['public'] == 'true' ) : ?>
												<p><strong><?= __( 'Public', 'wp-builder' ); ?></strong></p>
											<?php else : ?>
												<p><strong><?= __( 'Private', 'wp-builder' ); ?></strong></p>
											<?php endif; ?>
											<?php if ( isset( $post_type['has_archive'] ) && $post_type['has_archive'] == 'true' ) : ?>
												<p><strong><?= __( 'Archive available', 'wp-builder' ); ?></strong></p>
											<?php else : ?>
												<p><strong><?= __( 'No archive available', 'wp-builder' ); ?></strong></p>
											<?php endif; ?>
											<?php if ( isset( $post_type['show_in_rest'] ) && $post_type['show_in_rest'] == 'true' ) : ?>
												<p><strong><?= __( 'Show in REST', 'wp-builder' ); ?></strong></p>
											<?php else : ?>
												<p><strong><?= __( 'Not shown in REST', 'wp-builder' ); ?></strong></p>
											<?php endif; ?>
											<p><strong><?= __( 'Slug', 'wp-builder' ); ?></strong>: <?= $post_type['rewrite']['slug']; ?></p>
										</div>
									</div>
									<?php if ( isset( $post_type['supports'] ) && ! empty( $post_type['supports'] ) ) : ?>
										<div class="options-preview">
											<div class="left">
												<p><strong><?= __( 'Supported features', 'wp-builder' ); ?></strong>:</p>
											</div>
											<div class="right">
												<ul>
													<?php
													$supported_features = $this->post_type_manager->get_supported_features();
													foreach ( $post_type['supports'] as $key ) :
														?>
														<li><?= $supported_features[ $key ]; ?></li>
														<?php
													endforeach;
													?>
												</ul>
											</div>
										</div>
									<?php endif; ?>
									<?php if ( isset( $post_type['taxonomies'] ) && ! empty( $post_type['taxonomies'] ) ) : ?>
										<div class="options-preview">
											<div class="left">
												<p><strong><?= __( 'Taxonomies', 'wp-builder' ); ?></strong></p>
											</div>
											<div class="right">
												<ul>
													<?php
													$taxonomies = get_taxonomies( '', 'objects' );
													foreach ( $taxonomies as $taxonomy ) :
														if ( in_array( $taxonomy->name, $post_type['taxonomies'] ) ) :
															?>
															<li><?= $taxonomy->labels->name; ?></li>
															<?php
														endif;
													endforeach;
													?>
												</ul>
											</div>
										</div>
									<?php endif; ?>
									<?php if ( isset( $post_type['hierarchical'] ) && $post_type['hierarchical'] == true ) : ?>
										<p><strong><?= __( 'Hierarchical', 'wp-builder' ); ?></strong></p>
									<?php endif; ?>
								</div>
							</div>
							<hr>
							<p class="button--wrapper">
								<a href="<?= wp_nonce_url( $form_url . '&tab=post_types&action=edit&item=' . $i, 'wp-builder' ); ?>" class="button">
									<?= __( 'Edit Post Type', 'wp-builder' ); ?>
								</a>
								<a href="<?= wp_nonce_url( $form_url . '&tab=post_types&action=delete&item=' . $i, 'wp-builder' ); ?>" class="wp_builder-trash delete--post-type">
									<span class="dashicons dashicons-trash"></span><?= __( 'Delete Post Type', 'wp-builder' ); ?>
								</a>
							</p>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else : ?>
					<p><?= __( 'No custom post types defined.', 'wp-builder' ); ?></p>
			<?php endif; ?>
		</section>					
		<?php
	}

}
