<?php
/**
 * Customizer Views.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder\Views;

// If called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\CustomizerManager;

/**
 * Contains CustomizerViews class.
 */
class CustomizerViews {

	/**
	 * WpBuilder\CustomizerManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\CustomizerManager $customizer_manager
	 */
	protected $customizer_manager;

	/**
	 * Constructs a new WpBuilder\CustomizerViews object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->load_dependencies();
	}

	/**
	 * Loads dependencies.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function load_dependencies() {
		$this->customizer_manager = new \WpBuilder\CustomizerManager();
	}

	/**
	 * Customier Panel Overview.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form URL.
	 */
	public function panels_view( $form_url ) {
		$stored_panels = $this->customizer_manager->get_stored_panels();
		?>
		<section class="overview">
			<header class="overview-header">
				<h3 class="overview-title"><?= __( 'Manage Customizer Panels', 'wp-builder' ); ?>
					<a href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;action=add&amp;component=panel', 'wp-builder' ); ?>" class="page-title-action" >
						<?= __( 'Add Customizer Panel', 'wp_builder' ); ?>
					</a>
				</h3>
			</header>
			<?php
			$stored_panels = $this->customizer_manager->get_stored_panels();
			if ( !empty( $stored_panels ) ) :
				$customizer_panels_json = json_encode( $this->customizer_manager->get_stored_panels() );
				?>
				<aside class="export--wrapper">
					<div class="export-field-container postbox">
						<div class="button--wrapper">
							<button
								class="select-export"
								data-select="export-customizer-panel"
								aria-label="<?= __( 'Select Customizer Panels configuration', 'wp-builder' ); ?>"
								title="<?= __( 'Select Customizer Panels configuration', 'wp-builder' ); ?>"
								type="button">
								<span class="dashicons dashicons-admin-page" aria-hidden="true"></span>
							</button>
							<button
								class="hide-export close-button"
								data-close="export-customizer-panel"
								aria-label="<?= __( 'Close Customizer Panels export.', 'wp-builder' ); ?>"
								title="<?= __( 'Close Customizer Panels export.', 'wp-builder' ); ?>"
								type="button"
							>
								<span class="dashicons dashicons-no" aria-hidden="true"></span>
								
							</button>
						</div>
						<textarea class="export-field export-customizer-panel"><?= $customizer_panels_json; ?></textarea>
					</div>
					<button class="show-export button" data-show="export-customizer-panel"><?= __( 'Export Customizer Panels Configuration', 'wp-builder' ); ?></button>
				</aside>
			<?php endif; ?>
			<div class="wp_builder-accordion">
				<?php if ( ! empty( $stored_panels ) ) : ?>
					<?php foreach ( $stored_panels as $panel_id => $panel ) : ?>
						<h4><?= $panel['title']; ?></h4>
						<div class="accordion-contents">
							<p><strong><?= __( 'Panel ID', 'wp-builder' ); ?>:</strong> <?= $panel['machine_name']; ?></p>
							<p><strong><?= __( 'Panel Title', 'wp-builder' ); ?>:</strong> <?= $panel['title']; ?></p>
							<?php if ( array_key_exists( 'description', $panel ) ) : ?>
								<p><strong><?= __( 'Panel Description', 'wp-builder' ); ?>:</strong> <?= $panel['description']; ?></p>
							<?php endif; ?>
							<?php if ( array_key_exists( 'priority', $panel ) ) : ?>
								<p><strong><?= __( 'Panel Priority', 'wp-builder' ); ?>:</strong> <?= $panel['priority']; ?></p>
							<?php endif; ?>
							<p class="button--wrapper">
								<a 
									href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;component=panel&amp;action=edit&amp;item=' . $panel_id, 'wp-builder' ); ?>" 
									class="button">
									<?= __( 'Edit Panel', 'wp-builder' ); ?></a>
								<a 
									href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;component=panel&amp;action=delete&amp;item=' . $panel_id, 'wp-builder' ); ?>" 
									class="wp_builder-trash">
									<span class="dashicons dashicons-trash"></span>
									<?= __( 'Delete Panel', 'wp-builder' ); ?></a>
							</p>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</section>
		<?php
	}

	/**
	 * Customizer Sections Overview.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form URL.
	 */
	public function sections_view( $form_url ) {
		$stored_sections = $this->customizer_manager->get_stored_sections();
		?>
		<section class="overview">
			<header class="overview-header">
				<h3 class="overview-title"><?= __( 'Manage Customizer Sections', 'wp-builder' ); ?>
					<a href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;action=add&amp;component=section', 'wp-builder' ); ?>" class="page-title-action"><?= __( 'Add Customizer Section', 'wp-builder' ); ?></a>
				</h3>
			</header>
			<?php
			$stored_sections = $this->customizer_manager->get_stored_sections();
			if ( ! empty( $stored_sections ) ) :
				$customizer_sections_json = json_encode( $this->customizer_manager->get_stored_sections() );
				?>
				<aside class="export--wrapper">
					<div class="export-field-container postbox">
						<div class="button--wrapper">
							<button
								class="select-export"
								data-select="export-customizer-section"
								aria-label="<?= __( 'Select Customizer Sections configuration', 'wp-builder' ); ?>"
								title="<?= __( 'Select Customizer Sections configuration', 'wp-builder' ); ?>"
								type="button">
								<span class="dashicons dashicons-admin-page" aria-hidden="true"></span>
							</button>
							<button
								class="hide-export close-button"
								data-close="export-customizer-section"
								aria-label="<?= __( 'Close Customizer Sections export.', 'wp-builder' ); ?>"
								title="<?= __( 'Close Customizer Sections export.', 'wp-builder' ); ?>"
								type="button"
							>
								<span class="dashicons dashicons-no" aria-hidden="true"></span>
								
							</button>
						</div>
						<textarea class="export-field export-customizer-section"><?= $customizer_sections_json; ?></textarea>
					</div>
					<button class="show-export button" data-show="export-customizer-section"><?= __( 'Export Customizer Sections Configuration', 'wp-builder' ); ?></button>
				</aside>
			<?php endif; ?>
			<div class="wp_builder-accordion">
				<?php if ( ! empty( $stored_sections ) ) : ?>
					<?php foreach ( $stored_sections as $id => $section ) : ?>
						<h4><?= $section['title']; ?></h4>
						<div class="accordion-contents">
							<p><strong><?= __( 'Section ID', 'wp-builder' ); ?>:</strong> <?= $section['machine_name']; ?></p>
							<p><strong><?= __( 'Section Title', 'wp-builder' ); ?>:</strong> <?= $section['title']; ?></p>
							<?php if ( array_key_exists( 'description', $section ) ) : ?>
								<p><strong><?= __( 'Section Description', 'wp-builder' ); ?>: </strong> <?= $section['description']; ?></p>
							<?php endif; ?>
							<?php if ( array_key_exists( 'priority', $section ) ) : ?>
								<p><strong><?= __( 'Section Priority', 'wp-builder' ); ?>: </strong> <?= $section['priority']; ?></p>
							<?php endif; ?>
							<?php if ( array_key_exists( 'panel', $section ) ) : ?>
								<?php $panel = $this->customizer_manager->get_panel_by_id( $section['panel'] ); ?>
								<p>
									<strong><?= __( 'Panel', 'wp-builder' ); ?>: </strong> <?= $section['panel'] != '_none' && array_key_exists( 'title', $panel ) ? $panel['title'] : 'None'; ?> 
								</p>
							<?php endif; ?>
							<p class="button--wrapper">
								<a 
									href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;component=section&amp;action=edit&amp;item=' . $id, 'wp-builder' ); ?>" 
									class="button"><?= __( 'Edit Panel', 'wp_builder' ); ?></a>
								<a 
									href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;component=section&amp;action=delete&amp;item=' . $id, 'wp-builder' ); ?>" 
									class="wp_builder-trash"><span class="dashicons dashicons-trash"></span><?= __( 'Delete Section', 'wp_builder' ); ?></a>
							</p>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</section>
		<?php
	}

	/**
	 * Customizer Settings Overview.
	 *
	 * @since 2.0.0
	 * @param string $form_url The form URL.
	 */
	public function settings_view( $form_url ) {
		$stored_settings = $this->customizer_manager->get_stored_settings();
		$sections = $this->customizer_manager->get_all_sections();
		$setting_types = $this->customizer_manager->get_setting_types();
		$field_types = $this->customizer_manager->field_types();
		?>
		<section class="overview">
			<header class="overview-header">
				<h3 class="overview-title"><?= __( 'Manage Customizer Settings', 'wp-builder' ); ?>
					<a 
						href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;action=add&amp;component=setting', 'wp-builder' ); ?>" 
						class="page-title-action">
						<?= __( 'Add Customizer Setting', 'wp-builder' ); ?>	
					</a>
				</h3>
			</header>
			<?php
			if ( ! empty( $stored_settings ) ) :
				$customizer_settings_json = json_encode( $stored_settings );
				?>
				<aside class="export--wrapper">
					<div class="export-field-container postbox">
						<div class="button--wrapper">
							<button
								class="select-export"
								data-select="export-customizer-setting"
								aria-label="<?= __( 'Select Customizer Settings configuration', 'wp-builder' ); ?>"
								title="<?= __( 'Select Customizer Settings configuration', 'wp-builder' ); ?>"
								type="button">
								<span class="dashicons dashicons-admin-page" aria-hidden="true"></span>
							</button>
							<button
								class="hide-export close-button"
								data-close="export-customizer-setting"
								aria-label="<?= __( 'Close Customizer Settings export.', 'wp-builder' ); ?>"
								title="<?= __( 'Close Customizer Settings export.', 'wp-builder' ); ?>"
								type="button"
							>
								<span class="dashicons dashicons-no" aria-hidden="true"></span>
							</button>
						</div>
						<textarea class="export-field export-customizer-setting"><?= $customizer_settings_json; ?></textarea>
					</div>
					<button class="show-export button" data-show="export-customizer-setting"><?= __( 'Export Customizer Settings Configuration', 'wp-builder' ); ?></button>
				</aside>
			<?php endif; ?>	
			<div class="wp_builder-accordion">
				<?php if ( ! empty( $stored_settings ) ) : ?>
					<?php foreach ( $stored_settings as $id => $setting ) : ?>
						<h4><?= $setting['title']; ?></h4>
						<div class="accordion-contents">
							<div class="options-preview">
								<div class="left">
									<p><strong><?= __( 'Setting ID', 'wp-builder' ); ?>: </strong> <?= $setting['machine_name']; ?></p>
									<p><strong><?= __( 'Setting Title', 'wp-builder' ); ?>: </strong> <?= $setting['title']; ?></p>
									<?php if ( array_key_exists( 'type', $setting ) ) : ?>
										<p><strong><?= __( 'Setting type', 'wp-builder' ); ?>: </strong> <?= $setting_types[ $setting['type'] ]; ?></p>
									<?php endif; ?>
									<?php if ( array_key_exists( 'label', $setting['control'] ) ) : ?>
										<p><strong><?= __( 'Control Label', 'wp-builder' ); ?>: </strong> <?= $setting['control']['label']; ?></p>
									<?php endif; ?>
									<?php if ( array_key_exists( 'description', $setting['control'] ) ) : ?>
										<p><strong><?= __( 'Control Description', 'wp-builder' ); ?>: </strong> <?= $setting['control']['description']; ?></p>
									<?php endif; ?>
									<?php if ( array_key_exists( 'section', $setting['control'] ) ) : ?>
										<p><strong><?= __( 'Section', 'wp-builder' ); ?>: </strong> 
											<?php if ( $setting['control']['section'] != '_none_' ) : ?>
												<?= $sections[ $setting['control']['section'] ]['title']; ?>
											<?php endif; ?>
										</p>
									<?php endif; ?>
									<?php if ( array_key_exists( 'type', $setting['control'] ) ) : ?>
										<p><strong><?= __( 'Control Type', 'wp-builder' ); ?>: </strong> <?= $field_types[ $setting['control']['type'] ]['label']; ?></p>
										<?php
										if ( array_key_exists( 'settings', $field_types[ $setting['control']['type'] ] ) ) :
											foreach ( $field_types[ $setting['control']['type'] ]['settings'] as $fs_type => $fs_name ) :
												if ( array_key_exists( $fs_type, $setting['control'] ) ) :
													if ( 'choices' == $fs_type ) :
														?>
														<p><strong><?= $fs_name; ?>:</strong></p>
														<?php if ( is_array( $setting['control'][ $fs_type ] ) ) : ?>
															<ul class="choices-list">
																<?php foreach ( $setting['control'][ $fs_type ] as $control_setting ) : ?>
																	<li class="choice">
																		<p>
																			<strong><?= __( 'Label', 'wp-builder' ); ?>: </strong><?= $control_setting['label']; ?>
																		</p>
																		<p>
																			<strong><?= __( 'Value', 'wp-builder' ); ?>: </strong><?= $control_setting['value']; ?>
																		</p>
																	</li>
																<?php endforeach; ?>
															</ul>
														<?php
														endif;
												endif;
												if ( $fs_type == 'input_attrs' ) :
													?>
													<p><strong><?= __( 'Input Attributes', 'wp-builder' ); ?>:</strong></p>
														<?php if ( is_array( $setting['control'][ $fs_type ] ) ) : ?>
														<ul class="input-attrs-list">
															<?php foreach ( $setting['control'][ $fs_type ] as $input_attrs => $input_attrs_value ) : ?>
																<?php if ( isset( $fs_name['default_attributes'][ $input_attrs ] ) ) : ?>
																	<li>
																		<p><strong><?= $fs_name['default_attributes'][ $input_attrs ]['label']; ?>: </strong>
																			<?= $input_attrs_value; ?> 
																		</p>
																	</li>
																<?php else : ?>
																	<?php $attr = $this->customizer_manager->input_attribute_types(); ?>
																	<?php if ( array_key_exists( $input_attrs, $attr ) ) : ?>
																		<li>
																			<p>
																				<strong><?= $attr[ $input_attrs ]['label']; ?>: </strong> <?= $input_attrs_value; ?>
																			</p>
																		</li>
																	<?php endif; ?>
																<?php endif; ?>
															<?php endforeach; ?>
														</ul>
															<?php
														endif;
													endif;
												endif;
											endforeach;
										endif;
									endif;
									?>
							</div>
							<div class="right postbox">
								<h2 class="hndle"><span><?= __( 'Preview', 'wp-builder' ); ?></span></h2>
								<h3><?= $setting['title']; ?></h3>
								<div class="inside">
									<?php $this->show_setting_usage( $setting ); ?>
								</div>
							</div>
						</div>
						<p class="button--wrapper">
							<a 
								href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;component=setting&amp;action=edit&amp;item=' . $id, 'wp-builder' ); ?>" 
								class="button">
								<?= __( 'Edit Panel', 'wp_builder' ); ?>
							</a>
							<a 
								href="<?= wp_nonce_url( $form_url . '&amp;tab=customizer&amp;component=setting&amp;action=delete&amp;item=' . $id, 'wp-builder' ); ?>" 
								class="wp_builder-trash">
								<span class="dashicons dashicons-trash"></span>
								<?= __( 'Delete Section', 'wp_builder' ); ?>
							</a>
						</p>
					</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</section>
		<?php
	}

	/**
	 * Show Setting usage.
	 *
	 * @since 2.0.0
	 * @param array $setting The setting.
	 */
	public function show_setting_usage( $setting ) {
		?>
		<div class="field-canvas">
			<h3><?= __( 'How to access ' . $setting['title'] . ' in your theme.', "wp-builder" ); ?></h3>
			<?php
			if ( $setting['type'] == 'option' ) :
				?>
				<div>
					<code class="wp-builder-example">
						&lt;?php echo $variable = get_option( '<?= $setting['machine_name']; ?>' ); ?&gt;
					</code>
				</div>
				<?php
			else :
				?>
				<div>
					<code class="wp-builder-example">
						&lt;?php echo $variable = get_theme_mod( '<?= $setting['machine_name']; ?>' ); ?&gt;
					</code>
				</div>
				<?php
			endif;
			?>
		</div>
		<?php
	}

}
