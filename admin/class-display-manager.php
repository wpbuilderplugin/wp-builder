<?php
/**
 * WP Builder display manager.
 *
 * @since 2.0.0
 * @package wp-builder.
 */

namespace WpBuilder;

// If this file is called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\CustomizerManager;
use WpBuilder\MetaboxManager;
use WpBuilder\SidebarManager;
use WpBuilder\MenuLocationManager;
use WpBuilder\TaxonomyManager;
use WpBuilder\PostTypeManager;
use WpBuilder\Forms\CustomizerForms;
use WpBuilder\Forms\ImporterForms;
use WpBuilder\Forms\MenuLocationForms;
use WpBuilder\Forms\MetaboxForms;
use WpBuilder\Forms\PostTypeForms;
use WpBuilder\Forms\SidebarForms;
use WpBuilder\Forms\TaxonomyForms;
use WpBuilder\Views\CustomizerViews;
use WpBuilder\Views\MenuLocationViews;
use WpBuilder\Views\MetaboxViews;
use WpBuilder\Views\PostTypeViews;
use WpBuilder\Views\SidebarViews;
use WpBuilder\Views\TaxonomyViews;

/**
 * Contains DisplayManager class.
 */
class DisplayManager {

	/**
	 * Slug.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $slug
	 */
	protected $slug;

	/**
	 * WpBuilder\CustomizerManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\CustomizerManager $customizer_manager
	 */
	protected $customizer_manager;

	/**
	 * WpBuilder\MetaboxManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MetaboxManager $metabox_manager
	 */
	protected $metabox_manager;

	/**
	 * WpBuilder\PostTypeManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\PostTypeManager $post_type_manager
	 */
	protected $post_type_manager;

	/**
	 * WpBuilder\TaxonomyManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\TaxonomyManager $taxonomy_manager
	 */
	protected $taxonomy_manager;

	/**
	 * WpBuilder\MenuLocationManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MenuLocationManager $menu_location_manager
	 */
	protected $menu_location_manager;

	/**
	 * WpBuilder\SidebarManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\SidebarManager $sidebar_manager
	 */
	protected $sidebar_manager;

	/**
	 * WpBuilder\Views\CustomizerViews definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Views\CustomizerViews $customizer_views
	 */
	protected $customizer_views;

	/**
	 * WpBuilder\Views\MenuLocationViews definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Views\MenuLocationViews $menu_location_views
	 */
	protected $menu_location_views;

	/**
	 * WpBuilder\Views\MetaboxViews definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Views\MetaboxViews $metabox_views
	 */
	protected $metabox_views;

	/**
	 * WpBuilder\Views\PostTypeViews definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Views\PostTypeViews $post_type_views
	 */
	protected $post_type_views;

	/**
	 * WpBuilder\Views\TaxonomyViews definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Views\TaxonomyViews $taxonomy_views
	 */
	protected $taxonomy_views;

	/**
	 * WpBuilder\Views\SidebarViews definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\Views\SidebarViews $sidebar_views
	 */
	protected $sidebar_views;

	/**
	 * Constructs a new PageManager object.
	 *
	 * @since 2.0.0
	 */
	public function __construct() {
		$this->slug = 'wp-builder';
		$this->load_dependencies();
	}

	/**
	 * Load dependencies.
	 *
	 * @since 2.0.0
	 * @access private
	 */
	private function load_dependencies() {
		$this->customizer_manager = new CustomizerManager();
		$this->metabox_manager = new MetaboxManager();
		$this->menu_location_manager = new MenuLocationManager();
		$this->taxonomy_manager = new TaxonomyManager();
		$this->sidebar_manager = new SidebarManager();
		$this->post_type_manager = new PostTypeManager();
		$this->customizer_views = new CustomizerViews();
		$this->menu_location_views = new MenuLocationViews();
		$this->metabox_views = new MetaboxViews();
		$this->post_type_views = new PostTypeViews();
		$this->sidebar_views = new SidebarViews();
		$this->taxonomy_views = new TaxonomyViews();
	}

	/**
	 * Get tabs
	 *
	 * @since 2.0.0
	 * @param string $current_tab The current tab.
	 */
	public function render_tabs( $current_tab ) {
		$tabs = [
			'homepage' => __( 'Dashboard', 'wp-builder' ),
			'sidebars' => __( 'Sidebars', 'wp-builder' ),
			'post_types' => __( 'Post Types', 'wp-builder' ),
			'taxonomies' => __( 'Taxonomies', 'wp-builder' ),
			'metaboxes' => __( 'Metaboxes', 'wp-builder' ),
			'menu_locations' => __( 'Menu Locations', 'wp-builder' ),
			'customizer' => __( 'Customizer', 'wp-builder' ),
			'import' => __( 'Import', 'wp-builder' ),
		];
		echo '<div id="icon-themes" class="icon32"><img src="' . WP_BUILDER_ASSETS_PATH . 'images/icon.png"></div>';
		echo '<h2 class="nav-tab-wrapper">';
		foreach ( $tabs as $tab => $label ) {
			$class = ( $tab == $current_tab ) ? ' nav-tab-active' : '';
			echo '<a class="nav-tab ' . $class . '" href="?page=' . $this->slug . '&tab=' . $tab . '">' . $label . '</a>';
		}
		echo '</h2>';
	}

	/**
	 * Render page.
	 *
	 * @since 2.0.0
	 * @param string $tab    The current tab.
	 * @param string $action The current action.
	 */
	public function render_page( $tab = 'homepage', $action = 'view' ) {
		$form_url = $this->get_form_url( $tab, $action );
		ob_start();
		?>
		<div class="wrap">
			<h1>WP Builder</h1>
			<?php $this->render_tabs( $tab ); ?>
			<?php
			switch ( $tab ) {
				case 'import':
					$importer_forms = new ImporterForms();
					$importer_forms->import_form( $form_url );
					break;
				case 'homepage':
					require_once( WP_BUILDER_ADMIN_PATH . 'pages/homepage.php' );
					break;
				case 'sidebars':
					print '<h2>' . __( 'Manage Sidebars', 'wp-builder' );
					if ( 'view' == $action ) {
						print '<a href="' . wp_nonce_url( $form_url . '&action=add', 'wp-builder' ) . '" class="page-title-action">' . __( 'Add Sidebar', 'wp-builder' ) . '</a>';
					}
					print '</h2>';
					$sidebar_forms = new SidebarForms();
					$sidebars = $this->sidebar_manager->get_sidebars();
					switch ( $action ) {
						case 'delete':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( ! empty( $sidebars ) && isset( $_GET['item'] ) && isset( $sidebars[ $_GET['item'] ] ) ) {
									$sidebar_forms->delete_form( $form_url, $sidebars[ $_GET['item'] ] );
								} else {
									print '<p>' . __( 'Sidebar not found, please try again.', 'wp-builder' ) . '</p>';
								}
							}
							break;
						case 'add':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								$count = 0;
								if ( ! empty( $sidebars ) ) {
									$count = count( $sidebars );
								}
								$sidebar_forms->sidebar_form( $form_url, $count );
							}
							break;
						case 'edit':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( ! empty( $sidebars ) && isset( $_GET['item'] ) && isset( $sidebars[ $_GET['item'] ] ) ) {
									$sidebar_forms->sidebar_form( $form_url, $_GET['item'], $sidebars[ $_GET['item'] ] );
								} else {
									$count = 0;
									if ( ! empty( $sidebars ) ) {
										$count = count( $sidebars );
									}
									$sidebar_forms->sidebar_form( $form_url, $count );
								}
							}
							break;
						default:
							if ( ! empty( $sidebars ) ) {
								$this->sidebar_views->options_view( $form_url, $sidebars );
							} else {
								$sidebar_forms->sidebar_form( $form_url );
							}
							break;
					}
					break;
				case 'menu_locations':
					print '<h2>' . __( 'Manage Menu Locations', 'wp-builder' );
					if ( 'view' == $action ) {
						print '<a href="' . wp_nonce_url( $form_url . '&action=add', 'wp-builder' ) . '" class="page-title-action" >' . __( 'Add Menu Location', 'wp-builder' ) . '</a>';
					}
					print '</h2>';
					$menu_locations = $this->menu_location_manager->get_menu_locations();
					$menu_location_forms = new MenuLocationForms();
					switch ( $action ) {
						case 'delete':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( ! empty( $menu_locations ) && isset( $_GET['item'] ) && isset( $menu_locations[ $_GET['item'] ] ) ) {
									$menu_location_forms->menu_location_delete_form( $form_url, $menu_locations[ $_GET['item'] ] );
								} else {
									?>
									<div class="notice wp-builder-notice notice-warning is-dismissible">
										<p><?= __( 'Menu location not found, please try again.', 'wp-builder' ); ?></p>
									</div>
									<?php
								}
							}
							break;
						case 'add':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-warning">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								$count = 0;
								if ( ! empty( $menu_locations ) ) {
									$count = count( $menu_locations );
								}
								$menu_location_forms->menu_location_form( $form_url, $count );
							}
							break;
						case 'edit':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( isset( $_GET['item'] ) && isset( $menu_locations[ $_GET['item'] ] ) ) {
									$menu_location_forms->menu_location_form( $form_url, $_GET['item'], $menu_locations[ $_GET['item'] ] );
								} else {
									$count = 0;
									if ( ! empty( $menu_locations ) ) {
										$count = count( $menu_locations );
									}
									$menu_location_forms->menu_location_form( $form_url, $count );
								}
							}
							break;
						default:
							if ( ! empty( $menu_locations ) ) {
								$this->menu_location_views->options_view( $form_url, $menu_locations );
							} else {
								$menu_location_forms->menu_location_form( $form_url );
							}
							break;
					}
					break;
				case 'post_types':
					print '<h2>' . __( 'Manage Post Types', 'wp-builder' );
					if ( 'view' == $action ) {
						echo '<a href="' . wp_nonce_url( $form_url . '&action=add', 'wp-builder' ) . '" class="page-title-action" >' . __( 'Add Post Type', 'wp-builder' ) . '</a>';
					}
					print '</h2>';
					$post_type_forms = new PostTypeForms();
					$post_types = $this->post_type_manager->get_post_types();
					switch ( $action ) {
						case 'delete':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( ! empty( $post_types ) && isset( $_GET['item'] ) && isset( $post_types[ $_GET['item'] ] ) ) {
									$post_type_forms->delete_form( $form_url, $post_types[ $_GET['item'] ] );
								} else {
									?>
									<div class="notice wp-builder-notice notice-warning is-dismissible">
										<p><?= __( 'Post type not found, please try again.', 'wp-builder' ); ?></p>
									</div>
									<?php
								}
							}
							break;
						case 'add':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								$count = 0;
								if ( ! empty( $post_types ) ) {
									$count = count( $post_types );
								}
								$post_type_forms->post_type_form( $form_url, $count );
							}
							break;
						case 'edit':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( isset( $_GET['item'] ) && isset( $post_types[ $_GET['item'] ] ) ) {
									$post_type_forms->post_type_form( $form_url, $_GET['item'], $post_types[ $_GET['item'] ] );
								} else {
									$count = 0;
									if ( ! empty( $post_types ) ) {
										$count = count( $post_types );
									}
									$post_type_forms->post_type_form( $form_url, $count );
								}
							}
							break;
						default:
							if ( ! empty( $post_types ) ) {
								$this->post_type_views->options_view( $form_url, $post_types );
							} else {
								$post_type_forms->post_type_form( $form_url );
							}
							break;
					}
					break;
				case 'taxonomies':
					echo '<h2>' . __( 'Manage Taxonomies', 'wp-builder' );
					if ( 'view' == $action ) {
						echo '<a href="' . wp_nonce_url( $form_url . '&action=add', 'wp-builder' ) . '" class="page-title-action" >' . __( 'Add Taxonomy', 'wp-builder' ) . '</a>';
					}
					echo '</h2>';
					$taxonomy_forms = new TaxonomyForms();
					$taxonomies = $this->taxonomy_manager->get_taxonomies();
					switch ( $action ) {
						case 'delete':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( ! empty( $taxonomies ) && isset( $_GET['item'] ) && isset( $taxonomies[ $_GET['item'] ] ) ) {
									$taxonomy_forms->taxonomy_delete_form( $form_url, $taxonomies[ $_GET['item'] ] );
								} else {
									?>
									<div class="notice wp-builder-notice notice-error is-dismissible">
										<p><?= __( 'Taxonomy not found, please try again.', 'wp-builder' ); ?></p>
									</div>
									<?php
								}
							}
							break;
						case 'add':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								$count = 0;
								if ( ! empty( $taxonomies ) ) {
									$count = count( $taxonomies );
								}
								$taxonomy_forms->taxonomy_form( $form_url, $count );
							}
							break;
						case 'edit':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( ! empty( $taxonomies ) && isset( $_GET['item'] ) && isset( $taxonomies[ $_GET['item'] ] ) ) {
									$taxonomy_forms->taxonomy_form( $form_url, $_GET['item'], $taxonomies[ $_GET['item'] ] );
								} else {
									$count = 0;
									if ( ! empty( $taxonomies ) ) {
										$count = count( $taxonomies );
									}
									$taxonomy_forms->taxonomy_form( $form_url, $count );
								}
							}
							break;
						default:
							if ( ! empty( $taxonomies ) ) {
								$this->taxonomy_views->options_view( $form_url, $taxonomies );
							} else {
								$taxonomy_forms->taxonomy_form( $form_url );
							}
							break;
					}
					break;
				case 'metaboxes':
					echo '<h2>' . __( 'Manage Metaboxes', 'wp-builder' );
					if ( 'add' != $action && 'delete' != $action && 'edit' != $action ) {
						echo '<a href="' . wp_nonce_url( $form_url . '&action=add', 'wp-builder' ) . '" class="page-title-action" >' . __( 'Add Metabox', 'wp-builder' ) . '</a>';
					}
					echo '</h2>';
					$metabox_forms = new MetaboxForms();
					$metaboxes = $this->metabox_manager->get_metaboxes();
					switch ( $action ) {
						case 'delete':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( ! empty( $metaboxes ) && isset( $_GET['item'] ) && isset( $metaboxes[ $_GET['item'] ] ) ) {
									$metabox_forms->metabox_delete_form( $form_url, $metaboxes[ $_GET['item'] ] );
								} else {
									?>
									<div class="notice wp-builder-notice notice-error is-dismissible">
										<p><?= __( 'Metabox not found, please try again.', 'wp-builder' ); ?></p>
									</div>
									<?php
								}
							}
							break;
						case 'add':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								$count = 0;
								if ( ! empty( $metaboxes ) ) {
									$count = count( $metaboxes );
								}
								$metabox_forms->metabox_form( $form_url, $count );
							}
							break;
						case 'edit':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								if ( isset( $_GET['item'] ) && isset( $metaboxes[ $_GET['item'] ] ) ) {
									$metabox_forms->metabox_form( $form_url, $_GET['item'], $metaboxes[ $_GET['item'] ] );
								} else {
									$count = 0;
									if ( ! empty( $metaboxes ) ) {
										$count = count( $metaboxes );
									}
									$metabox_forms->metabox_form( $form_url, $count );
								}
							}
							break;
						default:
							if ( ! empty( $metaboxes ) ) {
								$this->metabox_views->options_view( $form_url, $metaboxes );
							} else {
								$metabox_forms->metabox_form( $form_url );
							}
							break;
					}
					break;
				case 'customizer':
					echo '<h2>' . __( 'Manage Customizer', 'wp-builder' ) . '</h2>';
					$customizer_forms = new CustomizerForms();
					$component = '';
					if ( isset( $_GET['component'] ) ) {
						$component = $_GET['component'];
					}
					switch ( $action ) {
						case 'delete':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} elseif ( empty( $component ) || ! isset( $_GET['item'] ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'No component selected, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								switch ( $component ) {
									case 'panel':
										$panel = $this->customizer_manager->get_panel_by_id( $_GET['item'] );
										if ( empty( $panel ) ) {
											?>
											<div class="notice wp-builder-notice notice-error is-dismissible">
												<p><?= __( 'No panel selected, please try again.', 'wp-builder' ); ?></p>
											</div>
											<?php
										} else {
											$customizer_forms->panel_delete_form( $form_url, $panel );
										}
										break;
									case 'section':
										$section = $this->customizer_manager->get_section_by_id( $_GET['item'] );
										if ( empty( $section ) ) {
											?>
											<div class="notice wp-builder-notice notice-error is-dismissible">
												<p><?= __( 'No section selected, please try again.', 'wp-builder' ); ?></p>
											</div>
											<?php
										} else {
											$customizer_forms->section_delete_form( $form_url, $section );
										}
										break;
									case 'setting':
										$setting = $this->customizer_manager->get_setting_by_id( $_GET['item'] );
										if ( empty( $setting ) ) {
											?>
											<div class="notice wp-builder-notice notice-error is-dismissible">
												<p><?= __( 'No setting selected, please try again.', 'wp-builder' ); ?></p>
											</div>
											<?php
										} else {
											$customizer_forms->setting_delete_form( $form_url, $setting );
										}
										break;
								}
							}
							break;
						case 'add':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} elseif ( empty( $component ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'No component selected, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								switch( $component ) {
									case 'panel':
										$next_id = $this->customizer_manager->next_id( 'panel' );
										$customizer_forms->panel_form( $form_url, $next_id );
										break;
									case 'section':
										$next_id = $this->customizer_manager->next_id( 'section' );
										$customizer_forms->section_form( $form_url, $next_id );
										break;
									case 'setting':
										$next_id = $this->customizer_manager->next_id( 'setting' );
										$customizer_forms->setting_form( $form_url, $next_id );
										break;
								}
							}
							break;
						case 'edit':
							if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-builder' ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'Something went wrong, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} elseif ( empty( $component ) || ! isset( $_GET['item'] ) ) {
								?>
								<div class="notice wp-builder-notice notice-error is-dismissible">
									<p><?= __( 'No component selected, please try again.', 'wp-builder' ); ?></p>
								</div>
								<?php
							} else {
								$item = $_GET['item'];
								switch ( $component ) {
									case 'panel':
										$panels = $this->customizer_manager->get_stored_panels();
										if ( ! isset( $panels[ $item ] ) ) {
											$next_id = $this->customizer_manager->next_id( 'panel' );
											$customizer_forms->panel_form( $form_url, $next_id );
										} else {
											$customizer_forms->panel_form( $form_url, $item, $panels[ $item ] );
										}
										break;
									case 'section':
										$sections = $this->customizer_manager->get_stored_sections();
										if ( ! isset( $sections[ $item ] ) ) {
											$next_id = $this->customizer_manager->next_id( 'section' );
											$customizer_forms->section_form( $form_url, $item );
										} else {
											$customizer_forms->section_form( $form_url, $item, $sections[ $item ] );
										}
										break;
									case 'setting':
										$settings = $this->customizer_manager->get_stored_settings();
										if ( ! isset( $settings[ $item ] ) ) {
											$next_id = $this->customizer_manager->next_id( 'setting' );
											$customizer_forms->setting_form( $form_url, $next_id );
										} else {
											$customizer_forms->setting_form( $form_url, $item, $settings[ $item ] );
										}
										break;
								}
							}
							break;
						default:
							$this->customizer_views->panels_view( $form_url );
							$this->customizer_views->sections_view( $form_url );
							$this->customizer_views->settings_view( $form_url );
							break;
					}
					break;
			}
			?>
		</div>
		<?php
		$content = ob_get_clean();
		print $content;
	}

	/**
	 * Ajax response, get field options.
	 *
	 * @since 2.0.0
	 */
	public function ajax_get_field_options() {
		global $wpdb;
		$data = json_decode( stripcslashes( $_POST['data'] ) );
		$type = $data->{'type'};
		$form_url = $data->{'form_url'};
		$field_name_prefix = $data->{'field_name_prefix'};
		$field_params = $data->{'field_params'};
		// Verify nonce.
		$nonce = $data->field_options_nonce;
		if ( ! wp_verify_nonce( $nonce, 'wp_builder_field_options_nonce' ) ) {
			?>
			<div class="notice wp-builder-notice notice-error">
				<p><?= __( 'This form is outdated, please refresh and try again.', 'wp-builder' ); ?></p>
			</div>
			<?php
			wp_die();			
		}
		$field_name_prefix_array = preg_split( '/[\[+\]]+/', $field_name_prefix );
		$i = $field_name_prefix_array[1];
		$f = $field_name_prefix_array[3];
		$count = $data->{'count'};
		$args = [
			'field_name_prefix' => $field_name_prefix,
			'field_params' => $field_params,
			'metabox_id' => $i,
			'field_id' => $f,
			'url' => $form_url,
			'count' => $count,
		];
		$metabox_forms = new MetaboxForms();
		echo $metabox_forms->get_field_options( $type, $args );
		wp_die();
	}

	/**
	 * Ajax get field form structure.
	 *
	 * @since 2.0.1
	 */
	public function ajax_get_field_form_structure() {
		/*
		 * @param string $type
		 *  The type of field to build.
		 * @param string $form_url
		 *  The form url.
		 * @param int $i
		 *  The current metabox iteration.
		 * @param int $f
		 *  The current field iteration.
		 * @param string $field_name_prefix
		 *  The prefix for the field.
		 * @param array $field_param
		 *  An array of field parameters.
		 * @param array $metabox
		 *  An array of the metabox.
		 */
		if ( ! isset( $_POST['data'] ) ) {
			wp_die();
		}
		$data = json_decode( stripcslashes( $_POST['data'] ) );
		$type = $data->{'field_type'};
		$form_url = $data->{'form_url'};
		$field_name_prefix = $data->{'field_name_prefix'};
		$field_params = [];
		$i = $data->{'i'};
		$f = $data->{'f'};
		// Verify nonce.
		$nonce = $data->field_form_structure_nonce;
		if ( ! wp_verify_nonce( $nonce, 'wp_builder_field_form_structure_nonce' ) ) {
			?>
			<div class="notice wp-builder-notice notice-error">
				<p><?= __( 'This form is outdated, please refresh and try again.' . $nonce, 'wp-builder' ); ?></p>
			</div>
			<?php
			wp_die();
		}
		$metabox_forms = new MetaboxForms();
		$field_form_structure = $metabox_forms->field_type_form_structure( $type, $form_url, $i, $f, $field_name_prefix );
		wp_die();
	}
	/**
	 * Ajax get_field_structure.
	 *
	 * @since 0.0.3
	 */
	public function ajax_get_field_structure() {
		if ( ! isset( $_POST['data'] ) ) {
			wp_die();
		}
		$data = json_decode( stripcslashes( $_POST['data'] ) );
		$type = $data->{'type'};
		$form_url = $data->{'form_url'};
		$field_name_prefix = $data->{'field_name_prefix'};
		$field_params = $data->{'field_params'};
		// Verify nonce.
		$nonce = $data->field_structure_nonce;
		if ( ! wp_verify_nonce( $nonce, 'wp_builder_field_structure_nonce' ) ) {
			?>
			<div class="notice wp-builder-notice notice-error">
				<p><?= __( 'This form is outdated, please refresh and try again.', 'wp-builder' ); ?></p>
			</div>
			<?php
			wp_die();
		}
		$field_name_prefix_array = preg_split( '/[\[+\]]+/', $field_name_prefix );
		$i = $field_name_prefix_array[1];
		$f = $field_name_prefix_array[3];
		$args = [
			'field_name_prefix' => $field_name_prefix,
			'field_params' => $field_params,
			'metabox_id' => $i,
			'field_id' => $f,
			'url' => $form_url,
		];
		$metabox_forms = new MetaboxForms();
		$field_structure = $metabox_forms->get_field_structure( $type, $args );
		wp_die();
	}

	/**
	 * Ajax get customizer settings.
	 *
	 * @since 2.0.0
	 */
	public function ajax_get_customizer_settings() {
		if ( ! isset( $_POST['data'] ) ) {
			wp_die();
		}
		$data = json_decode( stripslashes( $_POST['data'] ) );
		$nonce = $data->wp_builder_customizer_settings_nonce;	
		if ( ! wp_verify_nonce( $nonce, 'wp_builder_customizer_settings_nonce' ) ) {
			?>
			<div class="notice wp-builder-notice notice-error">
				<p><?= __( 'This form is outdated, please refresh and try again.', 'wp-builder' ); ?></p>
			</div>
			<?php
			wp_die();
		}
		$setting_type = $data->setting_type;
		$customizer_forms = new CustomizerForms();

		switch ( $setting_type ) {
			case 'choices':
				$field_setting_type_id = 'field_' . $setting_type . '_id';
				$args = [
					'field_name_prefix' => $data->field_name_prefix,
					'field_setting_id' => $data->field_setting_id,
					$field_setting_type_id => $data->field_setting_type_id,
				];
				$function = 'field_choices_form';
				$customizer_forms->$function( $args );
				break;
			case 'input_attrs':
				$field = [
					'attribute' => $data->attribute,
					'name' => $data->name,
					'type' => $data->type,
					'label' => $data->label,
				];
				$action = $data->action;
				$customizer_forms->input_attribute_field_form( $field, $action );
				break;
			case 'control':
				$type = $data->type;
				$setting_id = $data->setting_id;
				$customizer_forms->control_setting_form( $type, $setting_id );
				break;
		}
		wp_die();
	}

	/**
	 * Ajax get post type capabilities.
	 *
	 * @since 2.0.1
	 *
	 * @return string HTML
	 */
	public function ajax_get_post_type_capabilities() {
		if ( ! isset( $_POST['data'] ) ) {
			wp_die();
		}
		$data = json_decode( stripslashes( $_POST['data'] ) );
		$nonce = $data->post_type_capabilities_nonce;
		if ( ! wp_verify_nonce( $nonce, 'wp_builder_post_type_capabilities_nonce' ) ) {
			?>
			<div class="notice wp-builder-notice notice-error">
				<p><?= __( 'This form is outdated, please refresh and try again.', 'wp-builder' ); ?></p>
			</div>
			<?php
			wp_die();
		}
		$item = $data->item;
		$count = $data->count;
		$post_type_forms = new PostTypeForms();
		$post_type_forms->capabilities_form_structure( $item, $count );
		wp_die();
	}

	/**
	 * Ajax get_field_type_form_structure.
	 *
	 * @since 2.0.0
	 * @todo This should replace the js template function add_field.
	 */
	public function wp_builder_ajax_get_field_type_form_structure() {
		if ( ! isset( $_POST['data'] ) ) {
			wp_die();
		}
		$data = json_decode( stripcslashes( $_POST['data'] ) );
		$type = $data->{'type'};
		$form_url = $data->{'form_url'};
		$field_name_prefix = $data->{'field_name_prefix'};
		$i = $data->{'i'};
		$f = $data->{'f'};
		$this->metabox_forms->field_type_form_structure( $type, $form_url, $i, $f, $field_name_prefix = '', $field_params = [], $metabox );
	}

	/**
	 * Get form url.
	 *
	 * @since 2.0.0
	 * @param string $tab The tab.
	 *
	 * @return string $form_url
	 */
	public function get_form_url( $tab ) {
		$form_url = admin_url( 'admin.php?page=' . $this->slug . '&tab=' . $tab );
		return $form_url;
	}
}
