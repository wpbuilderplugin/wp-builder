<?php
/**
 * The admin functionality for WP Builder.
 *
 * @since 2.0.0
 * @package wp-builder
 */

namespace WpBuilder;

// If this file is called directly, short.
if ( ! defined( 'WPINC' ) ) {
	die;
}

use WpBuilder\CustomizerManager;
use WpBuilder\MetaboxManager;
use WpBuilder\PostTypeManager;
use WpBuilder\SidebarManager;
use WpBuilder\TaxonomyManager;
use WpBuilder\MenuLocationManager;
use WpBuilder\NoticeManager;
use WpBuilder\DisplayManager;
use WpBuilder\Utilities;

/**
 * Contains Admin class.
 */
class Admin {

	/**
	 * Current tab.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $current_tab
	 */
	protected $current_tab;

	/**
	 * Current Action.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $current_action
	 */
	protected $current_action;

	/**
	 * Slug.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $slug
	 */
	protected $slug;

	/**
	 * Query string.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var string $query_string
	 */
	protected $query_string;

	/**
	 * WpBuilder\CustomizerManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\CustomizerManager $customizer_manager
	 */
	protected $customizer_manager;

	/**
	 * WpBuilder\MetaboxManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MetaboxManager $metabox_manager
	 */
	protected $metabox_manager;

	/**
	 * WpBuilder\PostTypeManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\PostTypeManager $post_type_manager
	 */
	protected $post_type_manager;

	/**
	 * WpBuilder\TaxonomyManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\TaxonomyManager $taxonomy_manager
	 */
	protected $taxonomy_manager;

	/**
	 * WpBuilder\MenuLocationManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\MenuLocationManager $menu_location_manager
	 */
	protected $menu_location_manager;

	/**
	 * WpBuilder\SidebarManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\SidebarManager $sidebar_manager
	 */
	protected $sidebar_manager;

	/**
	 * WpBuilder\DisplayManager definition.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @var \WpBuilder\DisplayManager $display_manager
	 */
	 protected $display_manager;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    2.0.0
	 */
	public function __construct() {

		$this->slug = 'wp-builder';
		$this->current_tab = $this->set_current_tab();
		$this->current_action = $this->set_current_action();
		$this->query_string = $this->set_query_string();
		$this->load_dependencies();

	}

	/**
	 * Load dependencies.
	 *
	 * @since 2.0.0
	 */
	private function load_dependencies() {
		$this->customizer_manager = new CustomizerManager();
		$this->metabox_manager = new MetaboxManager();
		$this->menu_location_manager = new MenuLocationManager();
		$this->taxonomy_manager = new TaxonomyManager();
		$this->post_type_manager = new PostTypeManager();
		$this->sidebar_manager = new SidebarManager();
		$this->display_manager = new DisplayManager();
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    2.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'jquery-ui-accordion' );
		wp_register_script( 'wp-builder', WP_BUILDER_ADMIN_ASSETS_PATH . 'js/wp-builder.js', [ 'jquery', 'jquery-ui-sortable' ], WP_BUILDER_VERSION, true );
		wp_enqueue_script( 'wp-builder' );
		$localized_script = [
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'field_options_nonce' => wp_create_nonce( 'wp_builder_field_options_nonce' ),
			'field_form_structure_nonce' => wp_create_nonce( 'wp_builder_field_form_structure_nonce' ),
			'field_structure_nonce' => wp_create_nonce( 'wp_builder_field_structure_nonce' ),
			'customizer_settings_nonce' => wp_create_nonce( 'wp_builder_customizer_settings_nonce' ),
			'post_type_capabilities_nonce' => wp_create_nonce( 'wp_builder_post_type_capabilities_nonce' ),
			'metabox_field_types' => $this->metabox_manager->field_types(),
		];
		wp_localize_script( 'wp-builder', 'wp_builder_admin_local', $localized_script );
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    2.0.0
	 */
	public function enqueue_styles() {
		wp_register_style( 'wp-builder-jquery-ui', WP_BUILDER_ADMIN_ASSETS_PATH . 'css/jquery-ui-fresh.css', '', WP_BUILDER_VERSION );
		wp_register_style( 'wp-builder-jquery-ui-theme', plugins_url( 'vendor/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css', WP_BUILDER_FILE ), '', WP_BUILDER_VERSION );
		wp_register_style( 'wp-builder', WP_BUILDER_ADMIN_ASSETS_PATH . 'css/wp-builder.css', '', WP_BUILDER_VERSION );
		wp_enqueue_style( 'wp-builder' );
	}

	/**
	 * Action links callback.
	 *
	 * @since 2.0.0
	 * @param array $links The array of links.
	 *
	 * @return array
	 */
	public function action_links( $links ) {
		return array_merge( [ 'settings' => '<a href="' . admin_url( 'admin.php?page=' . $this->slug ) . '">' . __( 'Settings', 'wp-builder' ) . '</a>' ], $links );
	}

	/**
	 * Register settings page.
	 *
	 * @since 2.0.0
	 */
	public function add_admin_menu() {
		$option_page = add_menu_page( 
			'WP Builder Settings',
			'WP Builder',
			'manage_options',
			$this->slug,
			[ $this, 'create_admin_interface' ],
			WP_BUILDER_ASSETS_PATH . 'images/icon.svg',
			98
		);
		add_action( "load-{$option_page}", [ $this, 'router' ] );
	}

	/**
	 * Callback for admin routing.
	 *
	 * @since 2.0.0
	 */
	public function router() {
		if ( isset( $_POST['wp_builder_submit'] ) && $_POST['wp_builder_submit'] == 'Y' ) {
			if ( isset( $_POST['action'] ) && 'delete' == $_POST['action'] ) {
				if ( isset( $_POST['type'] ) ) {
					$this->delete( $_POST['type'] );
				}
			} else {
				$this->save();
			}
			wp_redirect( admin_url( 'admin.php?page=' . $this->slug . $this->query_string ) );
			exit;
		}
	}

	/**
	 * Save form data.
	 *
	 * @since 2.0.0
	 */
	public function save() {
		global $pagenow;
		if ( $pagenow == 'admin.php' && isset( $_GET['page'] ) && $_GET['page'] == $this->slug ) {
			switch ( $this->current_tab ) {
				case 'import':
					if ( ! isset( $_REQUEST['import_settings'] ) || ! wp_verify_nonce( $_REQUEST['import_settings'], 'wp_builder_import_settings' ) ) {
						NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=import' );
					} else {
						$type = $_POST['type'];
						/**
						 * @var string $import_strategy
						 * delete - delete existing.
						 * overwrite - overwrite matches.
						 * add - only add new, skip matches.
						 */
						$import_strategy = $_POST['strategy'];
						$uploaded_json = false;
						$pasted_json = false;
						$json_configuration = false;
						$stored_configuration = false;
						if ( $_POST['method'] == 'paste' ) {
							if ( array_key_exists( 'pasted', $_POST ) && $_POST['pasted'] != '' ) {
								$json_configuration = json_decode( stripslashes( $_POST['pasted'] ), true );
							}
						} elseif ( $_POST['method'] == 'upload' ) {
							if ( $_FILES['upload']['error'] == UPLOAD_ERR_OK && is_uploaded_file( $_FILES['upload']['tmp_name'] ) ) {
								$uploaded_json = file_get_contents( $_FILES['upload']['tmp_name'] );
							}
							if ( $uploaded_json != false && Utilities::isJson( $uploaded_json ) ) {
								$json_configuration = json_decode( $uploaded_json, true );
							}
						}
						if ( $json_configuration == false ) {
							NoticeManager::add_notice( 'error', __( 'Please check your json and try again.', 'wp-builder' ), true, 'tab=import' );
						} else {
							switch ( $type ) {
								case 'sidebars':
									$result = $this->sidebar_manager->update( $json_configuration, $import_strategy );
									if ( ! $result ) {
										NoticeManager::add_notice( 'error', __( 'Sidebars not imported, please check your json and try again.', 'wp-builder' ), true, 'tab=import' );
									} else {
										NoticeManager::add_notice( 'success', __( 'Sidebars imported successfully.', 'wp-builder' ), true, 'tab=import' );
									}
									break;  // end 'sidebars'.
								case 'post_types':
									$result = $this->post_type_manager->update( $json_configuration, $import_strategy );
									if ( ! $result ) {
										NoticeManager::add_notice( 'error', __( 'Post Types not imported, please check your json and try again.', 'wp-builder' ), true, 'tab=import' );
									} else {
										NoticeManager::add_notice( 'success', __( 'Post Types imported successfully.', 'wp-builder' ), true, 'tab=import' );
									}
									break; // post_types import.
								case 'menu_locations':
									$result = $this->menu_location_manager->update( $json_configuration, $import_strategy );
									if ( ! $result ) {
										NoticeManager::add_notice( 'error', __( 'Menu Locations not imported, please check your json and try again.', 'wp-builder' ), true, 'tab=import' );
									} else {
										NoticeManager::add_notice( 'success', __( 'Menu Locations imported successfully.', 'wp-builder' ), true, 'tab=import' );
									}
									break;  // end 'menu_locations'.
								case 'metaboxes':
									$result = $this->metabox_manager->update( $json_configuration, $import_strategy );
									if ( ! $result ) {
										NoticeManager::add_notice( 'error', __( 'Metaboxes not imported, please check your json and try again.', 'wp-builder'), true, 'tab=import' );
									} else {
										NoticeManager::add_notice( 'success', __( 'Metaboxes imported successfully.', 'wp-builder' ), true, 'tab=import' );
									}
									break; // end metaboxes import.
								case 'taxonomies':
									$result = $this->taxonomy_manager->update( $json_configuration, $import_strategy );
									if ( ! $result ) {
										NoticeManager::add_notice( 'error', __( 'Taxonomies not imported, please check your json and try again.', 'wp-builder' ), true, 'tab=import' );
									} else {
										NoticeManager::add_notice( 'success', __( 'Taxonomies imported successfully.', 'wp-builder' ), true, 'tab=import' );
									}
									break; // end taxonomies import.
								case 'customizer-panels':
									$result = $this->customizer_manager->import_panels( $json_configuration, $import_strategy );
									if ( ! $result ) {
										NoticeManager::add_notice( 'error', __( 'Could not import Customizer Panels, please try again.', 'wp-builder' ), true, 'tab=import' );
									} else {
										NoticeManager::add_notice( 'success', __( 'Customizer Panels imported successfully.', 'wp-builder' ), true, 'tab=import' );
									}
									break;
								case 'customizer-sections':
									$result = $this->customizer_manager->import_sections( $json_configuration, $import_strategy );
									if ( ! $result ) {
										NoticeManager::add_notice( 'error', __( 'Could not import the Customizer Sections, please try again.', 'wp-builder' ), true, 'tab=import' );
									} else {
										NoticeManager::add_notice( 'success', __( 'Customizer Sections imported successfully.', 'wp-builder' ), true, 'tab=import' );
									}
									break;
								case 'customizer-settings':
									$result = $this->customizer_manager->import_settings( $json_configuration, $import_strategy );
									if ( ! $result ) {
										NoticeManager::add_notice( 'error', __( 'Could not import the Customizer Settings, please try again.', 'wp-builder' ), true, 'tab=import' );
									} else {
										NoticeManager::add_notice( 'success', __( 'Customizer Setting imported successfully.', 'wp-builder' ), true, 'tab=import' );
									}
									break;
							}
						}
					}
					break;
				case 'sidebars':
					if ( !isset( $_REQUEST['sidebar_settings'] ) || !wp_verify_nonce( $_REQUEST['sidebar_settings'], 'wp_builder_sidebar_settings' ) ) {
						NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=sidebars' );
					} else {
						$result = $this->sidebar_manager->update( $_POST['sidebar'], 'overwrite' );
						if ( ! $result ) {
							NoticeManager::add_notice( 'error', __( 'Something went wrong, Sidebar not updated.', 'wp-builder' ), true, 'tab=sidebars' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Sidebar updated successfully', 'wp-builder' ), true, 'tab=sidebars' );
						}
					}
					break;
				case 'post_types':
					if ( ! isset( $_REQUEST['post_type_settings'] ) || ! wp_verify_nonce( $_REQUEST['post_type_settings'], 'wp_builder_post_type_settings' ) ) {
						NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=post_types' );
					} else {
						$result = $this->post_type_manager->update( $_POST['post_type'], 'overwrite' );
						if ( !$result ) {
							NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=post_types' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Post Type updated successfully', 'wp-builder' ), true, 'tab=post_types' );
						}
					}
					break;
				case 'metaboxes':
					if ( ! isset( $_REQUEST['metabox_settings'] ) || ! wp_verify_nonce( $_REQUEST['metabox_settings'], 'wp_builder_metabox_settings' ) ) {
						NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=metaboxes' );
					} else {
						$result = $this->metabox_manager->update( $_POST['metabox'], 'overwrite' );
						if ( ! $result ) {
							NoticeManager::add_notice( 'error', __( 'Metabox not updated, please try again.', 'wp-builder' ), true, 'tab=metaboxes' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Metabox updated successfully.', 'wp-builder' ), true, 'tab=metaboxes' );
						}
					}
					break;
				case 'menu_locations':
					if ( ! isset( $_REQUEST['menu_location_settings'] ) || ! wp_verify_nonce( $_REQUEST['menu_location_settings'], 'wp_builder_menu_location_settings' ) ) {
						NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=menu_locations' );
					} else {
						$result = $this->menu_location_manager->update( $_POST['menu_location'], 'overwrite' );
						if ( ! $result ) {
							NoticeManager::add_notice( 'error', __( 'Menu Locations not updated, please try again.', 'wp-builder' ), true, 'tab=menu_locations' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Menu Locations updated successfully.', 'wp-builder' ), true, 'tab=menu_locations' );
						}						
					}
					break;
				case 'taxonomies':
					if ( ! isset( $_REQUEST['taxonomy_settings'] ) || ! wp_verify_nonce( $_REQUEST['taxonomy_settings'], 'wp_builder_taxonomy_settings' ) ) {
						NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'global' );
					} else {				
						$result = $this->taxonomy_manager->update( $_POST['custom_taxonomy'], 'overwrite' );
						if ( !$result ) {
							NoticeManager::add_notice( 'error', __( 'Taxonomy not updated, please try again.', 'wp-builder' ), true, 'tab=taxonomies' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Taxonomy updated successfully.', 'wp-builder' ), true, 'tab=taxonomies' );
						}
					}
					break;
				case 'customizer':
					if ( ! isset( $_REQUEST['component'] ) || ! wp_verify_nonce( $_REQUEST['customizer_settings'], 'wp_builder_customizer_settings' ) ) {
						NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'global' );
					} else {
						switch ( $_REQUEST['component'] ) {
							case 'panel':
								$result = $this->customizer_manager->save_panel( $_POST['panel'] );
								if ( ! $result ) {
									NoticeManager::add_notice( 'error', __( 'Customizer Panel not updated, please try again.', 'wp-builder' ), true, 'tab=customizer' );
								} else {
									NoticeManager::add_notice( 'success', __( 'Customizer Panel updated successfully.', 'wp-builder' ), true, 'tab=customizer' );
								}
								break;
							case 'section':
								$result = $this->customizer_manager->save_section( $_POST['section'] );
								if ( ! $result ) {
									NoticeManager::add_notice( 'error', __( 'Customizer Section not updated, please try again.', 'wp-builder' ), true, 'tab=customizer' );
								} else {
									NoticeManager::add_notice( 'success', __( 'Customizer Section updated successfully.', 'wp-builder' ), true, 'tab=customizer' );
								}
								break;
							case 'setting':
								$result = $this->customizer_manager->save_setting( $_POST['setting'] );
								if ( ! $result ) {
									NoticeManager::add_notice( 'error', __( 'Customizer Setting not updated, please try again.', 'wp-builder' ), true, 'tab=customizer' );
								} else {
									NoticeManager::add_notice( 'success', __( 'Customizer Setting updated successfully.', 'wp-builder' ), true, 'tab=customizer' );
								}
								break;
						}
					}
					break;
			}
		}
	}

	/**
	 * Delete items.
	 *
	 * @since 2.0.0
	 * @param string $type The component type to delete.
	 */
	public function delete( $type ) {
		switch ( $type ) {
			case 'sidebar':
				if ( ! isset( $_REQUEST['sidebar_delete'] ) || ! wp_verify_nonce( $_REQUEST['sidebar_delete'], 'wp_builder_sidebar_delete' ) ) {
					NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=sidebars' );
				} else {
					$sidebar = $_REQUEST['sidebar'];
					if ( ! isset( $sidebar['machine_name'] ) ) {
						NoticeManager::add_notice( 'error', __( 'No sidebar selected.', 'wp-builder' ), true, 'tab=sidebars' );
					} else {
						$result = $this->sidebar_manager->delete_single( $sidebar['machine_name'] );
						if ( ! $result ) {
							NoticeManager::add_notice( 'error', __( 'Sidebar not deleted, please try again.', 'wp-builder' ), true, 'tab=sidebars' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Sidebar deleted successfully.', 'wp-builder' ), true, 'tab=sidebars' );
						}
					}
				}
				break;
			case 'metabox':
				if ( ! isset( $_REQUEST['metabox_delete'] ) || ! wp_verify_nonce( $_REQUEST['metabox_delete'], 'wp_builder_metabox_delete' ) ) {
					NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=metaboxes' );
				} else {
					$metabox = $_REQUEST['metabox'];
					if ( ! isset( $metabox['unique_id'] ) ) {
						NoticeManager::add_notice( 'error', __( 'No metabox selected, please try again.', 'wp-builder' ), true, 'tab=metaboxes' );
					} else {
						$result = $this->metabox_manager->delete_single( $metabox['unique_id'] );
						if ( ! $result ) {
							NoticeManager::add_notice( 'error', __( 'Metabox not deleted, please try again.', 'wp-builder' ), true, 'tab=metaboxes' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Metabox deleted successfully.', 'wp-builder' ), true, 'tab=metaboxes' );
						}
					}
				}
				break;
			case 'menu_location':
				if ( ! isset( $_REQUEST['menu_location_delete'] ) || ! wp_verify_nonce( $_REQUEST['menu_location_delete'], 'wp_builder_menu_location_delete' ) ) {
					NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=menu_locations' );
				} else {
					$menu_location = $_REQUEST['menu_location'];
					if ( ! isset( $menu_location['machine_name'] ) ) {
						NoticeManager::add_notice( 'error', __( 'No Menu Location selected', 'wp-builder' ), true, 'tab=menu_locations' );
					} else {
						$result = $this->menu_location_manager->delete_single( $menu_location['machine_name'] );
						if ( ! $result ) {
							NoticeManager::add_notice(
								'error',
								__( 'Menu Location not deleted, please try again.', 'wp-builder'),
								true,
								'tab=menu_locations'
							);
						} else {
							NoticeManager::add_notice(
								'success',
								__( 'Menu Location deleted successfully.', 'wp-builder' ),
								true,
								'tab=menu_locations'
							);
						}
					}
				}
				break;
			case 'taxonomy':
				if ( ! isset( $_REQUEST['taxonomy_delete'] ) || ! wp_verify_nonce( $_REQUEST['taxonomy_delete'], 'wp_builder_taxonomy_delete' ) ) {
					NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=taxonomies' );
				} else {
					$taxonomy = $_REQUEST['taxonomy'];
					if ( ! isset( $taxonomy['unique_id'] ) ) {
						NoticeManager::add_notice( 'error', __( 'No Taxonomy selected, please try again.', 'wp-builder' ), true, 'tab=taxonomies' );
					} else {
						$result = $this->taxonomy_manager->delete_single( $taxonomy['unique_id'] );
						if ( ! $result ) {
							NoticeManager::add_notice( 'error', __( 'Taxonomy not deleted, please try again.', 'wp-builder' ), true, 'tab=taxonomies' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Taxonomy deleted successfully.', 'wp-builder' ), true, 'tab=taxonomies' );
						}
					}
				}
				break;
			case 'post_type':
				if ( ! isset( $_REQUEST['post_type_delete'] ) || ! wp_verify_nonce( $_REQUEST['post_type_delete'], 'wp_builder_post_type_delete' ) ) {
					NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=post_types' );
				}
				else {
					$post_type = $_REQUEST['post_type'];
					if ( ! isset( $post_type['unique_id'] ) ) {
						NoticeManager::add_notice( 'error', __( 'No Post Type selected, please try again.', 'wp-builder' ), true, 'tab=post_types' );
					} else {
						$result = $this->post_type_manager->delete_single( $post_type['unique_id'] );
						if ( ! $result ) {
							NoticeManager::add_notice( 'error', __( 'Post Type not deleted, please try again.', 'wp-builder' ), true, 'tab=post_types' );
						} else {
							NoticeManager::add_notice( 'success', __( 'Post Type deleted successfully.', 'wp-builder' ), true, 'tab=post_types' );
						}
					}
				}
				break;
			case 'customizer':
				if ( ! isset( $_REQUEST['component'] ) || ! wp_verify_nonce( $_REQUEST['customizer_delete'], 'wp_builder_customizer_delete' ) ) {
					NoticeManager::add_notice( 'error', __( 'Something went wrong, please try again.', 'wp-builder' ), true, 'tab=customizer' );
				} else {
					switch ( $_REQUEST['component'] ) {
						case 'panel':
							$result = $this->customizer_manager->delete_panel( $_POST['panel'] );
							if ( ! $result ) {
								NoticeManager::add_notice( 'error', __( 'Could not delete this Customizer Panel, please try again.', 'wp-builder' ), true, 'tab=customizer' );
							} else {
								NoticeManager::add_notice( 'success', __( 'Customizer Panel deleted successfully.', 'wp-builder' ), true, 'tab=customizer' );
							}
							break;
						case 'section':
							$result = $this->customizer_manager->delete_section( $_POST['section'] );
							if ( ! $result ) {
								NoticeManager::add_notice( 'error', __( 'Could not delete this Customizer Section, please try again.', 'wp-builder' ), true, 'tab=customizer' );
							} else {
								NoticeManager::add_notice( 'success', __( 'Customizer Section deleted successfully.', 'wp-builder' ), true, 'tab=customizer' );
							}
							break;
						case 'setting':
							$result = $this->customizer_manager->delete_setting( $_POST['setting'] );
							if ( ! $result ) {
								NoticeManager::add_notice( 'error', __( 'Could not delete this Customizer Setting, please try again.', 'wp-builder' ), true, 'tab=customizer' );
							} else {
								NoticeManager::add_notice( 'success', __( 'Customizer Setting deleted successfully.', 'wp-builder' ), true, 'tab=customizer' );
							}
							break;
					}
				}
				break;
		}
	}

	/**
	 * Callback for Admin pages.
	 *
	 * @since 2.0.0
	 */
	public function create_admin_interface() {
		$this->display_manager->render_page( $this->current_tab, $this->current_action );
	}

	/**
	 * Set current tab.
	 *
	 * @since 2.0.0
	 *
	 * @return string $tab
	 */
	public function set_current_tab() {
		$tab = 'homepage';
		if ( isset( $_GET['tab'] ) ) {
			$tab = $_GET['tab'];
		}
		return $tab;
	}

	/**
	 * Get current tab.
	 *
	 * @since 2.0.0
	 *
	 * @return string $current_tab
	 */
	public function get_current_tab() {
		return $this->current_tab;
	}

	/**
	 * Set current action.
	 *
	 * @since 2.0.0
	 *
	 * @return string $action
	 */
	public function set_current_action() {
		$action = 'view';
		if ( isset( $_GET['action'] ) ) {
			$action = $_GET['action'];
		}
		return $action;
	}

	/**
	 * Get current action.
	 *
	 * @since 2.0.0
	 *
	 * @return string $current_action
	 */
	public function get_current_action() {
		return $this->current_action;
	}

	/**
	 * Set query string.
	 *
	 * @since 2.0.0
	 *
	 * @return string $query_string
	 */
	public function set_query_string() {
		$query_string = '&tab=' . $this->current_tab;
		return $query_string;
	}

	/**
	 * Get query string.
	 *
	 * @since 2.0.0
	 *
	 * @return string $query_string
	 */
	public function get_query_string() {
		return $this->query_string;
	}

}
